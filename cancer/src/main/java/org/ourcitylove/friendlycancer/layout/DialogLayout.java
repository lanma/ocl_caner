package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

public class DialogLayout extends FreeLayout {

    public final LinearLayout bg;
    public FreeLayout content;
    public FreeTextView text;
    public View underLine;
    public LinearLayout buttonPort;
    public AlertDialog dialog;

    public DialogLayout(Context context) {
        super(context);
        setPicSize(1340,1200,TO_WIDTH);
        setFreeLayoutWW();
        setPadding(this, 50, 50, 50, 50);
        setBackgroundColor(Color.TRANSPARENT);

        bg = (LinearLayout) addFreeView(new LinearLayout(context), -1, -2);
        bg.setOrientation(LinearLayout.VERTICAL);
        bg.setBackgroundResource(R.drawable.dialogcorner);

        content = (FreeLayout) addFreeLinearView(bg, new FreeLayout(context), -1, -2);
        text = (FreeTextView) content.addFreeView(new FreeTextView(context), -1, -2);
        setPadding(text, 100, 100, 100, 100);
        text.setGravity(Gravity.CENTER);
        text.setTextSizeFitSp(30);
        text.setTextColor(Color.BLACK);
        text.setTypeface(null, Typeface.BOLD);

        underLine = addFreeLinearView(bg, new View(context), -1, 2);
        underLine.setBackgroundColor(Color.WHITE);

        buttonPort = (LinearLayout) addFreeLinearView(bg, new LinearLayout(context), -1, 200);
        buttonPort.setBackgroundResource(R.drawable.dialogcornerbtn);
//        addButton(R.string.OK, v->dialog.dismiss());
    }

    public void addButton(int textResId, OnClickListener listener) {
        if (buttonPort.getChildCount() > 0) {
            View v = addFreeLinearView(buttonPort, new View(getContext()), 2, -1);
            v.setBackgroundColor(Color.WHITE);
        }

        FreeTextView btn = (FreeTextView) addFreeLinearView(buttonPort, new FreeTextView(getContext()),0,-1,1);
        setPadding(btn,0,10,0,10);
        btn.setGravity(Gravity.CENTER);
        btn.setTextSizeFitSp(30);
        btn.setTextColor(Color.WHITE);
        btn.setBackgroundColor(Color.TRANSPARENT);
//        btn.setBackgroundResource(R.color.mainColor);

        btn.setText(getContext().getString(textResId));
        btn.setOnClickListener(listener);
    }

    public void clearButton() {
        buttonPort.removeAllViews();
    }

    public void buildDialog() {
        dialog = new AlertDialog.Builder(getContext(), R.style.AppTheme)
                .setView(this).create();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        DisplayMetrics display = new DisplayMetrics();
        dialog.getWindow().getWindowManager().getDefaultDisplay().getMetrics(display);
        dialog.getWindow().setLayout(display.widthPixels*9/10, -2);
        dialog.show();
    }
}
