package org.ourcitylove.friendlycancer.layout;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import com.james.views.FreeTextView;

/**
 * Created by Vegetable on 2016/4/7.
 */
public class titletextlayout extends dialogbglayout{
    public FreeTextView title,content;
    public titletextlayout(Context context) {
        super(context);

        searchtitlelayout searchtitlelayout =(searchtitlelayout)addFreeView(new searchtitlelayout(context),-1,-2,cancel,new int[]{BELOW});
        searchtitlelayout.leftline.setBackgroundColor(Color.WHITE);
        searchtitlelayout.rightline.setBackgroundColor(Color.WHITE);
        searchtitlelayout.title.setTextColor(Color.WHITE);
        searchtitlelayout.icon.setVisibility(GONE);
        title = searchtitlelayout.title;

        content =(FreeTextView)addFreeView(new FreeTextView(context),-1,-2,searchtitlelayout,new int[]{BELOW,CENTER_IN_PARENT});
        setMargin(content,20,10,20,50);
        content.setGravity(Gravity.CENTER);
        content.setTextSizeFitSp(30);
        content.setTextColor(Color.WHITE);
    }
}
