package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.Window;
import android.widget.ImageView;

import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/6/23.
 */

public class SearchNoData extends dialogbglayout {
    public SearchNoData(Context context) {
        super(context);

        ImageView img = (ImageView)addFreeView(new ImageView(context), -1, 250, cancel, new int[]{BELOW, CENTER_HORIZONTAL});
        img.setScaleType(ImageView.ScaleType.FIT_CENTER);
        img.setImageResource(R.drawable.item_icon_nodata);

        FreeTextView dat = (FreeTextView)addFreeView(new FreeTextView(context), -1,-2, cancel, new int[]{BELOW});
        dat.setTextSizeFitSp(35);
        dat.setTextColor(Color.WHITE);
        dat.setGravity(Gravity.CENTER);
        dat.setText("查無資料");
        setMargin(dat, 10, 30, 10, 10);

    }
}
