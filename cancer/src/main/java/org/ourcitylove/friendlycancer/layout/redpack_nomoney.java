package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/6/17.
 */

public class redpack_nomoney extends redpack {

    protected FreeTextView button;
    public redpack_nomoney(Context context) {
        super(context);

        bg.setImageResource(R.drawable.red_pack_no);
        bg.setId(R.id.bg);

        title.setId(R.id.textView);

        button = (FreeTextView)addFreeView(new FreeTextView(context), -2, -2, title, new int[]{BELOW, CENTER_HORIZONTAL});
        button.setId(R.id.button);
        button.setBackgroundResource(R.drawable.redpackbutton);
        button.setTextSizeFitSp(30);
        setPadding(button,30,15,30,15);
        button.setText(R.string.OK);
        button.setVisibility(GONE);
        button.setTextColor(ContextCompat.getColor(context, R.color.redpackage_popup_text_color));
        setMargin(button,0,10,0,0);
        button.post(() -> button.setX(button.getX() + 10));

        icon.setVisibility(INVISIBLE);
        removeView(foreground);
        foreground = (ScratchImageView) addFreeView(foregroundview(), 544, 593, new int[]{CENTER_IN_PARENT});
        foreground.setId(R.id.foreground);
    }
}
