package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;

import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

/**
 * Created by Vegetable on 2016/7/9.
 */

public class MapIcon extends FreeLayout {
    public ImageView icon;
    public FreeTextView title;
    public MapIcon(Context context) {
        super(context);
        setPicSize(640, -2, TO_WIDTH);

        icon = (ImageView)addFreeView(new ImageView(context), 640, -2);
        icon.setScaleType(ImageView.ScaleType.FIT_CENTER);

        title = (FreeTextView)addFreeView(new FreeTextView(context), -1, -2);
        title.setTextSizeFitSp(20);
        title.setTextColor(Color.WHITE);
        setMargin(title, 0, 15, 0, 0);
    }
}
