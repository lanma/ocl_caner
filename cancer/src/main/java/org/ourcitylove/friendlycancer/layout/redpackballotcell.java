package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/9/19.
 */

public class redpackballotcell extends FreeLayout {
    public FreeTextView title;
    public redpackballotcell(Context context) {
        super(context);
        setFreeLayoutFW();
        setPicSize(640, -2, TO_WIDTH);
        setBackgroundResource(R.drawable.redpackresultballotcell);

        title = (FreeTextView)addFreeView(new FreeTextView(context), -1, -2, new int[]{CENTER_IN_PARENT});
        setPadding(title, 15, 15, 15, 15);
        title.setTextColor(Color.WHITE);
        title.setTextSizeFitSp(32f);


    }
}
