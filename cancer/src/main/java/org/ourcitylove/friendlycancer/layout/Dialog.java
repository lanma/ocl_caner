package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.james.views.FreeLayout;

import org.ourcitylove.friendlycancer.R;

import butterknife.ButterKnife;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by Vegetable on 2016/6/14.
 */

public class Dialog extends FreeLayout {
    public AlertDialog dialog;
    public TextView text;

    public ImageView icon;
    public GifImageView bg;
    public TextView button;
    public ScratchImageView foreground;

    public Dialog(Context context) {
        super(context);
        setFreeLayoutFF();
        redpack m = (redpack) addFreeView(new redpack(context), -1, -1, new int[]{CENTER_IN_PARENT});
        bindview(m);

    }
    private void bindview(View m){
        text = ButterKnife.findById(m, R.id.textView);
        bg = ButterKnife.findById(m, R.id.bg);
        icon = ButterKnife.findById(m, R.id.icon);
        button = ButterKnife.findById(m, R.id.button);
        foreground = ButterKnife.findById(m, R.id.foreground);
    }

    public Dialog resetnomoney(){
        removeAllViews();
        redpack_nomoney m = (redpack_nomoney) addFreeView(new redpack_nomoney(getContext()), -1, -1, new int[]{CENTER_IN_PARENT});
        bindview(m);
        return this;
    }


    public Dialog buildDialog(){
        dialog = new AlertDialog.Builder(getContext(), R.style.AlertDialog)
                .setView(this).create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();
         DisplayMetrics display = new DisplayMetrics();
        dialog.getWindow().getWindowManager().getDefaultDisplay().getMetrics(display);
        dialog.getWindow().setLayout(display.widthPixels, display.heightPixels);
        return this;
    }
}
