package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.LinearLayout;

import com.james.views.FreeLayout;

import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.helper.AddViewHelper;
import org.ourcitylove.friendlycancer.model.SearchModel;

import java.util.List;

/**
 * Created by Vegetable on 2016/3/31.
 */
public class ToWhereLayout extends FreeLayout{

    public ToWhereLayout(Context context, List<SearchModel>modelList) {
        super(context);
        setPicSize(640,-2,TO_WIDTH);
        setFreeLayoutFW();

        LinearLayout mlayout = (LinearLayout)addFreeView(new LinearLayout(context),-1,-2);
        mlayout.setOrientation(LinearLayout.VERTICAL);
        mlayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));

        AddViewHelper.addSearchView(mlayout,modelList);
    }
}
