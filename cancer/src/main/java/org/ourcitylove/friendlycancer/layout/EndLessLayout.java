package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Gravity;

import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/9/20.
 */

public class EndLessLayout extends BallotDialog {
    public FreeTextView URL, contentview1;
    public EndLessLayout(Context context) {
        super(context);

        titleview.setText(R.string.endless_title);
        contentview.setText(R.string.endless_content);
        contentview.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        setPadding(contentview, 20, 10, 20, 0);

        URL = (FreeTextView)addFreeView(new FreeTextView(context), -1, -2, contentview, new int[]{BELOW,CENTER_HORIZONTAL});
        URL.setBackgroundColor(Color.TRANSPARENT);
        URL.setTextSizeFitSp(26f);
        URL.setTextColor(Color.parseColor("#036eb3"));
        URL.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        setPadding(URL, 20, 0, 20, 20);
        SpannableString sbs = new SpannableString(context.getText(R.string.endless_url));
        sbs.setSpan(new UnderlineSpan(), 0, sbs.length(), 0);
        URL.setText(sbs);

        contentview1 = (FreeTextView)addFreeView(new FreeTextView(context), -1, -2, URL, new int[]{BELOW, CENTER_HORIZONTAL});
        contentview1.setBackgroundColor(Color.TRANSPARENT);
        contentview1.setTextSizeFitSp(30f);
        contentview1.setTextColor(Color.BLACK);
        contentview1.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        setPadding(contentview1, 20, 15, 20, 20);
        contentview1.setText(R.string.endless_content1);

        removeView(okbtn);
        addFreeView(okbtn, -1, -2, contentview1, new int[]{BELOW});
        okbtn.setOnClickListener(v->dialog.dismiss());
    }

    @Override
    public EndLessLayout buildDialog() {
        return (EndLessLayout)super.buildDialog();
    }
}
