package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.widget.ImageView;

import com.cooltechworks.views.*;
import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by Vegetable on 2016/6/15.
 */

public class redpack extends FreeLayout {

    protected ImageView icon;
    protected FreeTextView title;
    protected GifImageView bg;
    protected ScratchImageView foreground;

    public redpack(Context context) {
        super(context);
        setPicSize(640, -2, TO_WIDTH);
        setFreeLayoutFF();

        bg = (GifImageView) addFreeView(new GifImageView(context), 542, 592, new int[]{CENTER_IN_PARENT});
        bg.setScaleType(ImageView.ScaleType.FIT_XY);
        bg.setId(R.id.bg);
        bg.setImageResource(R.drawable.red_pack);

        icon = (ImageView) addFreeView(new ImageView(context), 150, 100, bg, new int[]{ALIGN_BOTTOM, CENTER_HORIZONTAL});
        setMargin(icon, 0, 0, 0, 20);
        setPadding(icon, 50, 0, 0, 0);
        icon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        icon.setImageResource(R.drawable.red_pack_deposit);
        icon.setId(R.id.icon);

        title = (FreeTextView) addFreeView(new FreeTextView(context), -1, -2, icon, new int[]{ABOVE});
        setMargin(title, 0, 10, 0, 10);
        setPadding(title, 50, 0, 0, 0);
        title.setTextSizeFitSp(35);
        title.setText(R.string.redpackage_init);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setTextColor(ContextCompat.getColor(context, R.color.redpackage_popup_text_color));
        title.setId(R.id.textView);

        foreground = (ScratchImageView) addFreeView(foregroundview(), 544, 593, new int[]{CENTER_IN_PARENT});
        foreground.setId(R.id.foreground);
    }

    protected ScratchImageView foregroundview() {
        ScratchImageView foreground = new ScratchImageView(getContext());
        foreground.setScratchDrawable(R.drawable.red_pack_foreground);
        foreground.setScratchSpeed(15);
        foreground.setStrokeWidth(15);
        foreground.setVisibility(GONE);
        return foreground;
    }
}
