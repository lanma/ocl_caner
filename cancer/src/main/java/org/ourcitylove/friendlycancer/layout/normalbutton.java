package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.widget.RelativeLayout;

import com.james.views.FreeLayout;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/7/7.
 */

public class normalbutton extends FreeLayout {
    public normalbutton(Context context) {
        super(context);
        setPicSize(640, -2, TO_WIDTH);

        button button = (button) addFreeView(new button(context), -1, -2, new int[]{CENTER_HORIZONTAL});
        button.setId(R.id.button);
        button.setTextSizeFitSp(28);
        GradientDrawable d = new GradientDrawable();
        d.setColor(ContextCompat.getColor(context, R.color.mainColor));
        d.setCornerRadius(20);
        button.setBackground(d);
        setPadding(button, 20, 25, 20, 25);
    }
}
