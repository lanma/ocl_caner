package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.widget.ImageView;

import com.james.views.FreeTextButton;

import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.helper.Helper;
import org.ourcitylove.oclapp.OtherApp;


/**
 * Created by Vegetable on 2016/6/16.
 */

public class leftimagewithcall extends leftimage {
    public leftimagewithcall(Context context) {
        super(context);

        img.setScaleType(ImageView.ScaleType.FIT_END);
        FreeTextButton call = (FreeTextButton)addFreeView(new FreeTextButton(context), 420,75,new int[]{ALIGN_PARENT_RIGHT,CENTER_VERTICAL});
        call.setTextSizeFitSp(29);
        call.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
        call.setText("臺北市政府衛生局專線");
        setMargin(call,0,0,5,0);
        setPadding(call,0,0,5,0);
        call.setTextColor(Color.BLACK);
        call.setBackgroundResource(R.drawable.button_normal);
        call.post(()->{
            Drawable d = ContextCompat.getDrawable(context, R.drawable.item_icon_call);
            Rect r = d.getBounds();
            r.left = 10;
            r.bottom = 10;
            r.right = call.getMeasuredHeight()-20;
            r.bottom = call.getMeasuredHeight()-20;
            d.setBounds(r);
            call.setCompoundDrawables(d,null,null,null);
            call.setMinimumHeight(call.getMeasuredHeight()+10);
        });
        call.setOnClickListener(v-> Helper.SeparateFromTitle("我做過了沒")
                                .subscribe(mvalue->{
                                    String[] type = mvalue.p1.split("\\$\\$");
                                    String[] content = mvalue.p2.split("\\$\\$");
                                    content[0] = "若有疑問，請撥打以下專線。";
                                    PhoneCall phoneCall = new PhoneCall(getContext(), type, content);
                                    phoneCall.builddialog();
                                    phoneCall.call.setOnClickListener(mv-> OtherApp.call(getContext(), mvalue.extra));
                                }));
    }
}
