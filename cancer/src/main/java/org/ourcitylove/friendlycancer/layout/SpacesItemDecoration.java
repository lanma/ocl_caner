package org.ourcitylove.friendlycancer.layout;

/**
 * Created by Vegetable on 2016/7/7.
 */

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int left, top, right, bottom;


    public SpacesItemDecoration(int space) {
        this.left = space;
        this.top = space;
        this.right = space;
        this.bottom = space;
    }

    public SpacesItemDecoration(int left, int top, int right, int bottom){
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = this.left;
        outRect.right = this.right;
        outRect.bottom = this.bottom;

        // Add top margin only for the first item to avoid double space between items
        if(parent.getChildAdapterPosition(view) == 0)
            outRect.top = this.top;
    }
}



