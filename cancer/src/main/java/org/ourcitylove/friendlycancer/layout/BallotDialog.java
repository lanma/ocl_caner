package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.Gravity;

import com.james.views.FreeLayout;
import com.james.views.FreeTextButton;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/9/19.
 */

public class BallotDialog extends FreeLayout {
    public AlertDialog dialog;
    public FreeTextButton okbtn;
    public FreeTextView titleview, contentview;
    public BallotDialog(Context context) {
        super(context);
        setFreeLayoutFF();
        setBackgroundResource(R.drawable.dialogcorner);
        setGravity(Gravity.CENTER_VERTICAL);

        titleview = (FreeTextView)addFreeView(new FreeTextView(context), -1, -2, new int[]{CENTER_HORIZONTAL});
        titleview.setBackgroundColor(Color.TRANSPARENT);
        titleview.setTextSizeFitSp(30f);
        titleview.setTextColor(Color.BLACK);
        titleview.setGravity(Gravity.CENTER);
        setPadding(titleview, 20, 15, 20, 0);

        contentview = (FreeTextView)addFreeView(new FreeTextView(context), -1, -2, titleview, new int[]{BELOW, CENTER_HORIZONTAL});
        contentview.setBackgroundColor(Color.TRANSPARENT);
        contentview.setTextSizeFitSp(30f);
        contentview.setTextColor(Color.BLACK);
        contentview.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        setPadding(contentview, 20, 15, 20, 20);

        okbtn = (FreeTextButton)addFreeView(new FreeTextButton(context), -1, -2, contentview, new int[]{BELOW});
        okbtn.setBackgroundResource(R.drawable.dialogcornerbtn);
        okbtn.setTextSizeFitSp(30.f);
        okbtn.setTextColor(Color.WHITE);
        okbtn.setGravity(Gravity.CENTER);
        okbtn.setText("確定");
        setPadding(okbtn, 0, 20, 0, 20);
    }

    public BallotDialog buildDialog(){
        dialog = new AlertDialog.Builder(getContext(), R.style.AlertDialog)
                .setView(this).create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.show();
        DisplayMetrics display = new DisplayMetrics();
        dialog.getWindow().getWindowManager().getDefaultDisplay().getMetrics(display);
        dialog.getWindow().setLayout(display.widthPixels*9/10, -2);
        return this;
    }

}
