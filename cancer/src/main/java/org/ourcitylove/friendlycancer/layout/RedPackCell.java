package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.ImageView;

import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/6/15.
 */

public class RedPackCell extends FreeLayout {
    public ImageView icon, righticon;
    public FreeTextView Title;

    public RedPackCell(Context context) {
        super(context);
        setFreeLayoutFW();
        setPicSize(640, -2, TO_WIDTH);
        setBackgroundResource(R.drawable.redpackresultcell);

        icon = (ImageView) addFreeView(new ImageView(context), 60, 60);
        icon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        setMargin(icon, 10, 10, 10, 10);

        righticon = (ImageView) addFreeView(new ImageView(context), 40, 40, new int[]{ALIGN_PARENT_RIGHT, CENTER_VERTICAL});
        setMargin(righticon, 0, 0, 3, 0);
        righticon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        righticon.setImageResource(R.drawable.menu_right);

        Title = (FreeTextView) addFreeView(new FreeTextView(context), -2, -1, icon, new int[]{RIGHT_OF, CENTER_VERTICAL}, righticon, new int[]{LEFT_OF});
        Title.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        Title.setTextColor(Color.BLACK);
        Title.setTextSizeFitSp(32.0f);
    }
}
