package org.ourcitylove.friendlycancer.layout;

/**
 * Created by Vegetable on 2016/8/1.
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import rx.Observable;
import rx.schedulers.Schedulers;

public class ScratchImageView extends ImageView{
    public static final float STROKE_WIDTH = 12.0F;
    private float mX;
    private float mY;
    private static final float TOUCH_TOLERANCE = 4.0F;
    private Bitmap mScratchBitmap;
    private Canvas mCanvas;
    private Path mErasePath;
    private Path mTouchPath;
    private Paint mBitmapPaint;
    private Paint mErasePaint;
    private Paint mGradientBgPaint;
    private BitmapDrawable mDrawable;
    private IRevealListener mRevealListener;
    private boolean mIsRevealed = false;
    private int Speed = 4;

    public ScratchImageView(Context context) {
        super(context);
        this.init();
    }

    public void setStrokeWidth(int multiplier) {
        this.mErasePaint.setStrokeWidth((float)multiplier * 12.0F);
    }

    public ScratchImageView(Context context, AttributeSet set) {
        super(context, set);
        this.init();
    }

    public ScratchImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init();
    }

    private void init() {
        this.mTouchPath = new Path();
        this.mErasePaint = new Paint();
        this.mErasePaint.setAntiAlias(true);
        this.mErasePaint.setDither(true);
        this.mErasePaint.setColor(Color.RED);
        this.mErasePaint.setStyle(Paint.Style.STROKE);
        this.mErasePaint.setStrokeJoin(Paint.Join.BEVEL);
        this.mErasePaint.setStrokeCap(Paint.Cap.ROUND);
        this.setStrokeWidth(6);
        this.mGradientBgPaint = new Paint();
        this.mErasePath = new Path();
        this.mBitmapPaint = new Paint(4);
//        this.mDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        this.setEraserMode();
    }

    public void setScratchDrawable(int res){
        Bitmap scratchBitmap = BitmapFactory.decodeResource(this.getResources(), res);
        this.mDrawable = new BitmapDrawable(this.getResources(), scratchBitmap);
    }

    public void setScratchSpeed(int Speed){ this.Speed = Speed;}

    public void AutoRevealed(){ this.clear(); }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.mScratchBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        this.mCanvas = new Canvas(this.mScratchBitmap);
        Rect rect = new Rect(0, 0, this.mScratchBitmap.getWidth(), this.mScratchBitmap.getHeight());
        this.mDrawable.setBounds(rect);
        int startGradientColor = Color.TRANSPARENT;
        int endGradientColor = Color.TRANSPARENT;
        this.mGradientBgPaint.setShader(new LinearGradient(0.0F, 0.0F, 0.0F, (float)this.getHeight(), startGradientColor, endGradientColor, Shader.TileMode.MIRROR));
        this.mCanvas.drawRect(rect, this.mGradientBgPaint);
        this.mDrawable.draw(this.mCanvas);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(this.mScratchBitmap, 0.0F, 0.0F, this.mBitmapPaint);
        canvas.drawPath(this.mErasePath, this.mErasePaint);
    }

    private void touch_start(float x, float y) {
        this.mErasePath.reset();
        this.mErasePath.moveTo(x, y);
        this.mX = x;
        this.mY = y;
    }

    private void clear() {
        int[] bounds = this.getImageBounds();
        int left = bounds[0];
        int top = bounds[1];
        int right = bounds[2];
        int bottom = bounds[3];
        int width = right - left;
        int height = bottom - top;
        int centerX = left + width / 2;
        int centerY = top + height / 2;
        left = centerX - width / 2;
        top = centerY - height / 2;
        right = left + width;
        bottom = top + height;
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        this.mCanvas.drawRect((float)left, (float)top, (float)right, (float)bottom, paint);
        this.checkRevealed();
        this.invalidate();
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - this.mX);
        float dy = Math.abs(y - this.mY);
        if(dx >= 4.0F || dy >= 4.0F) {
            this.mErasePath.quadTo(this.mX, this.mY, (x + this.mX) / 2.0F, (y + this.mY) / 2.0F);
            this.mX = x;
            this.mY = y;
            this.drawPath();
        }

        this.mTouchPath.reset();
        this.mTouchPath.addCircle(this.mX, this.mY, 30.0F, Path.Direction.CW);
    }

    private void drawPath() {
        this.mErasePath.lineTo(this.mX, this.mY);
        this.mCanvas.drawPath(this.mErasePath, this.mErasePaint);
        this.mTouchPath.reset();
        this.mErasePath.reset();
        this.mErasePath.moveTo(this.mX, this.mY);
        this.checkScratchPercent();
        this.checkRevealed();
    }

    public void reveal() {
    }

    private void touch_up() {
        this.drawPath();
    }

    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case 0:
                this.touch_start(x, y);
                this.invalidate();
                break;
            case 1:
                this.touch_up();
                this.invalidate();
                break;
            case 2:
                this.touch_move(x, y);
                this.invalidate();
        }
        return true;
    }

    public int getColor() {
        return this.mErasePaint.getColor();
    }

    public Paint getErasePaint() {
        return this.mErasePaint;
    }

    public void setEraserMode() {
        this.getErasePaint().setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    }

    public void setRevealListener(IRevealListener listener) {
        this.mRevealListener = listener;
    }

    public boolean isRevealed() {
        return this.mIsRevealed;
    }

    private void checkScratchPercent(){
        this.mRevealListener.onScratchPercent(getScratchedRatio(Speed));
    }
    private void checkRevealed() {
        if (!this.isRevealed() && this.mRevealListener != null) {
            int[] bounds = this.getImageBounds();
            int left = bounds[0];
            int top = bounds[1];
            int width = bounds[2] - left;
            int height = bounds[3] - top;
            Observable.just(left, top, width, height)
                    .toList()
                    .map(params -> {
                        Bitmap croppedBitmap = Bitmap.createBitmap(mScratchBitmap, params.get(0), params.get(1), params.get(2), params.get(3));
                        Bitmap emptyBitmap = Bitmap.createBitmap(croppedBitmap.getWidth(), croppedBitmap.getHeight(), croppedBitmap.getConfig());
                        return Boolean.valueOf(emptyBitmap.sameAs(croppedBitmap));
                    })
                    .filter(b -> !mIsRevealed)
                    .subscribe(hasRevealed -> {
                        mIsRevealed = hasRevealed.booleanValue();
                        if (mIsRevealed) {
                            mRevealListener.onRevealed(ScratchImageView.this);
                        }
                    });
        }
    }

    public int[] getImageBounds() {
        int paddingLeft = this.getPaddingLeft();
        int paddingTop = this.getPaddingTop();
        int paddingRight = this.getPaddingRight();
        int paddingBottom = this.getPaddingBottom();
        int vwidth = this.getWidth() - paddingLeft - paddingRight;
        int vheight = this.getHeight() - paddingBottom - paddingTop;
        int centerX = vwidth / 2;
        int centerY = vheight / 2;
        Drawable drawable = mDrawable;
        Rect bounds = drawable.getBounds();
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        if(width <= 0) {
            width = bounds.right - bounds.left;
        }

        if(height <= 0) {
            height = bounds.bottom - bounds.top;
        }

        if(height > vheight) {
            height = vheight;
        }

        if(width > vwidth) {
            width = vwidth;
        }

        ScaleType scaleType = this.getScaleType();
        int left;
        int top;
        switch(scaleType.ordinal()) {
            case 1:
                left = paddingLeft;
                top = centerY - height / 2;
                break;
            case 2:
                left = vwidth - paddingRight - width;
                top = centerY - height / 2;
                break;
            case 3:
                left = centerX - width / 2;
                top = centerY - height / 2;
                break;
            default:
                left = paddingLeft;
                top = paddingTop;
                width = vwidth;
                height = vheight;
        }

        return new int[]{left, top, left + width, top + height};
    }

    public float getScratchedRatio(int speed) {
        return null == mScratchBitmap ? 0 :
                Observable.just(speed)
                        .observeOn(Schedulers.computation())
                        .map(Speed -> {
                            final int width = mScratchBitmap.getWidth();
                            final int height = mScratchBitmap.getHeight();

                            int count = 0;
                            for (int i = 0; i < width; i += Speed)
                                for (int j = 0; j < height; j += Speed)
                                    if (0 == Color.alpha(mScratchBitmap.getPixel(i, j))) count++;


                            float completed = (float) count / ((width / Speed) * (height / Speed)) * 100;

                            return completed;
                        })
                        .toBlocking()
                        .single();
    }


    public interface IRevealListener {
        void onRevealed(ScratchImageView var1);
        void onScratchPercent(float var1);
    }
}
