package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.view.inputmethod.EditorInfo;

/**
 * Created by Vegetable on 2016/3/31.
 */
public class SearchBarLayout extends LabelAndEdit {
    public SearchBarLayout(Context context) {
        super(context);
        titlelabel.setVisibility(GONE);
        mirror.setVisibility(VISIBLE);
        searchText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
    }
}
