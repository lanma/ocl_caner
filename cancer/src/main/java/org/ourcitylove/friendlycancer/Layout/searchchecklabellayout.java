package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/3/31.
 */
public class searchchecklabellayout extends FreeLayout{
    public ImageView checkicon;
    public FreeTextView checktext;
    public searchchecklabellayout(Context context) {
        super(context);
        setPicSize(640,-2,TO_WIDTH);
        setFreeLayoutFW();
        setPadding(this,20,0,20,0);

        String androidassets = "file:///android_asset/";

        checktext = new FreeTextView(context);
//        mlayout.addView(checktext,-1,-1);
        checktext = (FreeTextView)addFreeView(new FreeTextView(context),-2,-1,new int[]{CENTER_IN_PARENT});
        checktext.setGravity(Gravity.CENTER);
        checktext.setTextSizeFitSp(30);
        checktext.setTextColor(ContextCompat.getColor(context, R.color.screen_checklabel));

        checkicon = new ImageView(context);
//        mlayout.addView(checkicon,30,30);
        checkicon = (ImageView)addFreeView(new ImageView(context),30,30,checktext,new int[]{LEFT_OF,CENTER_VERTICAL});
        setMargin(checkicon,0,0,05,0);
        checkicon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        Glide.with(context).load(androidassets + "image/search_check_no.png").fitCenter().into(checkicon);
    }
}
