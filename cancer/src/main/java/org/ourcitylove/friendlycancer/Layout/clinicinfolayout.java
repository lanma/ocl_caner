package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.StringRes;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Vegetable on 2016/4/4.
 */
public class clinicinfolayout extends FreeLayout{
    public FreeTextView phonenumber,address;
    public FreeTabletextView service;
    public TextView title,distance;
    public ViewGroup call,map,oldplace;
    public ImageView Certi;
    public ViewGroup screen;

    public clinicinfolayout(Context context) {
        super(context);
        setPicSize(640,-2,TO_WIDTH);
        setPadding(this,0,20,0,0);
        setBackgroundColor(Color.WHITE);

        View infotitle = addFreeView(inflate(context,R.layout.clinicinfotitle,null),-1,-2);
        setMargin(infotitle,20,0,20,10);
        Certi = ButterKnife.findById(infotitle, R.id.Certi);
        title = ButterKnife.findById(infotitle, R.id.clinic_title);
        distance = ButterKnife.findById(infotitle, R.id.distance);

        View infolayout = addFreeView(inflate(context, R.layout.clinicinfobutton, null),640,-2,infotitle,new int[]{BELOW,CENTER_HORIZONTAL});
        call = ButterKnife.findById(infolayout, R.id.phonecall);
        map = ButterKnife.findById(infolayout, R.id.navi_map);
        oldplace = ButterKnife.findById(infolayout, R.id.favor);
        setMargin(infolayout,20,0,20,0);

        clinicscreenprojectlayout screenlayout = (clinicscreenprojectlayout)addFreeView(new clinicscreenprojectlayout(context),-1,-2,infolayout,new int[]{BELOW,CENTER_HORIZONTAL});
        screen = screenlayout.screen;
        phonenumber = screenlayout.phonenumber;
        address = screenlayout.address;
        setMargin(screenlayout,20,0,20,0);

        clinicinfotitlelayout servicetitle = (clinicinfotitlelayout) addFreeView(new clinicinfotitlelayout(context), -1, -2, screenlayout, new int[]{BELOW});
        servicetitle.title.setText(R.string.clinicinfo_service);
        setMargin(servicetitle, 0, 20, 0, 0);

        service = (FreeTabletextView) addFreeView(new FreeTabletextView(context),-1,-2,servicetitle,new int[]{BELOW});
        service.setTextSizeFitSp(30);
        service.setTextColor(Color.BLACK);

        setMargin(service,20,20,20,0);

        clinicinfotitlelayout servicetimetitle = (clinicinfotitlelayout)addFreeView(new clinicinfotitlelayout(context),-1,-2,service,new int[]{BELOW});
        servicetimetitle.title.setText(R.string.clinicinfo_service_time);
        setMargin(servicetimetitle, 0, 20, 0, 0);

        clinictimetable servicetimetable = (clinictimetable)addFreeView(new clinictimetable(context),-1,-2,servicetimetitle,new int[]{BELOW});

        ImageView bottomview = (ImageView)addFreeView(new ImageView(context),-1,-2,servicetimetable,new int[]{BELOW});
        bottomview.setTag(null);
        bottomview.setScaleType(ImageView.ScaleType.FIT_CENTER);
        Glide.with(context).load(R.drawable.menu_info_bg).asBitmap().fitCenter().into(bottomview);
        setMargin(bottomview,0,20,0,0);

    }

    public class FreeTabletextView extends FreeLayout{
        private String[] prefix = {"A","B","C","D","E","F","G","H","I"};
        private List<FreeTextView> Title = new ArrayList();
        private List<FreeTextView> Content = new ArrayList();
        private final LinearLayout mlayout;
        private float TextSize = 20;
        @ColorInt
        private int TextColor = 0X000;

        public FreeTabletextView(Context context) {
            super(context);
            setPicSize(640, -2, TO_WIDTH);
            setFreeLayoutFW();
            setPadding(40,0,0,0);
            mlayout = (LinearLayout)addFreeView(new LinearLayout(context),-1, -2);
            mlayout.setOrientation(LinearLayout.VERTICAL);
        }

        public void setText(@StringRes int ResId){setText(getResources().getString(ResId));}
        public void setText(String Text){setText(new String[]{Text});}
        public void setText(Collection<? super String> Text){setText( Text.toArray(new String[0]));}

        public void setText(String[] Text) {
            for (int i = 0; i < Text.length; i++) {
                LinearLayout nlayout = new LinearLayout(getContext());
                nlayout.setOrientation(LinearLayout.HORIZONTAL);
                mlayout.addView(nlayout, -1, -2);
                FreeTextView title = new FreeTextView(getContext());
                FreeTextView content = new FreeTextView(getContext());
                Title.add(title);
                Content.add(content);
                nlayout.addView(title);
                nlayout.addView(content);

                title.setText(prefix[i]);
                title.setTextSizeFitSp(TextSize);
                title.setTextColor(TextColor);
                LinearLayout.LayoutParams mparams = (LinearLayout.LayoutParams) title.getLayoutParams();
                mparams.rightMargin = 5;

                content.setText(Text[i]);
                content.setTextSizeFitSp(TextSize);
                content.setTextColor(TextColor);
            }
        }

        public FreeTabletextView setTextSizeFitSp(float TextSizefitSp){TextSize = TextSizefitSp;return this;}
        public FreeTabletextView setTextColor(@ColorInt int Color){TextColor = Color;return this;}

    }


}
