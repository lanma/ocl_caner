package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.CollapsibleActionView;
import android.support.v7.widget.GridLayout;
import android.text.style.AlignmentSpan;
import android.view.Gravity;
import android.view.View;

import com.james.views.FreeLayout;
import com.james.views.FreeTextButton;

import org.ourcitylove.friendlycancer.R;

public class SearchButtonLayout extends FreeLayout{
    GridLayout gridLayout;
    Context context;
    protected final int columncount = 4;
    public SearchButtonLayout(Context context) {
        super(context);
        this.context = context;
        setPicSize(640);
        setFreeLayoutFW();
        setPadding(this,20,10,20,0);
        gridLayout = (GridLayout)addFreeView(new GridLayout(context),640,-1);
        gridLayout.setColumnCount(columncount);
        gridLayout.setOrientation(GridLayout.HORIZONTAL);
        gridLayout.setUseDefaultMargins(false);
    }

    public void addbutton(String[] title) {
        for (int index = 0; index < title.length; index++) {
            FreeTextButton button = new FreeTextButton(context);


            GridLayout.LayoutParams param = new GridLayout.LayoutParams(
                    GridLayout.spec(GridLayout.UNDEFINED, 1f),
                    GridLayout.spec(GridLayout.UNDEFINED, 1f)
            );
            button.setLayoutParams(param);
            setMargin(button, 20, 0, 20, 20);
            setPadding(button, 0, 10, 0, 10);

            button.setTextColor(ContextCompat.getColor(context, R.color.gray02));
            button.setBackgroundResource(R.drawable.searchbtn_bg);
            button.setText(title[index]);
            button.setTag(title[index]);
            button.setGravity(Gravity.CENTER);
            button.setTextSizeFitSp(23);
            gridLayout.addView(button);
        }
    }

    public void setMargin(View view, int left, int top, int right, int bottom) {
        ((MarginLayoutParams)view.getLayoutParams()).setMargins(left * this.datum / this.picSize, top * this.datum / this.picSize, right * this.datum / this.picSize, bottom * this.datum / this.picSize);
    }
}
