package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.view.Gravity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/4/7.
 */
public class dialognodatalayout extends dialogbglayout{
    public dialognodatalayout(Context context) {
        super(context);
        ImageView bg = (ImageView)addFreeView(new ImageView(context),320,-2,cancel,new int[]{BELOW,CENTER_IN_PARENT});
        bg.setScaleType(ImageView.ScaleType.FIT_CENTER);
        bg.setTag(null);
        Glide.with(context).load(Uri.parse(assetPath + "image/item_icon_nodata.png")).fitCenter().into(bg);

        FreeTextView nodata = (FreeTextView)addFreeView(new FreeTextView(context),-2,-2,bg,new int[]{ALIGN_TOP,CENTER_IN_PARENT});
        nodata.setGravity(Gravity.CENTER);
        nodata.setTextSizeFitSp(40);
        nodata.setTextColor(Color.WHITE);
        nodata.setText(R.string.nodata);
        setPadding(nodata,0,25,0,0);
    }
}
