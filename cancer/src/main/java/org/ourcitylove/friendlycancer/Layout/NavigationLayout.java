package org.ourcitylove.friendlycancer.layout;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.view.Gravity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/3/28.
 */
public class NavigationLayout extends FreeLayout{
    private String assetPath = "file:///android_asset/";

    public ImageView back,icon;
    public FreeTextView title,map;

    public NavigationLayout(Context context) {
        super(context);
        setPicSize(640,-2,TO_WIDTH);
//        setFreeLayoutFW();
        setBackgroundColor(Color.rgb(158,69,180));

        //back
        back = (ImageView)addFreeView(new ImageView(context),50,50,new int[]{CENTER_VERTICAL});
        setMargin(back,25,15,0,15);
        back.setScaleType(ImageView.ScaleType.FIT_CENTER);
        back.setTag(null);
        Glide.with(context).load(Uri.parse(assetPath + "image/navi_back_button.png")).into(back);
        back.setOnClickListener(view -> ((Activity)context).finish());

        FreeLayout mlayout = (FreeLayout)addFreeView(new FreeLayout(context),-2,-2,back,new int[]{CENTER_IN_PARENT});

        //icon
        icon = (ImageView)mlayout.addFreeView(new ImageView(context),50,50,new int[]{CENTER_VERTICAL});
        setMargin(icon,0,0,20,0);
        icon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        icon.setTag(null);
        //title
        title = (FreeTextView)mlayout.addFreeView(new FreeTextView(context),450,80,icon,new int[]{RIGHT_OF,CENTER_VERTICAL});
        title.setTextSizeFitSp(45);
        title.setTextColor(Color.WHITE);
        title.setGravity(Gravity.CENTER);
        setMargin(title,0,15,0,15);

        //map
        map = (FreeTextView)addFreeView(new FreeTextView(context),90,80,new int[]{ALIGN_PARENT_RIGHT,CENTER_VERTICAL});
        map.setTextSizeFitSp(37);
        map.setTextColor(Color.WHITE);
        map.setGravity(Gravity.CENTER);
        map.setText(getResources().getText(R.string.map));
        setMargin(map,0,15,0,15);

    }
}
