package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;

import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/4/5.
 */
public class clinicinfotitlelayout extends FreeLayout{
    protected FreeTextView title;
    public clinicinfotitlelayout(Context context) {
        super(context);
        setPicSize(640,100,TO_WIDTH);
        setBackgroundColor(ContextCompat.getColor(context, R.color.mainColor));

        title = (FreeTextView)addFreeView(new FreeTextView(context),-1,-2,new int[]{CENTER_IN_PARENT});
        title.setGravity(Gravity.CENTER);
        title.setTextSizeFitSp(35);
        title.setTextColor(Color.WHITE);
        setMargin(title,0,15,0,15);
    }
}
