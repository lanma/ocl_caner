package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.view.Gravity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/3/28.
 */
public class normalcolorcell extends normalcell{

    public normalcolorcell(Context context) {
        super(context);
        setPicSize(640);
        setFreeLayoutFW();
        setBackgroundColor(Color.WHITE);
        setMargin(this,10,15,10,0);

        contentlayout.removeAllViews();
        removeView(contentlayout);
        addFreeView(contentlayout, -2, -2, new int[]{CENTER_VERTICAL});

        //icon
        image = (ImageView)contentlayout.addFreeView(image,100,100,new int[]{CENTER_VERTICAL});
        image.setTag(null);
        image.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        icon.setBackgroundColor(Color.BLACK);
        setMargin(image,30,15,30,15);
        //title
        title = (FreeTextView)contentlayout.addFreeView(title,-2,-1,new int[]{CENTER_VERTICAL},image,new int[]{RIGHT_OF});
        title.setTextSizeFitSp(45);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);

        //right button
        ImageView right = (ImageView)contentlayout.addFreeView(new ImageView(context),75,75,new int[]{CENTER_VERTICAL,ALIGN_PARENT_RIGHT});
        right.setTag(null);
        right.setScaleType(ImageView.ScaleType.CENTER_CROP);
        setMargin(right,0,15,15,15);
        Glide.with(context).load(R.drawable.menu_right).diskCacheStrategy(DiskCacheStrategy.SOURCE).fitCenter().into(right);
    }
}
