package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;

import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/4/10.
 */
public class button extends FreeTextView{
    public button(Context context) {
        super(context);
        setPadding(0,25,0,25);
        setTextColor(Color.WHITE);
        setTextSizeFitSp(30);
        setGravity(Gravity.CENTER);
        setBackgroundColor(ContextCompat.getColor(context, R.color.purple));
    }
}
