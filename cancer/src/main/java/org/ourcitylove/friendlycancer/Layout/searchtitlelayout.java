package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/3/31.
 */
public class searchtitlelayout extends FreeLayout{
    public FreeLayout contentlayout;
    public FreeTextView title;
    public ImageView icon,img;
    public ImageView leftline,rightline;
    public searchtitlelayout(Context context) {
        super(context);
        setPicSize(640,-2,TO_WIDTH);
        setFreeLayoutFW();
        setPadding(this,20,20,20,20);

        String androidassets = "file:///android_asset/";

        contentlayout = (FreeLayout)addFreeView(new FreeLayout(context),-1,-2);

        title = (FreeTextView)contentlayout.addFreeView(new FreeTextView(context),-2,-2,new int[]{CENTER_IN_PARENT});
        title.setGravity(Gravity.CENTER);
        title.setTextSizeFitSp(28.5f);
        title.setTextColor(Color.BLACK);

        icon = (ImageView)contentlayout.addFreeView(new ImageView(context),50,50,new int[]{ALIGN_PARENT_RIGHT,CENTER_VERTICAL});
        icon.setTag(null);
        icon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        Glide.with(context).load(Uri.parse(androidassets + "image/item_icon_help.png")).fitCenter().into(icon);

        leftline = (ImageView)contentlayout.addFreeView(new ImageView(context),-1,5,title,new int[]{LEFT_OF,CENTER_VERTICAL});
        leftline.setBackgroundColor(ContextCompat.getColor(context, R.color.light_pink));
        setMargin(leftline,0,0,15,0);

        rightline = (ImageView)contentlayout.addFreeView(new ImageView(context),-1,5,title,new int[]{RIGHT_OF,CENTER_VERTICAL},icon,new int[]{LEFT_OF});
        rightline.setBackgroundColor(ContextCompat.getColor(context, R.color.light_pink));
        setMargin(rightline,15,0,15,0);


    }
}
