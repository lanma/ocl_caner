package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vegetable on 2016/4/6.
 */
public class clinictimetable extends FreeLayout{
    private Context context;

    private List<Map> map = new ArrayList(){{
        add(Type(""  ,"　$$上午$$下午$$晚上","$$上午$$下午$$晚上","#ff9aa2$$#ff9aa2$$#ff9aa2$$#ff9aa2","lefttop$$$$$$righttop"));
        add(Type("一","一$$休診$$休診$$休診","$$上午$$下午$$晚上","#ff9aa2$$#ffcfc5$$#ffcfc5$$#ffcfc5","$$$$$$"));
        add(Type("二","二$$休診$$休診$$休診","$$上午$$下午$$晚上","#ff9aa2$$#ffcfc5$$#ffcfc5$$#ffcfc5","$$$$$$"));
        add(Type("三","三$$休診$$休診$$休診","$$上午$$下午$$晚上","#ff9aa2$$#ffcfc5$$#ffcfc5$$#ffcfc5","$$$$$$"));
        add(Type("四","四$$休診$$休診$$休診","$$上午$$下午$$晚上","#ff9aa2$$#ffcfc5$$#ffcfc5$$#ffcfc5","$$$$$$"));
        add(Type("五","五$$休診$$休診$$休診","$$上午$$下午$$晚上","#ff9aa2$$#ffcfc5$$#ffcfc5$$#ffcfc5","$$$$$$"));
        add(Type("六","六$$休診$$休診$$休診","$$上午$$下午$$晚上","#ff9aa2$$#ffcfc5$$#ffcfc5$$#ffcfc5","$$$$$$"));
        add(Type("日","日$$休診$$休診$$休診","$$上午$$下午$$晚上","#ff9aa2$$#ffcfc5$$#ffcfc5$$#ffcfc5","leftbottom$$$$$$rightbottom"));
    }};
    public clinictimetable(Context context) {
        super(context);
        this.context = context;
        setPicSize(640,-2,TO_WIDTH);
        setPadding(0,20,0,0);


        LinearLayout gridLayout = (LinearLayout) addFreeView(new LinearLayout(context),-1,-2,new int[]{CENTER_HORIZONTAL});
        gridLayout.setOrientation(LinearLayout.VERTICAL);
        setPadding(gridLayout,40,0,40,0);
        addtable(map,gridLayout);
    }

    private void addtable(List<Map> data, ViewGroup rootlayout) {
        for(Map<String,String> mdata:data) {
            String group = mdata.get("GROUP");
            String[] mtitle = mdata.get("TITLE").split("\\$\\$");
            String[] mbgcolor = mdata.get("BGCOLOR").split("\\$\\$");
            String[] mtag = mdata.get("TAG").split("\\$\\$");
            String[] mradius = mdata.get("CORNERRADIUS").split("\\$\\$");
            LinearLayout contentlayout = new LinearLayout(context);
            contentlayout.setOrientation(LinearLayout.HORIZONTAL);
            contentlayout.setTag(group);
            rootlayout.addView(contentlayout,-1,-2);
            for (int index = 0; index < mtitle.length; index++) {
                FreeTextView mview = new FreeTextView(context);
                LinearLayout.LayoutParams mparams = new LinearLayout.LayoutParams(-1,-2,1);
                mparams.setMargins(2,2,2,2);
                mview.setText(mtitle[index]);
                mview.setTag(mtag[index]);
                mview.setTextSizeFitSp(30);
                mview.setTextColor(ContextCompat.getColor(context,mtitle[index].equals("休診")?R.color.textred:R.color.black));
                mview.setGravity(Gravity.CENTER);
                GradientDrawable shapeDrawable = new GradientDrawable();
                shapeDrawable.setColor(Color.parseColor(mbgcolor[index]));
                float r1 = 8f,r0=0;
                float[] radius = new float[8];
                Arrays.fill(radius,r0);
                if(mradius.length != 0) {
                    if (mradius[index].matches("lefttop")) Arrays.fill(radius, 0, 2, r1);
                    if (mradius[index].matches("righttop")) Arrays.fill(radius, 2, 4, r1);
                    if (mradius[index].matches("rightbottom")) Arrays.fill(radius, 4, 6, r1);
                    if (mradius[index].matches("leftbottom")) Arrays.fill(radius, 6, 8, r1);
                }
                shapeDrawable.setCornerRadii(radius);
                mview.setBackground(shapeDrawable);
                contentlayout.addView(mview,mparams);
            }
        }
    }

    private Map Type(String group,String title,String tag,String backgroundcolor,String cornerradius) {
        return new HashMap(){{
        put("GROUP", group);
        put("TITLE", title);
        put("BGCOLOR", backgroundcolor);
        put("TAG", tag);
        put("CORNERRADIUS",cornerradius);
        }};
    }
}
