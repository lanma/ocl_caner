package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.widget.ImageView;

import com.james.views.FreeLayout;

/**
 * Created by Vegetable on 2016/3/30.
 */
public class normalgridcell extends FreeLayout{
    public ImageView appicon;
    public normalgridcell(Context context) {
        super(context);
        setPicSize(640);
        setFreeLayoutFW();

        appicon = (ImageView)addFreeView(new ImageView(context),320,-2,new int[]{CENTER_IN_PARENT});
        appicon.setTag(null);
        appicon.setScaleType(ImageView.ScaleType.CENTER_CROP);
        setMargin(appicon,0,50,0,0);
    }
}
