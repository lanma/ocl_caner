package org.ourcitylove.friendlycancer.layout;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;

import org.ourcitylove.friendlycancer.utils.Utils;

/**
 * Created by Vegetable on 2016/4/8.
 */
public class titlenoiconwithimage extends searchtitlelayout{
    public titlenoiconwithimage(Context context) {
        super(context);
        setPicSize(640,-2,TO_WIDTH);

        img = (ImageView)addFreeView(new ImageView(context),160,-2,new int[]{ALIGN_PARENT_RIGHT});
        img.setScaleType(ImageView.ScaleType.FIT_CENTER);
        img.setTag(null);
        setMargin(img,0,0,15,0);

        removeView(contentlayout);
        addFreeView(contentlayout, -1, -2);


        LayoutParams mparams = (LayoutParams)contentlayout.getLayoutParams();
        mparams.addRule(BELOW,img.getId());
        contentlayout.setLayoutParams(mparams);
        contentlayout.setY(contentlayout.getY()- Utils.dpToPx((Activity)context, 15));

        icon.setVisibility(GONE);

    }
}
