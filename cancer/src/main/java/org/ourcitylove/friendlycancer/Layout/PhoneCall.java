package org.ourcitylove.friendlycancer.layout;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.james.views.FreeLayout;
import com.james.views.FreeTextButton;

import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.helper.AddViewHelper;

/**
 * Created by Vegetable on 2016/3/30.
 */
public class PhoneCall extends FreeLayout{
    private LinearLayout mlayout;

    public FreeTextButton cancel,call;

    public Dialog dialog;

    public PhoneCall(Context context,String[] modeltype,String[] modelcontent) {
        super(context);
        setPicSize(640);
        setFreeLayoutFW();
        setBackgroundResource(R.drawable.phonecall);
        mlayout = (LinearLayout)addFreeView(new LinearLayout(context),-1,-2);
        mlayout.setOrientation(LinearLayout.VERTICAL);
        mlayout.setGravity(Gravity.CENTER);
        setPadding(mlayout,0,30,0,0);
        AddViewHelper.addView(mlayout,modeltype,modelcontent,true);
        LayoutParams mparams = (LayoutParams)mlayout.findViewWithTag("IMAGE").getLayoutParams();
        mparams.addRule(CENTER_HORIZONTAL);
        mparams.width = mparams.width *3/4;
        mlayout.findViewWithTag("IMAGE").setLayoutParams(mparams);


        //隔線
        ImageView horizontal,vertical;

        horizontal = (ImageView)addFreeView(new ImageView(context),-1,2,mlayout,new int[]{BELOW});
        setMargin(horizontal, 0,5,0,0);
        horizontal.setBackgroundColor(ContextCompat.getColor(context,R.color.backgroundgray));

        vertical = (ImageView)addFreeView(new ImageView(context),2,100,mlayout,new int[]{BELOW,CENTER_HORIZONTAL});
        vertical.setBackgroundColor(ContextCompat.getColor(context, R.color.backgroundgray));

        cancel = (FreeTextButton)addFreeView(new FreeTextButton(context),319,100,horizontal,new int[]{BELOW,ALIGN_LEFT});
        cancel.setBackgroundColor(Color.TRANSPARENT);
        cancel.setGravity(Gravity.CENTER);
        cancel.setTypeface(null, Typeface.BOLD);
        cancel.setTextSizeFitSp(40);
        cancel.setTextColor(Color.rgb(217,0,0));
        cancel.setText(R.string.cancel);
        cancel.setOnClickListener(v->{if(dialog.isShowing())dialog.dismiss();});

        call = (FreeTextButton)addFreeView(new FreeTextButton(context),319,100,horizontal,new int[]{BELOW,ALIGN_RIGHT});
        call.setBackgroundColor(Color.TRANSPARENT);
        call.setTypeface(null,Typeface.BOLD);
        call.setGravity(Gravity.CENTER);
        call.setTextSizeFitSp(40);
        call.setTextColor(Color.BLACK);
        call.setText(R.string.call);

    }

    public void builddialog(){
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
}
