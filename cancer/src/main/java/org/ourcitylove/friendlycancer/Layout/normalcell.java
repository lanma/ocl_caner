package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.ImageView;

import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

/**
 * Created by Vegetable on 2016/3/23.
 */
public class normalcell extends FreeLayout{
    public ImageView image;
    public FreeTextView title;
    public FreeLayout contentlayout;
    public normalcell(Context context){
        super(context);
        setPicSize(640);
        setFreeLayoutFW();
        ImageView bg = (ImageView)addFreeView(new ImageView(context),-1,120);
        bg.setBackgroundColor(Color.WHITE);
        setMargin(this,0,0,0,5);

        contentlayout = (FreeLayout)addFreeView(new FreeLayout(context),500,120,new int[]{CENTER_IN_PARENT});
        contentlayout.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);


        image = (ImageView)contentlayout.addFreeView(new ImageView(context),100,100,new int[]{CENTER_VERTICAL});
        image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        setMargin(image,20,0,0,0);

        title = (FreeTextView)contentlayout.addFreeView(new FreeTextView(context),-2,-1,image,new int[]{RIGHT_OF,CENTER_VERTICAL});
        title.setGravity(Gravity.CENTER);
        setPadding(title,20,0,0,0);
        title.setTextSizeFitSp(50);
        title.setTextColor(Color.BLACK);

    }
}
