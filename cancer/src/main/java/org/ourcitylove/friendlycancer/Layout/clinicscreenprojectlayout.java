package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Vegetable on 2016/4/5.
 */
public class clinicscreenprojectlayout extends FreeLayout{

    private Context context;

    protected FreeTextView phonenumber,address;
    protected LinearLayout screen;
    public clinicscreenprojectlayout(Context context) {
        super(context);
        this.context = context;
        setPicSize(640,-2,TO_WIDTH);
        setPadding(20,20,20,20);


        FreeTextView project = (FreeTextView)addFreeView(new FreeTextView(context),-1,-2);
        project.setTextSizeFitSp(30);
        project.setTextColor(Color.BLACK);
        project.setText(R.string.screen_title);
        setMargin(project,0,0,0,20);

        screen = (LinearLayout)addFreeView(new LinearLayout(context),-1,-2,project,new int[]{BELOW});
        screen.setGravity(Gravity.CENTER);
        screen.setOrientation(LinearLayout.HORIZONTAL);
        setPadding(screen,0,0,0,10);
        addsubview(screen);

        FreeTextView phonetitle,addresstitle;
        phonetitle = (FreeTextView)addFreeView(new FreeTextView(context),-2,-1,screen,new int[]{BELOW});
        phonetitle.setTextSizeFitSp(30);
        phonetitle.setTextColor(Color.BLACK);
        phonetitle.setText(R.string.screen_phone);

        phonenumber = (FreeTextView)addFreeView(new FreeTextView(context),-2,-1,phonetitle,new int[]{RIGHT_OF,ALIGN_TOP});
        phonenumber.setTextSizeFitSp(30);
        phonenumber.setTextColor(Color.BLACK);
        phonenumber.setText("(02)2713-5211");



        addresstitle = (FreeTextView)addFreeView(new FreeTextView(context),-2,-1,phonetitle,new int[]{BELOW}, phonenumber, new int[]{BELOW});
        addresstitle.setTextSizeFitSp(30);
        addresstitle.setTextColor(Color.BLACK);
        addresstitle.setText(R.string.screen_address);
        setMargin(addresstitle,0,15,0,0);

        address = (FreeTextView)addFreeView(new FreeTextView(context),-2,-1,addresstitle,new int[]{RIGHT_OF,ALIGN_TOP});
        address.setTextSizeFitSp(30);
        address.setTextColor(Color.BLACK);
        address.setText("臺北市松山區敦化北路199號");
    }

    private void addsubview(LinearLayout content) {
        for (int ind=0;ind<4;ind++) {
            LinearLayout.LayoutParams mparams = new LinearLayout.LayoutParams(-1,-1,1);
            mparams.setMargins(5,0,5,0);
            FreeTextView type = new FreeTextView(context);
            type.setTextScaleX(1.02f);
            type.setPadding(0, 5, 0, 5);
            type.setTextColor(Color.WHITE);
            type.setBackgroundColor(Color.TRANSPARENT);
            type.setGravity(Gravity.CENTER);
            type.setTextSizeFitSp(21);
            type.setTextColor(Color.WHITE);
            content.addView(type,mparams);
        }
    }
}
