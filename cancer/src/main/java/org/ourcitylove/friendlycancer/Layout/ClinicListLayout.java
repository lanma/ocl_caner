package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;

import com.bumptech.glide.Glide;
import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Vegetable on 2016/3/31.
 */
public class ClinicListLayout extends FreeLayout {

    private static Hashtable<String, Typeface> fontCache = new Hashtable<String, Typeface>();
    private Context context;
    public FreeTextView title, distance;
    public LinearLayout contenttype;

    public ClinicListLayout(Context context) {
        super(context);
        this.context = context;
        setPicSize(640, 200, TO_WIDTH);
        setFreeLayoutFW();
        setMargin(this, 15, 10, 15, 0);
        setBackgroundColor(Color.WHITE);

//        String assetPath = "file:///android_asset/";

        ImageView righticon = (ImageView) addFreeView(new ImageView(context), 50, 50, new int[]{ALIGN_PARENT_RIGHT, CENTER_VERTICAL});
        righticon.setTag(null);
        righticon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        Glide.with(context).load(R.drawable.menu_right).centerCrop().into(righticon);

        title = (FreeTextView) addFreeView(new FreeTextView(context), -1, -2, righticon, new int[]{LEFT_OF});
        title.setTextSizeFitSp(30f);
        title.setTextColor(Color.BLACK);
        title.setLines(2);
        title.setMaxLines(2);
        title.setEllipsize(TextUtils.TruncateAt.END);
        title.setGravity(Gravity.LEFT);
        title.setLineSpacing(10,1);
        setMargin(title,10,5,0,5);

        distance = (FreeTextView) addFreeView(new FreeTextView(context), 125, -1, title, new int[]{BELOW, ALIGN_PARENT_RIGHT});
        distance.setGravity(Gravity.CENTER_VERTICAL);
        distance.setTextSizeFitSp(20);
        distance.setTextColor(ContextCompat.getColor(context, R.color.gray02));
        distance.setText("約800公尺");

        contenttype = (LinearLayout) addFreeView(new LinearLayout(context), -1, -1, title,new int[]{BELOW},distance,new int[]{LEFT_OF});
        contenttype.setOrientation(TableLayout.HORIZONTAL);
        setMargin(contenttype,10,0,0,10);
        addsubview(contenttype);
    }
    public void resetcontenttype(){
        contenttype.removeAllViews();
        addsubview(contenttype);
    }

    private void addsubview( LinearLayout content) {
        for (int i=0;i<4;i++) {
            LinearLayout.LayoutParams mparams = new LinearLayout.LayoutParams(-1,-1,1);
            mparams.setMargins(5,0,5,0);
            FreeTextView type = new FreeTextView(context);
            type.setPadding(0, 5, 0, 5);
            type.setTextColor(Color.WHITE);
            type.setTag(i);
            type.setBackgroundColor(Color.TRANSPARENT);
            type.setGravity(Gravity.CENTER);
            type.setTextSizeFitSp(18);
            type.setTextColor(Color.WHITE);
            type.setTypeface(fontCache.get("fonts/BGTR00EU.TTF"));
//            GradientDrawable shape = (GradientDrawable)type.getBackground().getCurrent();
            content.addView(type,mparams);
        }
    }
}
