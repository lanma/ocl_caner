package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import com.james.views.FreeTextButton;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/3/31.
 */
public class searchmultibuttonlayout extends SearchButtonLayout {
    public searchmultibuttonlayout(Context context) {
        super(context);
        this.context = context;
        setPicSize(640, -2, TO_WIDTH);
        setFreeLayoutFW();
//        setPadding(this, 20, 10, 20, 0);
    }

    @Override
    public void addbutton(String[] title) {
        super.addbutton(title);
        FreeTextButton button = (FreeTextButton) gridLayout.findViewWithTag(title[0]);
        button.setEnabled(false);
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setTextColor(ContextCompat.getColor(context, R.color.gray01));
    }
}
