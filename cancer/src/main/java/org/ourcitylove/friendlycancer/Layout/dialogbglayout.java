package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.widget.ImageView;
import android.app.Dialog;

import com.bumptech.glide.Glide;
import com.james.views.FreeLayout;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/4/7.
 */
public class dialogbglayout extends FreeLayout{
    public Dialog dialog;
    public ImageView cancel;
    protected  String assetPath = "file:///android_asset/";
    public dialogbglayout(Context context) {
        super(context);
        setPicSize(640,-2,TO_WIDTH);
        setPadding(10,10,10,10);

        GradientDrawable gradientDrawable = (GradientDrawable) ContextCompat.getDrawable(context, R.drawable.dialogbg);
        gradientDrawable.setColor(ContextCompat.getColor(context,R.color.mainColor));
        gradientDrawable.setAlpha(225);
        setBackground(gradientDrawable);

        cancel = (ImageView)addFreeView(new ImageView(context),50,50,new int[]{ALIGN_PARENT_TOP,ALIGN_PARENT_RIGHT});
        cancel.setTag(null);
        cancel.setScaleType(ImageView.ScaleType.FIT_CENTER);
        cancel.setOnClickListener(v->{if(dialog.isShowing())dialog.dismiss();});
        Glide.with(context).load(Uri.parse(assetPath + "image/item_icon_cancel.png")).into(cancel);
    }

    public void buildDialog(){
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(this);
        dialog.show();
    }
}
