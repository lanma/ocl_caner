package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.widget.ImageView;

import com.james.views.FreeEditText;
import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.listener.onTextChange;

public class LabelAndEdit extends FreeLayout {
    public FreeEditText searchText;

    protected ImageView mirror;
    public FreeTextView titlelabel;
    public LabelAndEdit(Context context) {
        super(context);
        setPicSize(640,-2,TO_WIDTH);
        setFreeLayoutFW();
        setPadding(this,20,10,20,10);

        //bg
        ImageView bg = (ImageView)addFreeView(new ImageView(context),-1,80);
        bg.setBackgroundColor(ContextCompat.getColor(context, R.color.gray04));

        titlelabel = (FreeTextView)addFreeView(new FreeTextView(context),150,80);
        titlelabel.setGravity(Gravity.CENTER);
        titlelabel.setBackgroundColor(Color.WHITE);
        titlelabel.setTextColor(ContextCompat.getColor(context,R.color.textgray));
        titlelabel.setTextSizeFitSp(36.5f);
//        setPadding(titlelabel,0,0,0,0);


        //放大鏡
        mirror = (ImageView)addFreeView(new ImageView(context),80,80,titlelabel,new int[]{RIGHT_OF,CENTER_VERTICAL});
        mirror.setBackgroundColor(ContextCompat.getColor(context, R.color.gray04));
        setPadding(mirror,15,25,0,25);
        mirror.setImageResource(R.drawable.search_icon);
        mirror.setVisibility(GONE);

        //ＸＸ
        ImageView xx = (ImageView)addFreeView(new ImageView(context),80,80,new int[]{CENTER_VERTICAL,ALIGN_PARENT_RIGHT});
        setPadding(xx, 0,15,0,15);
        xx.setBackgroundColor(ContextCompat.getColor(context, R.color.gray04));
        xx.setScaleType(ImageView.ScaleType.FIT_CENTER);
        xx.setImageResource( R.drawable.ic_cross);
        xx.setVisibility(GONE);
        xx.setOnClickListener(v-> searchText.setText(""));

        //搜尋文字
        searchText = (FreeEditText)addFreeView(new FreeEditText(context),-1,80,mirror,new int[]{RIGHT_OF,CENTER_VERTICAL}, xx, new int[]{LEFT_OF});
        searchText.setSingleLine(true);
        searchText.setBackgroundColor(Color.TRANSPARENT);
        searchText.setHint(R.string.search_hint);
        searchText.setHintTextColor(ContextCompat.getColor(context,R.color.search_hint));
        searchText.setTextSizeFitSp(32f);
        setPadding(searchText,25,0,0,0);
        searchText.addTextChangedListener(new onTextChange() {
            @Override
            public void getText(CharSequence text) {
                xx.setVisibility(text.length()==0?GONE:VISIBLE);
            }
        });
    }
}
