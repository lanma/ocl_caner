package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.widget.ImageView;

import com.james.views.FreeLayout;

/**
 * Created by Vegetable on 2016/4/10.
 */
public class leftimage extends FreeLayout{
    public ImageView img;
    public leftimage(Context context) {
        super(context);
        setPicSize(640,-2,TO_WIDTH);
        setPadding(20,0,0,0);

        img = (ImageView)addFreeView(new ImageView(context),160,160);
        img.setScaleType(ImageView.ScaleType.FIT_CENTER);
        img.setTag(null);
    }
}
