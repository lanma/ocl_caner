package org.ourcitylove.friendlycancer.layout;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

/**
 * Created by Vegetable on 2016/3/28.
 */
public class expandedcell extends FreeLayout{
    private final String assetPath = "file:///android_asset/";

    public FreeTextView title;
    public ImageView updownicon, icon;
//    public ExpandableRelativeLayout expandableLayout;
    public LinearLayout mlayout ;
    public expandedcell(Context context) {
        super(context);
        setPicSize(640,100);
        setFreeLayoutFW();
//        setMargin(this,20,20,20,0);
        setBackgroundColor(Color.WHITE);

        //icon
        icon = (ImageView)addFreeView(new ImageView(context), 100, 100);
        icon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        setMargin(icon, 10, 10, 0, 10);

        //title
        title = (FreeTextView)addFreeView(new FreeTextView(context),-2,100, icon, new int[]{RIGHT_OF});
        title.setTextSizeFitSp(60);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);
        setMargin(title,25,10,0,10);

        //updownicon
        updownicon = (ImageView)addFreeView(new ImageView(context),75,100,title,new int[]{ALIGN_PARENT_RIGHT,ALIGN_BOTTOM});
        updownicon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        updownicon.setTag(null);
        setPadding(updownicon,0,25,30,25);
        Glide.with(context).load(Uri.parse(assetPath + "image/menu_down.png")).diskCacheStrategy(DiskCacheStrategy.SOURCE).fitCenter().into(updownicon);

        mlayout = (LinearLayout)addFreeView(new LinearLayout(context),-1,-2,title,new int[]{BELOW});
        mlayout.setOrientation(LinearLayout.VERTICAL);
        mlayout.setVisibility(VISIBLE);
    }

}
