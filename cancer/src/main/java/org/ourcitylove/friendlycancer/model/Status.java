package org.ourcitylove.friendlycancer.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by Vegetable on 2016/9/20.
 */

public class Status {
    @JSONField(name = "result")
    public String result = "";
}
