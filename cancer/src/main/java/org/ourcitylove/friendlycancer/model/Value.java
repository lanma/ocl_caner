package org.ourcitylove.friendlycancer.model;

import com.alibaba.fastjson.annotation.JSONField;

import org.parceler.Parcel;

/**
 * Created by Vegetable on 2016/6/8.
 */

@Parcel
public class Value {
    @JSONField(name = "id")
    public String id;

    @JSONField(name = "area")
    public String area;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "address")
    public String address;
    @JSONField(name = "lat")
    public double lat;
    @JSONField(name = "lng")
    public double lng;
    @JSONField(name = "tel")
    public String tel;
    @JSONField(name = "remind1")
    public String remind1;
    @JSONField(name = "remind2")
    public String remind2;
    @JSONField(name = "remind3")
    public String remind3;
    @JSONField(name = "remind4")
    public String remind4;
    @JSONField(name = "remind5")
    public String remind5;
    @JSONField(name = "remind6")
    public String remind6;
    @JSONField(name = "certi")
    public String certi;
    @JSONField(name = "monday")
    public String monday;
    @JSONField(name = "tuesday")
    public String tuesday;
    @JSONField(name = "wednesday")
    public String wednesday;
    @JSONField(name = "thursday")
    public String thursday;
    @JSONField(name = "friday")
    public String friday;
    @JSONField(name = "saturday")
    public String saturday;
    @JSONField(name = "sunday")
    public String sunday;
    @JSONField(name = "breast")
    public String brestCancer;
    @JSONField(name = "cervix")
    public String cervixCancer;
    @JSONField(name = "cervix_woman")
    public String cervixWoman;
    @JSONField(name = "oral")
    public String oralCancer;
    @JSONField(name = "colorectal")
    public String colorectalCancer;
    @JSONField(name = "urls")
    public String urls;

    @JSONField(serialize = false, deserialize = false)
    public Double distance;

    public boolean isCerti() {
        return certi.equals("●") || certi.contains("●");
    }

    public Value() {
    }

//    /**
//     *@param id
//     * @param area
//     * @param title
//     * @param address
//     * @param lat
//     * @param lng
//     * @param tel
//     * @param remind1
//     * @param remind2
//     * @param remind3
//     * @param remind4
//     * @param certi
//     * @param monday
//     * @param tuesday
//     * @param wednesday
//     * @param thursday
//     * @param friday
//     * @param saturday
//     * @param sunday
//     * @param brestCancer
//     * @param cervixCancer
//     * @param cervixWoman
//     * @param oralCancer
//     * @param colorectalCancer
//     * @param urls
//     * @param distance
//     */
//    public Value(String area, String title, String address,
//                 double lat, double lng, String tel, String remind1,
//                 String remind2, String remind3, String remind4, String certi,
//                 String monday, String tuesday, String wednesday, String thursday,
//                 String friday, String saturday, String sunday, String brestCancer,
//                 String cervixCancer, String cervixWoman, String oralCancer, String colorectalCancer,
//                 String urls, Double distance) {
//        this.area = area;
//        this.title = title;
//        this.address = address;
//        this.lat = lat;
//        this.lng = lng;
//        this.tel = tel;
//        this.remind1 = remind1;
//        this.remind2 = remind2;
//        this.remind3 = remind3;
//        this.remind4 = remind4;
//        this.certi = certi;
//        this.monday = monday;
//        this.tuesday = tuesday;
//        this.wednesday = wednesday;
//        this.thursday = thursday;
//        this.friday = friday;
//        this.saturday = saturday;
//        this.sunday = sunday;
//        this.brestCancer = brestCancer;
//        this.cervixCancer = cervixCancer;
//        this.cervixWoman = cervixWoman;
//        this.oralCancer = oralCancer;
//        this.colorectalCancer = colorectalCancer;
//        this.urls = urls;
//        this.distance = distance;
//    }
}
