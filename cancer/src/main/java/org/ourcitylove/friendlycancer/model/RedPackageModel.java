package org.ourcitylove.friendlycancer.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vegetable on 2016/6/13.
 */

public class RedPackageModel {
    public String Gender;
    public Integer years;
    public ArrayList<Boolean> check;
    private static RedPackageModel model;
    public static RedPackageModel getInstance(String gender, String years, ArrayList check) {
        if (model == null) {
            model = new RedPackageModel();
        }
        model.Gender = gender;
        model.years = Integer.valueOf(years);
        model.check = check;
        return model;
    }
}
