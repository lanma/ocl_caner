package org.ourcitylove.friendlycancer.model;

import com.alibaba.fastjson.annotation.JSONField;

import org.parceler.Parcel;

/**
 * Created by Vegetable on 2016/6/15.
 */

@Parcel
public class MenuValue {
    @JSONField(name = "id")
    public String Id;
    @JSONField(name = "title_tw")
    public String titleTw = "";
    @JSONField(name = "imagefilename")
    public String imagefilename;
    @JSONField(name = "parentid")
    public String parentid;
    @JSONField(name = "downtype")
    public String downtype;
    @JSONField(name = "celltype")
    public String celltype;
    @JSONField(name = "cellcolor")
    public String cellcolor;
    @JSONField(name = "orderid")
    public int orderid;
    @JSONField(name = "p1")
    public String p1;
    @JSONField(name = "p2")
    public String p2;
    @JSONField(name = "extra")
    public String extra;

    public String getAssetsImageFileName() {
        return "file:///android_asset/image/" + imagefilename;
    }

    public String getNavigationImageFileName(){
        return imagefilename.replace("menu", "navi");
    }

    public String getOnlineImageFileName() {
        return imagefilename;
    }
}