package org.ourcitylove.friendlycancer.library;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class Memory {
	private static final String APP_SHARED_PREFS = "com.devandroid.ncuwlogin";
	private SharedPreferences appSharedPrefs;
	private Editor prefsEditor;

	public Memory(Context context) {
//		this.appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS,
//		Activity.MODE_PRIVATE);
		this.appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		this.prefsEditor = appSharedPrefs.edit();
	}

	public String getString(String key) {
		return appSharedPrefs.getString(key, "");
	}

	public void setString(String key, String value) {
		prefsEditor.putString(key, value);
		prefsEditor.commit();
	}

	public boolean getBoolean(String key) {
		return appSharedPrefs.getBoolean(key, true);
	}

	public void setBoolean(String key, boolean value) {
		prefsEditor.putBoolean(key, value);
		prefsEditor.commit();
	}

	public int getInt(String key) {
		return appSharedPrefs.getInt(key, 0);
	}

	public void setInt(String key, int value) {
		prefsEditor.putInt(key, value);
		prefsEditor.commit();
	}

	public long getLong(String key) {
		return appSharedPrefs.getLong(key, 0l);
	}

	public void setLong(String key, long value) {
		prefsEditor.putLong(key, value);
		prefsEditor.commit();
	}

//	public List<Integer> getIntList(String key) {
//		com.alibaba.fastjson.JSONObject json = JSON.parseObject(appSharedPrefs.getString(key, ""));
//		List<Integer> list = new ArrayList<Integer>();
//		JSONArray jsonArray = (JSONArray) json.get("array");
//		if (jsonArray != null) {
//			int len = jsonArray.size();
//			for (int i = 0; i < len; i++) {
//				list.add((Integer) jsonArray.get(i));
//			}
//		}
//		return list;
//	}
//
//	public void setIntList(String key, List<Integer> data) {
//		JSONObject json = new JSONObject();
//		json.put("array", JSON.toJSON(data));
//		String listString = json.toString();
//		prefsEditor.putString(key, listString);
//		prefsEditor.commit();
//	}
}