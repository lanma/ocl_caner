package org.ourcitylove.friendlycancer.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.jakewharton.rxbinding.view.RxView;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.helper.clinichelper;
import org.ourcitylove.friendlycancer.layout.PhoneCall;
import org.ourcitylove.friendlycancer.layout.clinicinfolayout;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.Value;
import org.ourcitylove.oclapp.LocationManager;
import org.ourcitylove.oclapp.OtherApp;
import org.parceler.Parcels;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Vegetable on 2016/4/4.
 */
public class ClinicInfoActivity extends BaseActivity {

    @BindView(R.id.Content)
    RelativeLayout contentlayout;

    clinicinfolayout mlayout;

    public LinkedHashMap<String, String> mtype = new LinkedHashMap();

    Value mvalue;

    @Override
    protected void onCreate(Bundle savedInstanced) {
        setContentView(R.layout.clinicinfo);
        super.onCreate(savedInstanced);
        setview();
    }

    @Override
    protected void setview() {
        super.setview();
        mlayout = new clinicinfolayout(this);
        ButterKnife.bind(mlayout);
        contentlayout.addView(mlayout, -1, -1);
        Parcelable pa = getIntent().getParcelableExtra(KEYMODEL.DATA);
        mvalue = Parcels.unwrap(pa);
        LoadInfo(mvalue);
    }

    @Override
    public void finish() {
        super.finish();
    }


    private void LoadInfo(Value mvalue) {
        //Certi Title
        mlayout.Certi.setVisibility(mvalue.isCerti() ? View.VISIBLE : View.GONE);
        SpannableString sbs = new SpannableString(mvalue.title);
        String url = mvalue.urls;
        if(!TextUtils.isEmpty(url)) {
            sbs.setSpan(new UnderlineSpan(), 0, sbs.length(), 0);
            mlayout.title.setTextColor(Color.parseColor("#4258a7"));
            mlayout.title.setOnClickListener(v-> App.enterWeb(this, url));
        }
        mlayout.title.setText(sbs);

        //distance
        mlayout.distance.setText(LocationManager.getDisplayDistance(this, mvalue.distance));

        //screen
        mtype.put("大腸癌", mvalue.colorectalCancer);
        mtype.put("乳癌", mvalue.brestCancer);
        mtype.put("口腔癌", mvalue.oralCancer);
        mtype.put("子宮頸癌", mvalue.cervixCancer);
        int num = 0;
        for (Map.Entry<String, String> entry : mtype.entrySet()) {
            if (!entry.getValue().equals("●")) continue;
            FreeTextView child = (FreeTextView) mlayout.screen.getChildAt(num);
            child.setText(entry.getKey());
            child.setBackgroundColor(Color.parseColor(clinichelper.bgcolor.get(entry.getKey())));
            num++;
        }

        //address
        mlayout.address.setText(mvalue.address);

        //phone
        mlayout.phonenumber.setText(mvalue.tel.replaceAll("\\$\\$", "\n"));
        RxView.clicks(mlayout.call).throttleWithTimeout(500, TimeUnit.MICROSECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(v -> {
                    String[] modeltype = {"image240", "text"};
                    String[] modelcontent = {"item_icon_call.png", mvalue.tel};

                    PhoneCall phoneCall = new PhoneCall(this, modeltype, modelcontent);
                    Dialog dialog = new Dialog(this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(phoneCall);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    phoneCall.cancel.setOnClickListener(cancelview -> dialog.dismiss());
                    if (mvalue.tel != null && !mvalue.tel.equals(""))
                        RxView.clicks(phoneCall.call).observeOn(AndroidSchedulers.mainThread()).subscribe(callview -> {
                            String number = mvalue.tel;
                            if(number.contains("轉"))number = number.substring(0, number.indexOf("轉"));
                            OtherApp.call(this, number);
                        });
                    dialog.show();
                });
        mlayout.map.setOnClickListener(v -> OtherApp.googleMapNavigate(v.getContext(), mvalue.address));

        //table
        String[] mdate = {"一", "二", "三", "四", "五", "六", "日"};
        String[] mday = {mvalue.monday, mvalue.tuesday, mvalue.wednesday, mvalue.thursday, mvalue.friday, mvalue.saturday, mvalue.sunday};
        for (int index = 0; index < mdate.length; index++) {
            View root = mlayout.findViewWithTag(mdate[index]);
            String[] tag = mday[index].replaceAll(" ", "").split(",");
            if (Arrays.asList(tag).contains("無") || (tag.length == 1 & tag[0].trim().equals("")))
                continue;
            Flowable.fromArray(tag)
                    .map(mtag->(FreeTextView) root.findViewWithTag(mtag))
                    .subscribe(mview->{
                        mview.setText("●");
                        mview.setTextColor(ContextCompat.getColor(this, R.color.mainColor));
                    },throwable -> {});
        }

        //Service subject
        Observable.just(mvalue.remind1, mvalue.remind2, mvalue.remind3, mvalue.remind4)
                .filter(remind -> !remind.equals("")).toList()
                .subscribe(remind -> mlayout.service.setText(remind));

        //老地方
        ButterKnife.findById(mlayout, R.id.favor).setOnClickListener(v -> Favor((ViewGroup) v));
        boolean isFavor = ((App) getApplication()).pref.contains(KEYMODEL.FAVORITE + mvalue.id);
        ((ImageView) mlayout.oldplace.getChildAt(0)).setImageResource(isFavor ? R.drawable.item_icon_guide_favor_red : R.drawable.item_icon_favor);
    }

    public void Favor(ViewGroup view) {
        ImageView icon = (ImageView) view.getChildAt(0);
        boolean isFavor = ((App) getApplication()).pref.contains(KEYMODEL.FAVORITE + mvalue.id);
        if (isFavor)
            ((App) getApplication()).pref.edit().remove(KEYMODEL.FAVORITE + mvalue.id).commit();
        else
            ((App) getApplication()).pref.edit().putString(KEYMODEL.FAVORITE + mvalue.id, KEYMODEL.FAVORITE + mvalue.id).commit();
        isFavor = !isFavor;
        icon.setImageResource(isFavor ? R.drawable.item_icon_guide_favor_red : R.drawable.item_icon_favor);
    }
}
