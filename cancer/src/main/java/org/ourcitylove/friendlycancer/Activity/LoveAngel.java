package org.ourcitylove.friendlycancer.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.jakewharton.rxbinding.view.RxView;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.layout.PhoneCall;
import org.ourcitylove.friendlycancer.layout.SpacesItemDecoration;
import org.ourcitylove.friendlycancer.layout.normalbutton;
import org.ourcitylove.oclapp.OtherApp;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Vegetable on 2016/4/7.
 */
public class LoveAngel extends BaseActivity {
    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanced) {
        setContentView(R.layout.love_angel);
        super.onCreate(savedInstanced);
        setview();
    }

    @Override
    protected void setview() {
        super.setview();

        List<LoveAngelModel> mlist = new ArrayList() {{
            add(new LoveAngelModel("我有疑問！衛生局健康好站專線", "Call", ""));
            add(new LoveAngelModel("我要加入！進入醫療院所申請註冊", "Web", "https://ourcitylovewebapps.azurewebsites.net/Cancer/#/menu"));
            add(new LoveAngelModel("我已加入！進入公開資料登錄系統", "Web", "https://ourcitylovewebapps.azurewebsites.net/Cancer/"));
        }};

        GridLayoutManager manager = new GridLayoutManager(this, 1);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        manager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(25, 70, 25, 70));
        recyclerView.setAdapter(new normalbuttonadapter(mlist));
    }

    protected class normalbuttonadapter extends RecyclerView.Adapter<normalbuttonadapter.normalbuttonviewholder> {
        List<LoveAngelModel> mlist;

        public normalbuttonadapter(List mlist) {
            this.mlist = mlist;
        }

        @Override
        public normalbuttonviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new normalbuttonviewholder(new normalbutton(parent.getContext()));
        }

        @Override
        public void onBindViewHolder(normalbuttonviewholder holder, int position) {
            holder.button.setText(mlist.get(position).title);

            switch (mlist.get(position).downtype.toLowerCase()) {
                case "web":
                    if (mlist.get(position).url.equals("")) break;
                    RxView.clicks(holder.button)
                            .subscribe(v -> {
                                Intent intent = new Intent("android.intent.action.VIEW");
                                intent.setData(Uri.parse(mlist.get(position).url));
                                try {
                                    intent.setClassName("com.android.chrome", "com.google.android.apps.chrome.Main");
                                } catch (Exception e) {
                                    intent.setComponent(new ComponentName("com.android.browser",
                                            "com.android.browser.BrowserActivity"));
                                }
                                LoveAngel.this.startActivity(intent);
                            });
                    break;
                case "call":
                    RxView.clicks(holder.button)
                            .subscribe(v -> {
                                String[] type = String.format("text$$image240$$text").split("\\$\\$");
                                String[] content = String.format("若有疑問，請撥打以下專線。$$item_icon_call.png$$臺北市政府衛生局癌症篩檢諮詢\n服務時間：09:00~17:00\n(02)2720-8889轉1829").split("\\$\\$");
                                PhoneCall phoneCall = new PhoneCall(holder.button.getContext(), type, content);
                                phoneCall.builddialog();
                                phoneCall.call.setOnClickListener(mv -> OtherApp.call(holder.button.getContext(), "0227208889"));
                            });
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return mlist.size();
        }

        protected class normalbuttonviewholder extends RecyclerView.ViewHolder {
            FreeTextView button;

            public normalbuttonviewholder(View itemView) {
                super(itemView);
                button = ButterKnife.findById(itemView, R.id.button);
            }
        }
    }

    public class LoveAngelModel {
        public String title;
        public String downtype;
        public String url;

        public LoveAngelModel(String title, String downtype, String url) {
            this.title = title;
            this.downtype = downtype;
            this.url = url;
        }
    }
}
