package org.ourcitylove.friendlycancer.activity;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.oclapp.Firebase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BaseActivity extends Activity{
    private static final String ANDROID_ASSETS = "file:///android_asset/";

    @Nullable
    @BindView(R.id.backgroundimg)
    ImageView backgroundView;

    @BindView(R.id.toolbar_title)
    TextView title;

    @BindView(R.id.toolbar_icon)
    ImageView icon_view;

    @BindView(R.id.navi_map)
    TextView map_view;

    @BindView(R.id.toolbar_background)
    View bg;

    Unbinder unbind;

    @Override
    protected void onCreate(Bundle savedInstanced){
        super.onCreate(savedInstanced);
        unbind = ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        finish();
        unbind.unbind();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    protected void setview(){
        Bundle bundle = getIntent().getExtras();
//        String icon = bundle.getString(KEYMODEL.NAVI_ICON,"");
        boolean map = bundle.getBoolean(KEYMODEL.NAVI_MAP,false);
        String navititle = bundle.getString(KEYMODEL.NAVI_TITLE,getString(R.string.navititle));
        title.setText(navititle);
        String navicolor = bundle.getString(KEYMODEL.NAVI_COLOR,"#F63E74");
        bg.setBackgroundColor(Color.parseColor(navicolor));
//        if(!icon.equals("")){
//            Glide.with(this).load(ANDROID_ASSETS +"image/" + icon).diskCacheStrategy(DiskCacheStrategy.SOURCE).fitCenter().into(icon_view);
//        }else {
//            icon_view.setVisibility(View.GONE);
//        }
        String backgroundimg = bundle.getString(KEYMODEL.BACKGROUNDIMG,"");
        if(backgroundView != null & backgroundimg!= "")
            Glide.with(this).load(Uri.parse(ANDROID_ASSETS + "image/" + backgroundimg)).diskCacheStrategy(DiskCacheStrategy.SOURCE).fitCenter().into(backgroundView);

        map_view.setVisibility(map?View.VISIBLE:View.INVISIBLE);


        Firebase.trackScreen(navititle);//todo Track Activity Title
    }

}
