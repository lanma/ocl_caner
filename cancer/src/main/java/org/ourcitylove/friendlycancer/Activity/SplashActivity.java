package org.ourcitylove.friendlycancer.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.BuildConfig;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.helper.Helper;
import org.ourcitylove.friendlycancer.model.KEYMODEL;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;


/**
 * Created by Vegetable on 2016/3/20.
 */
public class SplashActivity extends Activity{
    @BindView(R.id.splash)
    ImageView splash;

   @Override
    protected void onCreate(Bundle savedInstanced){
       super.onCreate(savedInstanced);

       if (BuildConfig.DEBUG)
           go_to_main();

       overridePendingTransition(0, 0);
       setContentView(R.layout.splashactivity);
       ButterKnife.bind(this);

       Glide.with(this).load(R.drawable.splash).skipMemoryCache(true).crossFade(700).fitCenter().into(splash);
       new Handler().postDelayed(this::go_to_main,3000);
   }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        this.finish();
    }

    private void go_to_main(){
        Flowable.concat(Helper.SeparateFromTitle(getString(R.string.redpacktitle_separate)), Helper.SeparateFromDownType(getString(R.string.redpackdowntype_separate)))
                .filter(data -> data != null)
                    .take(1)
                    .doOnNext(value -> ((App) getApplication()).pref.edit().putBoolean(KEYMODEL.FIRST, true).commit())
                    .subscribe(value -> App.enterRedPack(this, value.titleTw, true));
        finish();
    }

    @Override
    public void finish(){
        super.finish();

    }
}
