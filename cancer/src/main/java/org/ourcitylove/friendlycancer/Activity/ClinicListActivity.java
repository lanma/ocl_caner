package org.ourcitylove.friendlycancer.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.karumi.dexter.Dexter;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.adapter.ClinicAdapter;
import org.ourcitylove.friendlycancer.converter.Converter;
import org.ourcitylove.friendlycancer.layout.titletextlayout;
import org.ourcitylove.friendlycancer.model.ClinicModel;
import org.ourcitylove.friendlycancer.model.EventModel;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.Value;
import org.ourcitylove.oclapp.LocationManager;
import org.ourcitylove.oclapp.RationalePermissionListener;
import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.nlopez.smartlocation.SmartLocation;
import rx.Observable;

public class ClinicListActivity extends BaseActivity {

    @BindView(R.id.recycler)
    RecyclerView recyclerview;

    List<Value> mdata;

    @Override
    protected void onCreate(Bundle savedInstanced) {
        setContentView(R.layout.clinic_listlayout);
        super.onCreate(savedInstanced);
        setview();
        Parcelable parcelable = getIntent().getParcelableExtra(KEYMODEL.DATA);
        onMessageEvent(Parcels.unwrap(parcelable));

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(RationalePermissionListener.Builder.with(this)
                        .withRunOnGranted(()->{
                            SmartLocation.with(this).location().start(loc ->
                                    Observable.from(mdata)
                                            .doOnNext(data->data.distance = LocationManager.getDistance(
                                                    loc.getLatitude(), loc.getLongitude(),
                                                    data.lat, data.lng))
                                            .toSortedList(ClinicModel::compareByDistance)
                                            .subscribe(this::LoadData));
                        }).build()
                ).check();

//        App.loc.lastAndUpdate(this, true)
//                .filter(loc->loc!=null)
//                .doOnNext(loc->App.loc.stop())
//                .subscribe(loc->
//                        Observable.from(mdata)
//                                .doOnNext(data->data.distance = LocationManager.getDistance(
//                                        loc.getLatitude(), loc.getLongitude(),
//                                        data.lat, data.lng))
//                                .toSortedList(ClinicModel::compareByDistance)
//                                .subscribe(this::LoadData));
    }

    @Override
    protected void onStop() {
        super.onStop();
        App.loc.stop();
    }

    @OnClick(R.id.navi_map)
    public void tomap(){
        Intent m = new Intent(this,MapActivity.class);
        Parcelable parcelable = getIntent().getParcelableExtra(KEYMODEL.DATA);
        m.putExtra(KEYMODEL.DATA, parcelable);
        startActivity(m);
    }

    @Override
    public void finish(){
        super.finish();
    }

    @Override
    protected void setview(){
        super.setview();
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
    }

    private void LoadData(List<Value> mdata) {
        if (recyclerview == null) return;

        if(recyclerview.getAdapter() == null)
            recyclerview.setAdapter( new ClinicAdapter(this, mdata));
        else ((ClinicAdapter)recyclerview.getAdapter()).notifyDataSetChanged(mdata);
    }

    public void onMessageEvent(EventModel model) {
        switch (model.EventType) {
            case EventModel.CONTINUE:
                App.clinicvalue = Converter.ClinicConverter();
                Observable.just(model.Mdata)
                        .doOnNext(mdata->this.mdata = mdata)
                        .subscribe(this::LoadData);
                if(getIntent().getBooleanExtra(KEYMODEL.FAVOR, false) && mdata.size() == 0){
                    titletextlayout dialogbg = new titletextlayout(this);
                    dialogbg.title.setText(R.string.sys);
                    dialogbg.content.setText(R.string.favornodata);
                    dialogbg.buildDialog();
                    return;
                }
                break;
            case EventModel.Pause:
                break;
        }
    }
}
