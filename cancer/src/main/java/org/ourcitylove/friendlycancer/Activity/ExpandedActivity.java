package org.ourcitylove.friendlycancer.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.james.views.FreeLayout;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.utils.Utils;
import org.ourcitylove.friendlycancer.converter.Converter;
import org.ourcitylove.friendlycancer.helper.Helper;
import org.ourcitylove.friendlycancer.helper.AddViewHelper;
import org.ourcitylove.friendlycancer.layout.PhoneCall;
import org.ourcitylove.friendlycancer.layout.expandedcell;
import org.ourcitylove.friendlycancer.listener.AnimationListner;
import org.ourcitylove.friendlycancer.model.EventModel;
import org.ourcitylove.friendlycancer.model.FuncModel;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.Value;
import org.ourcitylove.oclapp.OtherApp;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Flowable;
import rx.Observable;

/**
 * Created by Vegetable on 2016/4/16.
 */
public class ExpandedActivity extends BaseActivity {

    @BindView(R.id.content)
    LinearLayout contentlayout;

    @BindView(R.id.where_bg)
    View where;
    @BindView(R.id.call_bg)
    View call;

    @Override
    protected void onCreate(Bundle savedInstanced){
        setContentView(R.layout.expandedlayout);
        super.onCreate(savedInstanced);
        loaddata();
        setview();
    }

    private void loaddata(){
        App.menuvalue = Converter.MenuConverter();}

    @Override
    protected void setview() {
        super.setview();
        title.setText(String.format("%s%s", title.getText(), "小百科"));
        Bundle bundle = getIntent().getExtras();
        int id = Integer.valueOf(bundle.getString(KEYMODEL.MENUID, "1"));
        String bgcolor = bundle.getString(KEYMODEL.NAVI_COLOR);
        where.setBackgroundColor(Color.parseColor(bgcolor));
        call.setBackgroundColor(Color.parseColor(bgcolor));
        Helper.Separate(App.menuvalue, id)
                .flatMap(Flowable::fromIterable)
                .subscribe(mvalue -> {
                    switch (FuncModel.celltype.get(mvalue.celltype)) {
                        case 2://expandedcell
                            expandedcell expandedcell = new expandedcell(this);
                            Glide.with(this).load(mvalue.getAssetsImageFileName())
                                    .dontTransform()
                                    .fitCenter()
                                    .into(expandedcell.icon);
                            expandedcell.title.setText(mvalue.titleTw);
                            contentlayout.addView(expandedcell, -1, -2);
                            LinearLayout.LayoutParams mparams = (LinearLayout.LayoutParams) expandedcell.getLayoutParams();
                            mparams.topMargin = Utils.dpToPx(this, 8);
                            expandedcell.setLayoutParams(mparams);
                            try {
                                expandedcell.mlayout.setBackgroundColor(Color.parseColor(mvalue.extra));
                            } catch (Exception e) {
                            }

                            String[] modeltype = mvalue.p1.split("\\$\\$");
                            String[] modelcontent = mvalue.p2.split("\\$\\$");
                            if (!modeltype[0].equals("") && !modelcontent[0].equals("")) {
                                AddViewHelper.Expanded(expandedcell.mlayout, modeltype, modelcontent);
                                expandedcell.mlayout.setTag(true);
                                expandedcell.mlayout.setVisibility(View.GONE);
                            }
                            //
                            expandedcell.setOnClickListener(v -> {
                                if (expandedcell.mlayout.getVisibility() == View.VISIBLE) {
                                    Animation fold = AnimationUtils.loadAnimation(v.getContext(),
                                            R.anim.view_scale_y_drop_reverse);
                                    fold.setAnimationListener(new AnimationListner() {
                                        @Override
                                        protected void onAnimation(Animation animation) {
                                            expandedcell.mlayout.setVisibility(View.GONE);
                                        }
                                    });
                                    expandedcell.mlayout.startAnimation(fold);
                                } else {
                                    expandedcell.mlayout.setVisibility(View.VISIBLE);
                                    Animation unfold = AnimationUtils.loadAnimation(
                                            v.getContext(), R.anim.view_scale_y_drop);
                                    unfold.setAnimationListener(new AnimationListner() {
                                        @Override
                                        protected void onAnimation(Animation animation) {
                                            expandedcell.mlayout.setVisibility(View.VISIBLE);
                                        }
                                    });
                                    expandedcell.mlayout.startAnimation(unfold);
                                }
                            });
                            break;
                        case 3://backgroundimagecell
                        {
                            FreeLayout content = new FreeLayout(this);
                            content.setPicSize(640, -2, FreeLayout.TO_WIDTH);
                            ImageView mview = (ImageView) content.addFreeView(new ImageView(this), 640, -2);
                            mview.setScaleType(ImageView.ScaleType.FIT_CENTER);
                            Glide.with(this).load(mvalue.getAssetsImageFileName()).diskCacheStrategy(DiskCacheStrategy.SOURCE).fitCenter().into(mview);
                            contentlayout.addView(content, -1, -2);
                        }
                        break;
                    }
                });
    }

    @OnClick(R.id.where)
    public void where(View view){
        Observable<Value> ob = Observable.from(App.clinicvalue);

        switch (title.getText().toString()){
            case KEYMODEL.colorectal:
                ob = ob.filter(clinic->clinic.colorectalCancer.equals("●"));
                break;
            case KEYMODEL.brest:
                ob = ob.filter(clinic->clinic.brestCancer.equals("●"));
                break;
            case KEYMODEL.cervical:
                ob = ob.filter(clinic->clinic.cervixCancer.equals("●"));
                break;
            case KEYMODEL.oral:
                ob = ob.filter(clinic->clinic.oralCancer.equals("●"));
                break;
        }
        ob.toList().subscribe(mvalues->{
            App.enterClinic(this, getIntent().getStringExtra(KEYMODEL.NAVI_ICON),
                    getIntent().getStringExtra(KEYMODEL.NAVI_TITLE), false,
                    new EventModel(EventModel.CONTINUE, mvalues));
        });
    }

    @OnClick(R.id.call)
    public void call(View view){
        Helper.SeparateFromTitle(getString(R.string.expanded_call))
                .subscribe
                        (values->{
                    ArrayList<String> modeltype = new ArrayList(Arrays.asList(values.p1.split("\\$\\$")));
                    ArrayList<String> modelcontent = new ArrayList(Arrays.asList(values.p2.split("\\$\\$")));
                    String number = values.extra;
                    PhoneCall phoneCall = new PhoneCall(this,modeltype.toArray(new String[0]),modelcontent.toArray(new String[0]));
                    if(number != null && !number.equals(""))
                        phoneCall.call.setOnClickListener(callview -> OtherApp.call(this, number));
                    phoneCall.builddialog();
                });
    }
}
