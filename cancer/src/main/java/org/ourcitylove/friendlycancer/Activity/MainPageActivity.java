package org.ourcitylove.friendlycancer.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.adapter.MenuAdapter;
import org.ourcitylove.friendlycancer.converter.Converter;
import org.ourcitylove.friendlycancer.helper.Helper;
import org.ourcitylove.friendlycancer.layout.BallotWinDialog;
import org.ourcitylove.friendlycancer.model.EventModel;
import org.ourcitylove.friendlycancer.model.KEYMODEL;

import butterknife.BindView;
import rx.Observable;

/**
 * Created by Vegetable on 2016/3/20.
 */
public class MainPageActivity extends BaseActivity {

    @BindView(R.id.Content)
    RelativeLayout contentlayout;
    @BindView(R.id.sliderShow)
    SliderLayout bannerview;
    @BindView(R.id.recycler)
    RecyclerView recyclerview;

//    List mlist;
    @Override
    protected void onCreate(Bundle savedInstaneced){
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstaneced);
        setview();
    }
    @Override
    protected void onResume(){
        super.onResume();
        LoadData();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause(){
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
    @Override
    protected void setview(){
        super.setview();

        boolean banner = getIntent().getBooleanExtra(KEYMODEL.BANNER,false);
        bannerview.setVisibility(banner? View.VISIBLE:View.GONE);

        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));

        if(getIntent().getBooleanExtra(KEYMODEL.BallotWin, false)) {
            String LotteryType = getIntent().getExtras().getString(KEYMODEL.BallotType, "");
            String Content = "";
            switch (LotteryType){
                case "下載抽抽樂":
                    Content = "恭喜您！獲得「APP下載抽抽樂」活動好禮1份，獲獎名單於105年12月15日內公告臺北市政府衛生局網站";
                    break;
                case "篩檢抽抽樂":
                    Content = "恭喜您！獲得「上傳癌篩資料」活動精美小禮物1份，獲獎名單於105年12月15日公告臺北市政府衛生局網站";
                    break;
            }

            BallotWinDialog bd = new BallotWinDialog(this).buildDialog();
            bd.dialog.setCancelable(false);
            bd.titleview.setText("中獎通知");
            bd.contentview.setText(Content);
            bd.contentview1.setText("如有疑問，請撥打以下專線：\n(02)2720-8889轉1829，服務時間：週一至週五9:00~17:00，謝謝您！");
            bd.okbtn.setOnClickListener(v->bd.dialog.dismiss());
        }
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(EventModel model){
        switch (model.EventType){
            case EventModel.CONTINUE:
                LoadData();
                break;
            case EventModel.Pause:
                break;
        }
}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        boolean fromredpack = getIntent().getBooleanExtra("FROMREDPACK", false);
        if(fromredpack)
            App.enterMainPage(this, "1", "", getString(R.string.navititle), "", "", null);
    }

    private void LoadData() {
        App.menuvalue = Converter.MenuConverter();
        App.clinicvalue = Converter.ClinicConverter();
        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString(KEYMODEL.MENUID, "1");
        Helper.Separate(App.menuvalue, 0)
                .doOnNext(d -> bannerview.removeAllSliders())
                .subscribe(menuValues -> {
                    if (menuValues.size() <= 1) {
                        ImageView mview = new ImageView(this);
                        bannerview.addView(mview, -1, -2);
                        mview.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        Glide.with(this).load(menuValues.get(0).getAssetsImageFileName()).fitCenter().into(mview);
                    } else Observable.from(menuValues)
                            .subscribe(mbanner -> {
                                DefaultSliderView mslider = new DefaultSliderView(this);
                                mslider.image(mbanner.getAssetsImageFileName())
                                        .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                                        .setOnSliderClickListener(slider -> {
                                            if (null != mbanner.extra && mbanner.extra != "")
                                                App.enterWeb(this, mbanner.extra);
                                        });
                                bannerview.addSlider(mslider);
                                bannerview.startAutoCycle(8000, 8000, true);
                            });
                });

        Helper.Separate(App.menuvalue, id)
                .subscribe(mlist -> {
                    if (recyclerview.getAdapter() == null)
                        recyclerview.setAdapter(new MenuAdapter(this, mlist));
                    else
                        recyclerview.getAdapter().notifyDataSetChanged();
                });

    }
}
