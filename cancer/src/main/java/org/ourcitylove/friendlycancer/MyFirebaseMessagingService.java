package org.ourcitylove.friendlycancer;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.ourcitylove.friendlycancer.model.KEYMODEL;

import java.util.Map;
import java.util.UUID;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> msgData = remoteMessage.getData();
        Log.e("data", msgData.toString());
        App app = (App) getApplication();
        Intent mainintent = app.enterNotifaction(app.getBaseContext(), "1", "", getString(R.string.navititle), "", "");
        mainintent.putExtra(KEYMODEL.BallotWin, true);
        mainintent.putExtra(KEYMODEL.BallotTitle, msgData.get("title"));
        mainintent.putExtra(KEYMODEL.BallotSubTitle, msgData.get("sub_title"));
        mainintent.putExtra(KEYMODEL.BallotType, msgData.get("LotteryType"));
        app.sendNotification(mainintent, UUID.randomUUID().hashCode(),
                msgData.get("title"), msgData.get("sub_title"), null);
    }
}
