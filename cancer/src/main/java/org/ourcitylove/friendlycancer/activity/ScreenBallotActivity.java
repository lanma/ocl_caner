package org.ourcitylove.friendlycancer.activity;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.google.firebase.iid.FirebaseInstanceId;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.utils.Utils;
import org.ourcitylove.friendlycancer.helper.ServerHelper;
import org.ourcitylove.friendlycancer.layout.BallotDialog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * Created by Vegetable on 2016/9/19.
 */

public class ScreenBallotActivity extends BaseActivity {
    @BindView(R.id.field_name)
    EditText fieldName;
    @BindView(R.id.field_ssn)
    EditText fieldSSN;
    @BindView(R.id.field_phone)
    EditText fieldPhone;
    @BindView(R.id.field_address)
    EditText fieldAddress;

    @BindViews({R.id.colorectal, R.id.oral,
                R.id.breast,     R.id.cervical})
    List<CheckBox> checkboxs;

    private AwesomeValidation validator;
    @Override
    protected void onCreate(Bundle savedInstanced) {
        setContentView(R.layout.screenballot);
        super.onCreate(savedInstanced);
        setview();
    }

    @Override
    protected void setview() {
        super.setview();

        SpannableString sbs = new SpannableString(getText(R.string.downloadballot_url_update));
        sbs.setSpan(new UnderlineSpan(), 0, sbs.length(), 0);
        ((TextView)ButterKnife.findById(this, R.id.url)).setText(sbs);
        ButterKnife.findById(this, R.id.url).setOnClickListener(v-> App.enterWeb(this, sbs.toString().replaceAll("[()]", "")));

        validator = new AwesomeValidation(ValidationStyle.COLORATION);
        validator.addValidation(fieldName, RegexTemplate.NOT_EMPTY, "請輸入姓名");
        validator.addValidation(fieldSSN, "[a-zA-Z]\\d{9}", "請輸入身份證字號");
        validator.addValidation(fieldPhone, "09\\d{8}", "請輸入手機號碼");
        validator.addValidation(fieldAddress, RegexTemplate.NOT_EMPTY, "請輸入地址");

        Utils.setupUI(this, ButterKnife.findById(this, R.id.constraintLayout));

        ButterKnife.findById(this, R.id.submit).setOnClickListener(v -> {
            if (!validator.validate()) return;

            boolean ischeck = false;
            for (CheckBox cb : checkboxs) {
                ischeck = cb.isChecked();
                if (ischeck) break;
            }
            if (!ischeck) {
                Toast.makeText(this, "請勾選一項篩檢項目", Toast.LENGTH_SHORT).show();
                return;
            }

            FirebaseInstanceId firebaseInstanceId = FirebaseInstanceId.getInstance();
            String InstanceId = firebaseInstanceId.getId();
            String token = firebaseInstanceId.getToken();

            Map<String, String> query = new HashMap() {{
                put("InstanceId", InstanceId);
                put("InstanceToken", token);
                put("Name", fieldName.getText().toString());
                put("Phone", fieldPhone.getText().toString());
                put("SSN", fieldSSN.getText().toString());
                put("Address", fieldAddress.getText().toString());
                put("Colorectal", String.valueOf(checkboxs.get(0).isChecked() ? 1 : 0));
                put("Oral", String.valueOf(checkboxs.get(1).isChecked() ? 1 : 0));
                put("Breast", String.valueOf(checkboxs.get(2).isChecked() ? 1 : 0));
                put("Cervical", String.valueOf(checkboxs.get(3).isChecked() ? 1 : 0));
            }};
            Toast.makeText(this, "資料上傳中...請稍待片刻", Toast.LENGTH_LONG).show();
            v.setClickable(false);
            ServerHelper.AddScreenList(query)
                    .subscribe(status -> {
                        v.setClickable(true);
                        if (status.result.equalsIgnoreCase("success")) {
                            BallotDialog bd = new BallotDialog(this).buildDialog();
                            bd.dialog.setCancelable(false);
                            bd.titleview.setText(R.string.screenballot_title);
                            bd.contentview.setText(R.string.screenballot_content);
                            bd.okbtn.setOnClickListener(vv -> {
                                bd.dialog.dismiss();
                                finish();
                            });
                        } else {
                        }
                    }, throwable -> {
                        v.setClickable(true);
                        Toast.makeText(this, "連線異常...請稍候再試", Toast.LENGTH_SHORT).show();
                        throwable.printStackTrace();
                    });
        });
    }
}
