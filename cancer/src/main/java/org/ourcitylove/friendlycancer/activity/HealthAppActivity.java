package org.ourcitylove.friendlycancer.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.adapter.MenuAdapter;
import org.ourcitylove.friendlycancer.helper.Helper;
import org.ourcitylove.friendlycancer.model.KEYMODEL;

import butterknife.BindView;

/**
 * Created by Vegetable on 2016/8/20.
 */

public class HealthAppActivity extends BaseActivity{

    @BindView(R.id.recycler)
    RecyclerView recyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.healthapp);
        super.onCreate(savedInstanceState);
        setview();
    }

    @Override
    protected void setview(){
        super.setview();

        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        LoadData();
    }

    void LoadData(){
        String id = getIntent().getExtras().getString(KEYMODEL.MENUID, "1");
        Helper.Separate(App.menuvalue, id)
                .subscribe(mlist -> {
                    if (recyclerview.getAdapter() == null)
                        recyclerview.setAdapter(new MenuAdapter(this, mlist));
                    else
                        recyclerview.getAdapter().notifyDataSetChanged();
                });
    }
}
