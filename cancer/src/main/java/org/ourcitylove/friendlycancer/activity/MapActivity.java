package org.ourcitylove.friendlycancer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterManager;

import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.utils.MapClusterRenderer;
import org.ourcitylove.friendlycancer.utils.MapItem;
import org.ourcitylove.friendlycancer.model.EventModel;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.Value;
import org.parceler.Parcels;

import java.util.List;

import butterknife.ButterKnife;
import rx.Observable;

/**
 * Created by Vegetable on 2016/6/7.
 */
public class MapActivity extends FragmentActivity implements OnMapReadyCallback{
    List<Value> mvalue;

    ClusterManager<MapItem> mClusterManager;

    MapItem item;
    @Override
    protected void onCreate(Bundle savedInstanced){
        super.onCreate(savedInstanced);
        setContentView(R.layout.map);
        ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.frglbsmap)).getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();

        Parcelable parcelable = getIntent().getParcelableExtra(KEYMODEL.DATA);
        mvalue = ((EventModel) Parcels.unwrap(parcelable)).Mdata;

        mClusterManager = new ClusterManager<>(this, googleMap);

        Observable.from(mvalue)
                .subscribe(mv -> {
                    MapItem item = new MapItem(mv);
                    mClusterManager.addItem(item);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mv.lat, mv.lng), 12));
                });

        googleMap.setOnInfoWindowClickListener(mClusterManager);
        googleMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        googleMap.setOnCameraIdleListener(mClusterManager);
//        googleMap.setOnCameraChangeListener(mClusterManager);
        googleMap.setOnMarkerClickListener(mClusterManager);

        mClusterManager.setOnClusterItemClickListener(item -> {
            this.item = item;
            return false;
        });
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(infoWindowAdapter);
        mClusterManager.setOnClusterItemInfoWindowClickListener(infoWindowClickListener);
        mClusterManager.setRenderer(new MapClusterRenderer(this, googleMap, mClusterManager));
    }

    private InfoWindowAdapter infoWindowAdapter = new InfoWindowAdapter() {
        @Override
        public View getInfoWindow(Marker marker) {
            Value mv = mvalue.get(getMarkerPos(marker.getPosition()));
            View m = getLayoutInflater().inflate(R.layout.marker_info, null);
            TextView title = ButterKnife.findById(m, R.id.map_title);
            TextView addr = ButterKnife.findById(m, R.id.map_addr);
            title.setText(mv.title);
            addr.setText(mv.address);
            return m;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    };

    private ClusterManager.OnClusterItemInfoWindowClickListener infoWindowClickListener = marker -> {
            Value mv = mvalue.get(getMarkerPos(marker.getPosition()));
        Intent m = new Intent(this, ClinicInfoActivity.class);
        m.putExtra(KEYMODEL.DATA, Parcels.wrap(mv));
        startActivity(m);
    };

    private int getMarkerPos(LatLng position) {
        int best_pos = -1;
        double min_diff = Double.MAX_VALUE;
        for (Value value : mvalue) {
            double diff = Math.pow(position.latitude - value.lat, 2)
                    + Math.pow(position.longitude - value.lng, 2);
            if (diff < min_diff) {
                min_diff = diff;
                best_pos = mvalue.indexOf(value);
            }
        }
        return best_pos;
    }
}
