package org.ourcitylove.friendlycancer.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.fragment.RedPack;
import org.ourcitylove.friendlycancer.layout.Dialog;
import org.ourcitylove.friendlycancer.model.KEYMODEL;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class Red_PackActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstance) {
        setContentView(R.layout.basefragment);
        super.onCreate(savedInstance);
        setview();
    }

    protected void setview() {
        super.setview();
        title.setText(R.string.redpacktitle);
        ButterKnife.findById(this, R.id.toolbar_back).setVisibility(getIntent().getBooleanExtra(KEYMODEL.BACK, false)?View.VISIBLE:View.GONE);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.add(R.id.fragmentroot, new RedPack(), "TRANSMIT");
        transaction.commit();

        Dialog d = new Dialog(this).buildDialog();
        d.dialog.setCancelable(true);
        d.setOnClickListener(v->d.dialog.dismiss());
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event){
        if (getIntent().getBooleanExtra(KEYMODEL.BACK, false) & keycode == KeyEvent.KEYCODE_BACK){ToMain();}
        return super.onKeyDown(keycode, event);
    }

    @OnClick(R.id.toolbar_back)
    public void ToMain(){
        App.enterMainPage(this, "1", "", getString(R.string.navititle), "", "", null);
        finish();
    }
}
