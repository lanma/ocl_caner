package org.ourcitylove.friendlycancer.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.adapter.MenuAdapter;
import org.ourcitylove.friendlycancer.helper.Helper;
import org.ourcitylove.friendlycancer.model.KEYMODEL;

import butterknife.BindView;

/**
 * Created by Vegetable on 2016/8/20.
 */

public class Encyclopedia extends BaseActivity {

    @BindView(R.id.recycler)
    RecyclerView recyclerview;

    @BindView(R.id.description)
    TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.encyclopedia);
        super.onCreate(savedInstanceState);
        setview();
    }

    @Override
    protected void setview(){
        super.setview();

        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));

        SpannableString spa = new SpannableString("資料來源：衛生福利部國民健康署");
        spa.setSpan(new UnderlineSpan(), 0, spa.length(), 0);
        description.setText(spa);

        RxView.clicks(description).subscribe(v-> App.enterWeb(this, "http://health99.hpa.gov.tw/default.aspx"));


        LoadData();
    }

    void LoadData(){
        String id = getIntent().getExtras().getString(KEYMODEL.MENUID, "1");
        Helper.Separate(App.menuvalue, id)
                .subscribe(mlist -> {
                    if (recyclerview.getAdapter() == null)
                        recyclerview.setAdapter(new MenuAdapter(this, mlist));
                    else
                        recyclerview.getAdapter().notifyDataSetChanged();
                });
    }
}
