package org.ourcitylove.friendlycancer.activity;

import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.ourcitylove.friendlycancer.R;

public class WebActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanced) {
        setContentView(R.layout.activity_web);
        super.onCreate(savedInstanced);

        setview();
    }

    @Override
    protected void setview() {
        super.setview();
        String url = getIntent().getStringExtra("URL");
        WebView webView = (WebView) findViewById(R.id.web_view);
        WebSettings setting = webView.getSettings();
        setting.setJavaScriptEnabled(true);
        setting.setJavaScriptCanOpenWindowsAutomatically(true);
        setting.setSupportMultipleWindows(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl(url);
    }
}
