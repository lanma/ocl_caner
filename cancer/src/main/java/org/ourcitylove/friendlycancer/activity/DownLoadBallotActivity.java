package org.ourcitylove.friendlycancer.activity;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.google.firebase.iid.FirebaseInstanceId;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.utils.Utils;
import org.ourcitylove.friendlycancer.helper.ServerHelper;
import org.ourcitylove.friendlycancer.layout.BallotDialog;
import org.ourcitylove.friendlycancer.layout.EndLessLayout;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.Status;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;

/**
 * Created by Vegetable on 2016/9/19.
 */

public class DownLoadBallotActivity extends BaseActivity {
    @BindView(R.id.field_name)
    EditText fieldName;
    @BindView(R.id.field_phone)
    EditText fieldPhone;
    @BindView(R.id.field_address)
    EditText fieldAddress;

    private AwesomeValidation validator;
    @Override
    protected void onCreate(Bundle savedInstanced) {
        setContentView(R.layout.downloadballot);
        super.onCreate(savedInstanced);
        setview();
    }

    @Override
    protected void setview() {
        super.setview();

        SpannableString sbs = new SpannableString(getText(R.string.downloadballot_url_update));
        sbs.setSpan(new UnderlineSpan(), 0, sbs.length(), 0);
        ((TextView)ButterKnife.findById(this, R.id.url)).setText(sbs);
        ButterKnife.findById(this, R.id.url).setOnClickListener(v-> App.enterWeb(this, sbs.toString().replaceAll("[()]", "")));


        Utils.setupUI(this, ButterKnife.findById(this, R.id.constraintLayout));

        validator = new AwesomeValidation(ValidationStyle.COLORATION);
        validator.addValidation(fieldName, RegexTemplate.NOT_EMPTY, "請輸入姓名");
        validator.addValidation(fieldPhone, "09\\d{8}", "請輸入手機號碼");
        validator.addValidation(fieldAddress, RegexTemplate.NOT_EMPTY, "請輸入地址");

        if (App.pref.getBoolean(KEYMODEL.HAS_DownLoadBallot, false)) {
            fieldName.setText(App.pref.getString(KEYMODEL.FieldName, ""));
            fieldPhone.setText(App.pref.getString(KEYMODEL.FieldPhone, ""));
            fieldAddress.setText(App.pref.getString(KEYMODEL.FieldAddress, ""));
        }

        ButterKnife.findById(this, R.id.submit).setOnClickListener(v -> {
            if (!validator.validate()) return;

            App.pref.edit()
                    .putString(KEYMODEL.FieldName, fieldName.getText().toString())
                    .putString(KEYMODEL.FieldPhone, fieldPhone.getText().toString())
                    .putString(KEYMODEL.FieldAddress, fieldAddress.getText().toString())
                    .apply();

            FirebaseInstanceId firebaseInstanceId = FirebaseInstanceId.getInstance();
            String InstanceId = firebaseInstanceId.getId();
            String InstanceToken = firebaseInstanceId.getToken();
            Map query = new HashMap() {{
                put("InstanceId", InstanceId);
                put("InstanceToken", InstanceToken);
                put("Name", fieldName.getText().toString());
                put("Phone", fieldPhone.getText().toString());
                put("Address", fieldAddress.getText().toString());
            }};

            Flowable<Status> sub = App.pref.getBoolean(KEYMODEL.HAS_DownLoadBallot, false) ?
                    ServerHelper.UpdateDownList(query) : ServerHelper.AddDownLoadList(query);

            Toast.makeText(this, "資料上傳中...請稍待片刻", Toast.LENGTH_LONG).show();
            v.setClickable(false);
            sub.subscribe(status -> {
                v.setClickable(true);
                if (status.result.equalsIgnoreCase("success")) {
                    if(App.pref.getBoolean(KEYMODEL.HAS_DownLoadBallot, false)) {
                        EndLessLayout el = new EndLessLayout(this).buildDialog();
                        el.dialog.setCancelable(false);
                        el.titleview.setText(R.string.downloadballot_title);
                        el.contentview.setText(R.string.downloadballot_content_update);
                        String url = getString(R.string.downloadballot_url_update).replaceAll("[()]","");
                        el.URL.setOnClickListener(vv-> App.enterWeb(this, url));
                        el.URL.setText(R.string.downloadballot_url_update);
                        el.contentview1.setText(R.string.downloadballot_content1_update);
                        el.okbtn.setOnClickListener(vv->{
                            el.dialog.dismiss();
                            finish();
                        });
                    }else{
                        BallotDialog bd = new BallotDialog(this).buildDialog();
                        bd.dialog.setCancelable(false);
                        bd.titleview.setText(R.string.downloadballot_title);
                        bd.contentview.setText(R.string.downloadballot_content);
                        bd.okbtn.setOnClickListener(vv -> {
                            bd.dialog.dismiss();
                            finish();
                        });
                    }
                    App.pref.edit().putBoolean(KEYMODEL.HAS_DownLoadBallot, true).apply();
                } else {
                    BallotDialog bd = new BallotDialog(this).buildDialog();
                    bd.dialog.setCancelable(false);
                    bd.contentview.setText(R.string.downloadballot_content);
                    bd.okbtn.setOnClickListener(vv -> {
                        bd.dialog.dismiss();
                        finish();
                    });
                }
            }, throwable -> {
                v.setClickable(true);
                Toast.makeText(this, "連線異常...請稍候再試", Toast.LENGTH_SHORT).show();
                throwable.printStackTrace();
            });
        });
    }
}
