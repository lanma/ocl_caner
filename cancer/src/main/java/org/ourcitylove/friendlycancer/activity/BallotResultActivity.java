package org.ourcitylove.friendlycancer.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import org.ourcitylove.friendlycancer.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BallotResultActivity extends BaseActivity {
    @BindView(R.id.web_view)
    WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.ballotresult);
        super.onCreate(savedInstanceState);
        setview();
    }

    @Override
    protected void setview() {
        super.setview();

        ButterKnife.findById(this, R.id.toolbar_back).setVisibility(View.VISIBLE);
        ButterKnife.findById(this, R.id.toolbar_back).setOnClickListener(v->finish());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl("http://health.gov.taipei/Default.aspx");

    }
}
