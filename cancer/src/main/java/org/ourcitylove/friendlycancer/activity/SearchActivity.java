package org.ourcitylove.friendlycancer.activity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.converter.Converter;
import org.ourcitylove.friendlycancer.helper.AddViewHelper;
import org.ourcitylove.friendlycancer.layout.SearchNoData;
import org.ourcitylove.friendlycancer.layout.SearchBarLayout;
import org.ourcitylove.friendlycancer.layout.ToWhereLayout;
import org.ourcitylove.friendlycancer.model.EventModel;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.SearchModel;
import org.ourcitylove.friendlycancer.model.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Flowable;

public class SearchActivity extends BaseActivity {

    @BindView(R.id.scrolllayout)
    LinearLayout scrolllayout;
    @BindView(R.id.search_button)
    Button search;

    List<SearchModel> searchModels = new ArrayList<>();

    ArrayList<Value> mvalues = new ArrayList<>();

    public HashMap<String, Object> filter = new HashMap<>();
    {
        filter.put("搜尋", "");
        filter.put("篩檢", new Hashtable());
        filter.put("區域", new Hashtable());
        filter.put("時段", "");
        filter.put("平日時段", new Hashtable());
        filter.put("假日時段-週六", new Hashtable());
        filter.put("假日時段-週日", new Hashtable());
    }


    @Override
    protected void onCreate(Bundle savedInstance) {
        setContentView(R.layout.towherelayout);
        super.onCreate(savedInstance);
        setview();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        return super.onTouchEvent(event);
    }

    @Override
    protected void setview() {
        super.setview();
        searchModels.add(new SearchModel(SearchModel.group_search, SearchModel.type_searchbar, getString(R.string.search_hint), getString(R.string.search_hint)));
        searchModels.add(new SearchModel(SearchModel.group_screen, SearchModel.type_title, getString(R.string.screening), getString(R.string.screening_description)));
        searchModels.add(new SearchModel(SearchModel.group_screen, SearchModel.type_button, "大腸癌$$乳癌$$口腔癌$$子宮頸癌", ""));
        searchModels.add(new SearchModel(SearchModel.group_screen, SearchModel.type_checklabel, getString(R.string.screen_woman), ""));
        searchModels.add(new SearchModel(SearchModel.group_area, SearchModel.type_title, getString(R.string.area), getString(R.string.area_description)));
        searchModels.add(new SearchModel(SearchModel.group_area, SearchModel.type_button, "中正$$大同$$中山$$松山$$大安$$萬華$$信義$$士林$$北投$$內湖$$南港$$文山", ""));
        searchModels.add(new SearchModel(SearchModel.group_time, SearchModel.type_title, getString(R.string.time), getString(R.string.time_description)));
        searchModels.add(new SearchModel(SearchModel.group_workday, SearchModel.type_multibutton, "平日$$上午$$下午$$晚上", ""));
        searchModels.add(new SearchModel(SearchModel.group_holiday_saturday, SearchModel.type_multibutton, "週六$$上午$$下午$$晚上", ""));
        searchModels.add(new SearchModel(SearchModel.group_holiday_sunday, SearchModel.type_multibutton, "週日$$上午$$下午$$晚上", ""));
        ToWhereLayout layout = new ToWhereLayout(this, searchModels);
        scrolllayout.addView(layout, -1, -1);

        AddViewHelper.setSearchListener(filter, searchModels, scrolllayout);

        search.setText(R.string.search);
        SearchBarLayout searchBarLayout = (SearchBarLayout) ((ViewGroup) layout.getChildAt(0)).getChildAt(0);
        searchBarLayout.searchText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                submit();
                return true;
            } else return false;
        });

    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onFilterThread(HashMap<String, Object> filter) {
        this.filter = filter;
    }

    @Subscribe
    public void onEvent(EventModel model){ App.clinicvalue = Converter.ClinicConverter(); }

    public boolean getFilter(final Value clinic, String key) {
        if (key.equals(SearchModel.group_search)) {
            Pattern pattern = Pattern.compile(String.format("(%s)", filter.get(key)));
            return pattern.matcher(clinic.title).find();
        }
        if (key.equals(SearchModel.group_screen)) {
            Hashtable<String, String> screen = (Hashtable) filter.get(key);
            if (screen.size() == 0) return true;
            boolean Filter = (screen.containsKey(SearchModel.Colorectal) & clinic.colorectalCancer.equals("●")) ||
                    (screen.containsKey(SearchModel.Brest) & clinic.brestCancer.equals("●")) ||
                    (screen.containsKey(SearchModel.Oral) & clinic.oralCancer.equals("●")) ||
                    (screen.containsKey(SearchModel.Cervix) & clinic.cervixCancer.equals("●"));
            if(screen.containsKey(SearchModel.Cervix_Woman))
                Filter = Filter && clinic.cervixWoman.equals("●");
            return Filter;
        }
        if (key.equals(SearchModel.group_area)) {
            Hashtable<String, String> area = (Hashtable) filter.get(key);
            if (area.size() == 0) return true;
            for (String areakey : area.keySet()) {
                Pattern areapattern = Pattern.compile(String.format("(%s)", area.get(areakey)));
                if( areapattern.matcher(clinic.area).find())return true;
            }
            return false;
        }
        if (key.equals(SearchModel.group_workday)) {
            Hashtable<String, String> work = (Hashtable) filter.get(key);
//            if (work.size() == 0) return false;
            {
                String[] workdays = {clinic.monday, clinic.tuesday, clinic.wednesday, clinic.thursday, clinic.friday};
                for (String workkey : work.keySet())
                    for (String workday : workdays) {
                        if(workday.contains(work.get(workkey)))
                            return true;
//                        Pattern workpattern = Pattern.compile(".*"+ workday + "*.");
//                        boolean fin = workpattern.matcher(work.get(workkey)).find();
//                        if (workpattern.matcher(work.get(workkey)).find())
//                            return true;
                    }
                return false;
            }
        }
        if (key.equals(SearchModel.group_holiday_saturday)) {
            Hashtable<String, String> saturday = (Hashtable) filter.get(key);
//            if (saturday.size() == 0) return false;
            for (String sat : saturday.keySet()) {
                if(clinic.saturday.contains(sat))
                    return true;
//                Pattern satpattern = Pattern.compile(String.format("(%s)", clinic.saturday));
//                if (satpattern.matcher(sat).find()) return true;
            }
            return false;
        }
        if (key.equals(SearchModel.group_holiday_sunday)) {
            Hashtable<String, String> sunday = (Hashtable) filter.get(key);
//            if (sunday.size() == 0) return false;
            for (String sun : sunday.keySet()) {
                if(clinic.sunday.contains(sun))
                    return true;
//                Pattern sunpattern = Pattern.compile(String.format("(%s)", clinic.sunday));
//                if (sunpattern.matcher(sun).find()) return true;
            }
            return false;
        }else return true;
    }

    @OnClick(R.id.search_button)
    public void submit(){
        mvalues.clear();
        Flowable<Value> ob = Flowable.fromIterable(App.clinicvalue);
        ob = ob .filter(cli -> getFilter(cli, SearchModel.group_search))
                .filter(cli -> getFilter(cli, SearchModel.group_screen))
                .filter(cli -> getFilter(cli, SearchModel.group_area))
                .filter(cli -> getFilter(cli, SearchModel.group_time));

        if(((Hashtable)filter.get(SearchModel.group_workday)).size() != 0||
           ((Hashtable)filter.get(SearchModel.group_holiday_saturday)).size() != 0||
           ((Hashtable)filter.get(SearchModel.group_holiday_sunday)).size() !=0)
            ob = ob.filter(cli -> getFilter(cli, SearchModel.group_workday)||
                getFilter(cli, SearchModel.group_holiday_saturday)||
                getFilter(cli, SearchModel.group_holiday_sunday));

        ob.toList().subscribe(mlist->mvalues.addAll(mlist));

        if (this.mvalues.size() == 0) {
            new SearchNoData(this).buildDialog();
            return;
        }
//
        App.enterClinic(this, getIntent().getStringExtra(KEYMODEL.NAVI_ICON),
                        getIntent().getStringExtra(KEYMODEL.NAVI_TITLE), false,
                        new EventModel(EventModel.CONTINUE, mvalues));
    }


}
