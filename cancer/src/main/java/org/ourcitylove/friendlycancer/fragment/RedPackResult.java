package org.ourcitylove.friendlycancer.fragment;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.adapter.RedPackAgeAdapter;
import org.ourcitylove.friendlycancer.model.MenuValue;
import org.ourcitylove.oclapp.layout.ListSpaceDecorator;
import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Vegetable on 2016/6/15.
 */

public class RedPackResult extends Fragment {
    @BindView(R.id.recycler)
    RecyclerView recycler;

    @BindView(R.id.smallicon)
    ViewGroup share;

    private Unbinder unbind;
    private View rootview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null)
            return null;
        if (rootview == null)
            rootview = inflater.inflate(R.layout.red_package_result, container, false);
        unbind = ButterKnife.bind(this, rootview);
        Bundle b = getArguments();
        if (b != null) {
            List mdata = Parcels.unwrap(b.getParcelable("DATA"));
            MenuValue mv = new MenuValue();
            mv.titleTw = getString(R.string.referral_unit);
            mdata.add(mv);
            recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            recycler.addItemDecoration(new ListSpaceDecorator(20));
            recycler.setAdapter(new RedPackAgeAdapter(mdata));
        }
        return rootview;
    }

    @OnClick(R.id.smallicon)
    public void share() {
        String redpack = getString(R.string.redpack_sharedescription);
        Uri shareuri = Uri.parse("file:///android_asset/image/redpackshare.png");
        App.share(getActivity(), "http://App.OurCityLove.com/CAPrevention", redpack, shareuri);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbind.unbind();
    }
}
