package org.ourcitylove.friendlycancer.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding.view.RxView;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.utils.Utils;
import org.ourcitylove.friendlycancer.helper.Helper;
import org.ourcitylove.friendlycancer.helper.Red_Pack_Gift;
import org.ourcitylove.friendlycancer.helper.ServerHelper;
import org.ourcitylove.friendlycancer.layout.Dialog;
import org.ourcitylove.friendlycancer.layout.ScratchImageView;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.MenuValue;
import org.ourcitylove.friendlycancer.model.RedPackageModel;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import io.reactivex.Flowable;
import rx.Observable;
import rx.functions.Action1;

/**
 * Created by Vegetable on 2016/6/15.
 */
public class RedPack extends Fragment {
    @BindViews({R.id.boy, R.id.girl})
    public TextView[] genders;
    @BindView(R.id.yearold)
    public TextView yearsold;
    @BindViews({R.id.betel_nut, R.id.smoke, R.id.Aboriginal, R.id.brestcancer})
    public CheckBox[] checks;

    public String gender = "";
    public String years = "";


    private Unbinder unbind;

    private View rootView;
    private boolean[] che = new boolean[2];
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null)
            return null;
        if(rootView == null)
            rootView = inflater.inflate(R.layout.red_package, container, false);
        unbind = ButterKnife.bind(this, rootView);
        Utils.setupUI(getActivity(), rootView);
        return rootView;
    }


    @OnCheckedChanged({R.id.betel_nut, R.id.smoke, R.id.Aboriginal, R.id.brestcancer})
    public void setCheck(CheckBox check) {
        check.setChecked(check.isChecked());
    }


    @OnClick({R.id.boy, R.id.girl})
    public void setGender(TextView textView) {
        che[0] = true;
        gender = (String) textView.getTag();
        for (TextView g : genders) {
            boolean eq = g.getTag().equals(this.gender);
            g.setBackgroundResource(eq ? R.drawable.button_click : R.drawable.button_normal);
            g.setTextColor(ContextCompat.getColor(getActivity(), eq ? R.color.white : R.color.gray01));
        }
    }

    @OnTextChanged(R.id.yearold)
    public void yeasold() {
        years = String.format("%s", yearsold.getText());
        che[1] = !years.equals("");
    }

    @OnClick(R.id.submit)
    public void submit() {
        ArrayList<String> check = new ArrayList();
        Observable.from(checks).filter(ch -> ch.isChecked())
                .subscribe(ch -> check.add(String.format("%s", ch.getText())));
        if (!che[0]) {
            Toast.makeText(getActivity(), getString(R.string.rec_submit_failed, "性別"), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!che[1]) {
            Toast.makeText(getActivity(), getString(R.string.rec_submit_failed, "年次"), Toast.LENGTH_SHORT).show();
            return;
        }
        RedPackageModel gift = RedPackageModel.getInstance(gender, years, check);
        Flowable.merge(Red_Pack_Gift.getcolorectal(gift), Red_Pack_Gift.getbrest(gift),
                Red_Pack_Gift.getcervical(gift), Red_Pack_Gift.getoral(gift))
                .defaultIfEmpty(new MenuValue())
                .toList()
                .subscribe(mi -> {
                    Dialog dialog = new Dialog(getActivity());
                    if (mi.size() == 1 && null == mi.get(0).Id) {
                        dialog.resetnomoney();
                        dialog.foreground.setRevealListener(new ScratchImageView.IRevealListener() {
                            @Override
                            public void onRevealed(ScratchImageView var1) {
                                var1.setVisibility(View.GONE);

                                Action1<Void> a = o -> {
                                    dialog.dialog.dismiss();
                                    getActivity().finish();
                                    Bundle b = new Bundle();
                                    b.putBoolean("FROMREDPACK", true);
                                    Helper.SeparateFromTitle("癌篩小百科")
                                            .subscribe(mvale -> App.enterMainPage(getActivity(), mvale.Id,
                                                    mvale.imagefilename.replace("menu", "navi"), mvale.titleTw,
                                                    mvale.extra, mvale.cellcolor, b)
                                            );
                                };

                                RxView.clicks(dialog).subscribe(a);
                                RxView.clicks(dialog.button).subscribe(a);
                            }

                            @Override
                            public void onScratchPercent(float var1) {
                                Log.d("ScrathchPercent", String.valueOf(var1));
                                if (var1 > 90)
                                    dialog.foreground.AutoRevealed();
                            }
                        });
                        dialog.foreground.setVisibility(View.VISIBLE);
                        dialog.text.setText(R.string.redpackage_no);
                        dialog.button.setVisibility(View.VISIBLE);
                    } else {
                        Integer money = 0;
                        String[] type = new String[4];
                        Arrays.fill(type, "0");
                        for (MenuValue m : mi) {
                            money += m.p1.equals(KEYMODEL.DOLLAR) ? Integer.valueOf(m.p2) : 0;
                            switch (m.titleTw) {
                                case KEYMODEL.colorectal:
                                    type[0] = "1";
                                    break;
                                case KEYMODEL.brest:
                                    type[1] = "1";
                                    break;
                                case KEYMODEL.cervical:
                                    type[3] = "1";
                                    break;
                                case KEYMODEL.oral:
                                    type[2] = "1";
                                    break;
                            }
                        }
                        final String m = String.valueOf(money);
                        dialog.foreground.setVisibility(View.VISIBLE);
                        dialog.text.setText(getString(R.string.congradulation));
                        dialog.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18.5f);
                        dialog.icon.setImageResource(R.drawable.red_pack_get);
                        dialog.bg.setImageResource(R.drawable.red_pack_no);
                        dialog.foreground.setRevealListener(new ScratchImageView.IRevealListener() {
                            @Override
                            public void onRevealed(ScratchImageView var1) {
                                var1.setVisibility(View.GONE);
                                dialog.bg.setImageResource(R.drawable.red_pack);
                                Action1 a = o -> {
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    Fragment transmit = getFragmentManager().findFragmentByTag("TRANSMIT");
                                    RedPackResult result = new RedPackResult();
                                    ServerHelper.SaveGameLog(gender, years, type[0], type[1], type[2], type[3], m, TextUtils.join(",", check.toArray(new String[check.size()])));
                                    transaction.remove(transmit);
                                    Bundle b = new Bundle();
                                    b.putParcelable("DATA", Parcels.wrap(mi));
                                    result.setArguments(b);
                                    transaction.replace(R.id.fragmentroot, result);
                                    transaction.commit();
                                    dialog.dialog.dismiss();
                                };
                                RxView.clicks(dialog).subscribe(a);
                            }

                            @Override
                            public void onScratchPercent(float var1) {
                                Log.d("ScrathchPercent", String.valueOf(var1));
                                if (var1 > 90)
                                    dialog.foreground.AutoRevealed();
                            }
                        });
                    }
                    dialog.buildDialog();
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbind.unbind();
    }
}
