package org.ourcitylove.friendlycancer;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Dimension;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.apache.commons.io.IOUtils;
import org.ourcitylove.friendlycancer.activity.BallotResultActivity;
import org.ourcitylove.friendlycancer.activity.ClinicListActivity;
import org.ourcitylove.friendlycancer.activity.DownLoadBallotActivity;
import org.ourcitylove.friendlycancer.activity.Encyclopedia;
import org.ourcitylove.friendlycancer.activity.ExpandedActivity;
import org.ourcitylove.friendlycancer.activity.HealthAppActivity;
import org.ourcitylove.friendlycancer.activity.MainPageActivity;
import org.ourcitylove.friendlycancer.activity.Red_PackActivity;
import org.ourcitylove.friendlycancer.activity.ScreenBallotActivity;
import org.ourcitylove.friendlycancer.activity.SearchActivity;
import org.ourcitylove.friendlycancer.activity.WebActivity;
import org.ourcitylove.friendlycancer.converter.Converter;
import org.ourcitylove.friendlycancer.helper.ServerHelper;
import org.ourcitylove.friendlycancer.helper.clinichelper;
import org.ourcitylove.friendlycancer.model.EventModel;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.MenuValue;
import org.ourcitylove.friendlycancer.model.Value;
import org.ourcitylove.oclapp.OclApp;
import org.parceler.Parcels;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.UUID;


//@ReportsCrashes(
//        formUri = "https://ourcitylove.cloudant.com/acra-friendly-cancer-screening/_design/acra-storage/_update/report",
//        reportType = org.acra.sender.HttpSender.Type.JSON,
//        httpMethod = org.acra.sender.HttpSender.Method.PUT,
//        formUriBasicAuthLogin="pricheningereedstelamend",
//        formUriBasicAuthPassword="9bd5e1c2f0e1c82387383b162231a6d4222b858e",
//        // Your usual ACRA configuration
//        mode = ReportingInteractionMode.TOAST,
//        resToastText = R.string.message_toast_crash
//)
public class App extends OclApp {
    public static List<MenuValue> menuvalue;
    public static List<Value> clinicvalue;
    public static clinichelper cli;
//    public static SharedPreferences pref;
//    public static FirebaseAnalytics fa;

    private Runnable dbUpdate;
    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        cli = new clinichelper();
//        pref = PreferenceManager.getDefaultSharedPreferences(this);
//        fa = FirebaseAnalytics.getInstance(this);
        if (pref.getString(KEYMODEL.MENU, "").equals("")) {
            try {
                String defaultmenu = IOUtils.toString(getAssets().open("DefaultMenu.txt"), Charset.defaultCharset());
                pref.edit().putString(KEYMODEL.MENU, defaultmenu).apply();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (pref.getString(KEYMODEL.ClINIC, "").equals("")) {
            try {
                String defaultclinic = IOUtils.toString(getAssets().open("DefaultClinic.txt"), Charset.defaultCharset());
                pref.edit().putString(KEYMODEL.ClINIC, defaultclinic).apply();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ServerHelper.init();
        scheduleDbUpdate();

        menuvalue = Converter.MenuConverter();
        clinicvalue = Converter.ClinicConverter();
//        LeakCanary.install(this);
    }
    private void scheduleDbUpdate() {
        Handler handler = new Handler();
        dbUpdate = () -> {
            ServerHelper.getMenuData();
            handler.postDelayed(dbUpdate, 60 * 60 * 1000);
        };
        handler.post(dbUpdate);
    }

    public static void enterExpanded(Context context, String id, String image, String title, String backgroundimg, String cellcolor) {
        Intent intent = new Intent(context, ExpandedActivity.class);
        intent.putExtra(KEYMODEL.MENUID, id);
        intent.putExtra(KEYMODEL.NAVI_ICON, image);
        intent.putExtra(KEYMODEL.NAVI_TITLE, title);
        intent.putExtra(KEYMODEL.BACK, true);
        if (!backgroundimg.isEmpty())
            intent.putExtra(KEYMODEL.BACKGROUNDIMG, backgroundimg);
        intent.putExtra(KEYMODEL.NAVI_COLOR, cellcolor.equals("") ? "#F63E74" : cellcolor);
        context.startActivity(intent);
    }
    public Intent enterNotifaction(Context context, String id, String image, String title, String backgroundimg, String cellcolor) {
        return baseMainPage(context, MainPageActivity.class, id, image, title, backgroundimg, cellcolor, null);
    }

    public static void enterMainPage(Context context, String id, String image, String title, String backgroundimg, String cellcolor, @Nullable Bundle bundle) {
        context.startActivity(baseMainPage(context, MainPageActivity.class, id, image, title, backgroundimg, cellcolor, bundle));
    }

    public static void enterHealth(Context context, String id, String image, String title, String backgroundimg, String cellcolor, @Nullable Bundle bundle) {
        context.startActivity(baseMainPage(context, HealthAppActivity.class, id, image, title, backgroundimg, cellcolor, bundle));
    }

    public static void enterEncyclopedia(Context context, String id, String image, String title, String backgroundimg, String cellcolor, @Nullable Bundle bundle) {
        context.startActivity(baseMainPage(context, Encyclopedia.class, id, image, title, backgroundimg, cellcolor, bundle));
    }

    public static void enterScreenBallot(Context context, String id, String image, String title, String backgroundimg, String cellcolor, @Nullable Bundle bundle){
        context.startActivity(baseMainPage(context, ScreenBallotActivity.class, id, image, title, backgroundimg, cellcolor, bundle));
    }

    public static void enterDownloadBallot(Context context, String id, String image, String title, String backgroundimg, String cellcolor, @Nullable Bundle bundle){
        context.startActivity(baseMainPage(context, DownLoadBallotActivity.class, id, image, title, backgroundimg, cellcolor, bundle));
    }
    public static void enterBallotResult(Context context, String id, String image, String title, String backgroundimg, String cellcolor, @Nullable Bundle bundle){
        context.startActivity(baseMainPage(context, BallotResultActivity.class, id, image, title, backgroundimg, cellcolor, bundle));
    }

    private static Intent baseMainPage(Context context, Class to, String id, String image, String title, String backgroundimg, String cellcolor, @Nullable Bundle bundle){
        Intent intent = new Intent(context, to);
        intent.putExtra(KEYMODEL.MENUID, id);
        intent.putExtra(KEYMODEL.BANNER, id.equals("1"));
        intent.putExtra(KEYMODEL.NAVI_ICON, image);
        intent.putExtra(KEYMODEL.NAVI_TITLE, title);
        intent.putExtra(KEYMODEL.BACK, true);
        if (!backgroundimg.isEmpty())
            intent.putExtra(KEYMODEL.BACKGROUNDIMG, backgroundimg);
        intent.putExtra(KEYMODEL.NAVI_COLOR, cellcolor.equals("") ? "#F63E74" : cellcolor);
        if (bundle != null)
            intent.putExtras(bundle);
        return intent;
    }

    public static void enterToWhere(Context context, String imagefile, String titleTw) {
        Intent mintent = new Intent(context, SearchActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(KEYMODEL.NAVI_ICON, imagefile);
        bundle.putString(KEYMODEL.NAVI_TITLE, titleTw);
        bundle.putBoolean(KEYMODEL.NAVI_TITLE_ICON, true);
        mintent.putExtras(bundle);
        context.startActivity(mintent);
    }

    public static void enterClinic(Context context, String image, String title, boolean isfavor, EventModel model) {
        Intent mintent = new Intent(context, ClinicListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(KEYMODEL.NAVI_ICON, image);
        bundle.putString(KEYMODEL.NAVI_TITLE, title);
        bundle.putBoolean(KEYMODEL.NAVI_TITLE_ICON, true);
        bundle.putBoolean(KEYMODEL.NAVI_MAP, true);
        bundle.putBoolean(KEYMODEL.FAVOR, isfavor);
        bundle.putParcelable(KEYMODEL.DATA, Parcels.wrap(model));
        mintent.putExtras(bundle);
        context.startActivity(mintent);
    }

    public static void enterRedPack(Context context, String title, boolean back) {
        Intent mintent = new Intent(context, Red_PackActivity.class);
        mintent.putExtra(KEYMODEL.NAVI_TITLE, title);
        mintent.putExtra(KEYMODEL.BACK, back);
        context.startActivity(mintent);
    }

    public static void enterWeb(Context context, String url) {
        Intent mintent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(mintent);
    }

    public static void enterEmbeddedWeb(Context context, String url) {
        Intent intent = new Intent(context, WebActivity.class);
        intent.putExtra("URL", url);
        intent.putExtra(KEYMODEL.NAVI_TITLE, "轉介單位");
        intent.putExtra(KEYMODEL.NAVI_MAP, false);
        context.startActivity(intent);
    }

    public static void share(Context context, String SUBJECT, String TEXT, Uri imagePath) {
        App.fa.logEvent(FirebaseAnalytics.Event.SHARE, null);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        intent.putExtra(Intent.EXTRA_STREAM, imagePath);
        intent.putExtra(android.content.Intent.EXTRA_TEXT, String.format("%s\n%s", TEXT, SUBJECT));
        context.startActivity(Intent.createChooser(intent, "分享至"));
    }

    public void sendNotification(Intent clickIntent, int id, String title, String subTitle, String text) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, UUID.randomUUID().hashCode(), clickIntent,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        subTitle = subTitle.substring(0, 40) + "...";
        RemoteViews rv = new RemoteViews(getPackageName(), R.layout.notification_layout);
        rv.setTextViewText(R.id.noti_title, title);
        rv.setTextViewText(R.id.noti_subtitle, subTitle);
        rv.setTextViewTextSize(R.id.noti_title, Dimension.SP, 14);
        rv.setTextViewTextSize(R.id.noti_subtitle, Dimension.SP, 14);
        rv.setImageViewResource(R.id.noti_img, R.drawable.notificationicon);

        Notification notification = new NotificationCompat.Builder(this)
                .setCustomContentView(rv)
                .setCustomHeadsUpContentView(rv)
                .setCustomBigContentView(rv)
                .setSmallIcon(R.drawable.notificationicon)
                .setContentTitle(title)
                .setSubText(subTitle)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .setBigContentTitle(title)
                        .bigText(text == null ? subTitle : String.format("%s\n%s", subTitle, text))
                )
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .build();


        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, notification);
    }

    public static void sendEmail(Activity context, String[] sendto, String Title, String msg){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, sendto);
        intent.putExtra(Intent.EXTRA_SUBJECT, Title);
        intent.putExtra(Intent.EXTRA_TEXT, msg);
        context.startActivity(Intent.createChooser(intent, "選擇發送程式"));
    }
}
