package org.ourcitylove.friendlycancer.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;

/**
 * Created by Vegetable on 2016/7/9.
 */

public class MapClusterRenderer extends DefaultClusterRenderer<MapItem> {

    private final IconGenerator mIconGenerator;
    @ColorRes
    private final int clusterOutlineColor = R.color.mainColor;
    private ShapeDrawable mColoredCircleBackground;
    private SparseArray<BitmapDescriptor> mIcons = new SparseArray();
    private final float mDensity;
    private Context mContext;

    public MapClusterRenderer(Context context, GoogleMap map,
                              ClusterManager<MapItem> clusterManager) {
        super(context, map, clusterManager);

        this.mContext = context;
        this.mDensity = context.getResources().getDisplayMetrics().density;
        this.mIconGenerator = new IconGenerator(context);
        this.mIconGenerator.setContentView(this.makeSquareTextView(context));
        this.mIconGenerator.setTextAppearance(
                com.google.maps.android.R.style.amu_ClusterIcon_TextAppearance);
        this.mIconGenerator.setBackground(this.makeClusterBackground());
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<MapItem> cluster) { return super.shouldRenderAsCluster(cluster); }

    @Override
    protected void onBeforeClusterItemRendered(MapItem item, MarkerOptions markerOptions) {
        int bucket = 0;
        BitmapDescriptor descriptor = (BitmapDescriptor)this.mIcons.get(bucket);
        if(descriptor == null) {
            this.mColoredCircleBackground.getPaint().setColor(ContextCompat.getColor(mContext, clusterOutlineColor));
            Bitmap b = this.mIconGenerator.makeIcon("");
//            int stroke = b.getWidth() > b.getHeight()?b.getWidth():b.getHeight();
            b = Bitmap.createScaledBitmap(b, Utils.dpToPx(mContext, 20), Utils.dpToPx(mContext, 20), true);
            descriptor = BitmapDescriptorFactory.fromBitmap(b);
            this.mIcons.put(bucket, descriptor);
        }
        markerOptions.icon(descriptor);
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<MapItem> cluster,
                                           MarkerOptions markerOptions) {
//        int bucket = this.getBucket(cluster);
        int bucket = cluster.getSize();
        BitmapDescriptor descriptor = (BitmapDescriptor)this.mIcons.get(bucket);
        if(descriptor == null) {
            this.mColoredCircleBackground.getPaint().setColor(ContextCompat.getColor(mContext, clusterOutlineColor));
            Bitmap b = this.mIconGenerator.makeIcon(String.valueOf(bucket));
            int stroke = b.getWidth() > b.getHeight()?b.getWidth():b.getHeight();
            b = Bitmap.createScaledBitmap(b, stroke, stroke, true);
            descriptor = BitmapDescriptorFactory.fromBitmap(b);
            this.mIcons.put(bucket, descriptor);
        }

        markerOptions.icon(descriptor);
    }

    private FreeTextView makeSquareTextView(Context context) {
        FreeTextView squareTextView = new FreeTextView(context);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
        squareTextView.setLayoutParams(layoutParams);
        squareTextView.setId(com.google.maps.android.R.id.amu_text);
        squareTextView.setTextSizeFitSp(25.0f);
        squareTextView.setTextColor(Color.WHITE);
        squareTextView.setGravity(Gravity.CENTER);
        int twelveDpi = (int) (10F * this.mDensity);
        squareTextView.setPadding(twelveDpi, twelveDpi, twelveDpi, twelveDpi);
        return squareTextView;
    }

    private Drawable makeClusterBackground() {
        this.mColoredCircleBackground = new ShapeDrawable(new OvalShape());
        ShapeDrawable outline = new ShapeDrawable(new OvalShape());
        outline.getPaint().setColor(Color.WHITE);
        outline.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
        int strokeWidth = (int)(this.mDensity * 3.0F);
        LayerDrawable layer = new LayerDrawable(new Drawable[]{outline, this.mColoredCircleBackground});
        layer.setLayerInset(1, strokeWidth, strokeWidth, strokeWidth, strokeWidth);
        return layer;
    }


}
