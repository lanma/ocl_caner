package org.ourcitylove.friendlycancer.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import org.ourcitylove.friendlycancer.model.Value;

/**
 * Created by Vegetable on 2016/7/9.
 */

public class MapItem implements ClusterItem{
    private final Value Value;

    public MapItem(Value Value){this.Value = Value;}
    @Override
    public LatLng getPosition() {
        return new LatLng(Value.lat, Value.lng);
    }

    @Override
    public String getTitle() {
        return "";
    }

    @Override
    public String getSnippet() {
        return "";
    }
}
