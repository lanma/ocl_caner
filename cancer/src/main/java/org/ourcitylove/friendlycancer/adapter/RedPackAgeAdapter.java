package org.ourcitylove.friendlycancer.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jakewharton.rxbinding.view.RxView;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.layout.RedPackCell;
import org.ourcitylove.friendlycancer.layout.redpackballotcell;
import org.ourcitylove.friendlycancer.model.MenuValue;

import java.util.List;

public class RedPackAgeAdapter extends RecyclerView.Adapter<RedPackAgeAdapter.ViewHolder> {
    private List<MenuValue> list;

    public RedPackAgeAdapter(List<MenuValue> list){
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType != 0)
            return new ViewHolder(new RedPackCell(parent.getContext()));
        else return new ViewHolder(new redpackballotcell(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MenuValue mvalue = list.get(position);

        if (getItemViewType(position) == 1) {
            ((RedPackCell) holder.cell).Title.setText(String.format("%s篩檢", mvalue.titleTw));
            GradientDrawable bgcolor = (GradientDrawable) holder.cell.getBackground();
            bgcolor.setStroke(2, Color.parseColor(mvalue.cellcolor));
            holder.cell.invalidate();

            Glide.with(holder.cell.getContext()).load(Uri.parse(mvalue.getAssetsImageFileName())).diskCacheStrategy(DiskCacheStrategy.SOURCE).fitCenter().into(((RedPackCell) holder.cell).icon);

            RxView.clicks(holder.cell)
                    .subscribe(v ->
                            App.enterExpanded(holder.cell.getContext(), list.get(position).Id,
                                    list.get(position).imagefilename.replace("menu", "navi"),
                                    list.get(position).titleTw, list.get(position).extra, list.get(position).cellcolor));
        } else {
            ((redpackballotcell) holder.cell).title.setText(mvalue.titleTw);
            RxView.clicks(holder.cell)
                    .subscribe(v -> {
                        String url = "https://ourcitylovewebapps.azurewebsites.net/cancer/#/custom";
                        App.enterEmbeddedWeb(holder.cell.getContext(), url);

//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                        holder.cell.getContext().startActivity(intent);

//                        SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT, Locale.TAIWAN);
//                        try {
//                            Date eventEnd = formatter.parse("2016/11/30 23:59:59");
////                            Date result = formatter.parse("2016/12/15 00:00:00");
//                            Date now = new Date();
//                            if (now.before(eventEnd))
//                                App.enterScreenBallot(holder.cell.getContext(), "", "", "篩檢抽好禮", "", "", null);
//                            else {
//                                EndLessLayout ell = new EndLessLayout(holder.cell.getContext()).buildDialog();
//                                ell.contentview.setText("獲獎名單於105年12月30日公告於臺北市政府衛生局網站。");
//                                SpannableString sbs = new SpannableString(holder.cell.getResources().getText(R.string.endless_url));
//                                sbs.setSpan(new UnderlineSpan(), 0, sbs.length(), 0);
//                                ell.URL.setText(sbs);
//                                ell.URL.setOnClickListener(vv-> App.enterWeb(holder.cell.getContext(), sbs.toString().replaceAll("[()]]", "")));
//                                ell.contentview1.setText("謝謝您！");
//                            }
//                        } catch (ParseException e) {
//                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {return list.get(position).celltype == null?0:1;}

    @Override
    public int getItemCount() {
        return list.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{
        View cell;
        public ViewHolder(View itemView) {
            super(itemView);
            cell = itemView;
        }
    }
}
