package org.ourcitylove.friendlycancer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.activity.LoveAngel;
import org.ourcitylove.friendlycancer.helper.Helper;
import org.ourcitylove.friendlycancer.helper.AddViewHelper;
import org.ourcitylove.friendlycancer.layout.DialogLayout;
import org.ourcitylove.friendlycancer.layout.EndLessLayout;
import org.ourcitylove.friendlycancer.layout.PhoneCall;
import org.ourcitylove.friendlycancer.layout.expandedcell;
import org.ourcitylove.friendlycancer.layout.noarmalballot;
import org.ourcitylove.friendlycancer.layout.normalcell;
import org.ourcitylove.friendlycancer.layout.normalcolorcell;
import org.ourcitylove.friendlycancer.layout.normalgridcell;
import org.ourcitylove.friendlycancer.layout.normalredpackcell;
import org.ourcitylove.friendlycancer.model.EventModel;
import org.ourcitylove.friendlycancer.model.FuncModel;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.MenuValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.ourcitylove.friendlycancer.utils.Utils.DATE_TIME_FORMAT;

/**
 * Created by Vegetable on 2016/3/20.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.Menu_ViewHolder>{

    private List<MenuValue> mlist;
    private Context context;
    public MenuAdapter(Context context , List mlist){
        this.context = context;
        this.mlist = mlist;
    }

    @Override
    public Menu_ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = new View(context);
        switch (viewType){
            case 0://normalcell
                mview = new normalcell(context);
                break;
            case 1://normalcolorcell
                mview = new normalcolorcell(context);
                break;
            case 2://expandedcell
                mview = new expandedcell(context);
                break;
            case 3://backgroundimagecell
                mview = new ImageView(context);
                break;
            case 4://normalgridcell
                mview = new normalgridcell(context);
                break;
            case 5://normalredpackcell
                mview = new normalredpackcell(context);
                break;
            case 6://noarmalballot
                mview = new noarmalballot(context);
        }
        return new Menu_ViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(Menu_ViewHolder holder, int position) {
        MenuValue mdata = mlist.get(position);
        switch (getItemViewType(position)){
            case 5: //normalredpackcell
            case 6: //noarmalballot
            case 0: {//normalcell
                normalcell mlayout = (normalcell) holder.itemView;
                mlayout.title.setText(mdata.titleTw);
                Glide.with(context).load(Uri.parse(mdata.getAssetsImageFileName())).diskCacheStrategy(DiskCacheStrategy.SOURCE).fitCenter().into(mlayout.image);
            }
                break;
            case 1: {//normalcolorcell
                normalcolorcell mlayout = (normalcolorcell)holder.itemView;
                Glide.with(context).load(Uri.parse(mdata.getAssetsImageFileName())).diskCacheStrategy(DiskCacheStrategy.SOURCE).fitCenter().into(mlayout.image);
                mlayout.title.setText(mdata.titleTw);
            }
                break;
            case 2: {//expandedcell
                expandedcell mlayout = (expandedcell)holder.itemView;
                mlayout.title.setText(mdata.titleTw);
                try {
                    mlayout.mlayout.setBackgroundColor(Color.parseColor(mdata.extra));
                }catch (Exception e){}

                String[] modeltype = mdata.p1.split("\\$\\$");
                String[] modelcontent = mdata.p2.split("\\$\\$");
                if(!modeltype[0].equals("") && !modelcontent[0].equals("")) {
                    AddViewHelper.addView(mlayout.mlayout, modeltype, modelcontent, false);
                    mlayout.mlayout.setTag(true);
                    mlayout.mlayout.setVisibility(View.GONE);
                }
            }
                break;
            case 3: {//backgroundimagecell
                ImageView mlayout = (ImageView)holder.itemView;
                mlayout.setScaleType(ImageView.ScaleType.FIT_CENTER);
                Glide.with(context).load(mdata.getAssetsImageFileName()).diskCacheStrategy(DiskCacheStrategy.SOURCE).fitCenter().into(mlayout);
            }
                break;
            case 4: {//normalgridcell
                normalgridcell normalgridcell = (normalgridcell)holder.itemView;
                Glide.with(context).load(mdata.getAssetsImageFileName()).fitCenter().into(normalgridcell.appicon);
                try {
                    String[] path = mdata.extra.split("\\$\\$");
                    normalgridcell.appicon.setOnClickListener(v -> context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(path[0]))));
                }catch(Exception e){}
            }
            break;
        }
        holder.itemView.setOnClickListener(v -> onmenuclicklistener(v,position));
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    @Override
    public int getItemViewType(int position) {
        int celltype = mlist.get(position).celltype.equals("")?0: FuncModel.celltype.get(mlist.get(position).celltype);
        return celltype;
    }

    private void onmenuclicklistener(View v,int position){
        switch (FuncModel.downtype.get(mlist.get(position).downtype)){
            case 0://進搜索
            {
                App.enterToWhere(context, mlist.get(position).imagefilename.replace("menu","navi"),
                                mlist.get(position).titleTw);
            }
                break;
            case 1://列表頁
            {
                App.enterMainPage(context,mlist.get(position).Id,
                            mlist.get(position).imagefilename.replace("menu","navi"),mlist.get(position).titleTw,
                        mlist.get(position).extra,mlist.get(position).cellcolor, null);
            }
                break;
            case 2://小天使
            {
                Intent toAngel = new Intent(context, LoveAngel.class);
                Bundle bundle = new Bundle();

                bundle.putBoolean(KEYMODEL.NAVI,true);
                bundle.putString(KEYMODEL.NAVI_ICON,mlist.get(position).imagefilename.replace("menu","navi"));
                bundle.putString(KEYMODEL.NAVI_TITLE,mlist.get(position).titleTw);
                bundle.putBoolean(KEYMODEL.BACK,true);
                bundle.putBoolean(KEYMODEL.NAVI_TITLE_ICON,true);
                toAngel.putExtras(bundle);

                context.startActivity(toAngel);
            }
                break;
            case 3://打電話
            {
                String[] modeltype = mlist.get(position).p1.split("\\$\\$");
                String[] modelcontent = mlist.get(position).p2.split("\\$\\$");
                String number = mlist.get(position).extra;
                PhoneCall phoneCall = new PhoneCall(context,modeltype,modelcontent);
                Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(phoneCall);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                phoneCall.cancel.setOnClickListener(cancelview -> dialog.dismiss());
                if(number != null && !number.equals(""))
                    phoneCall.call.setOnClickListener(callview -> call(context, number));
                dialog.show();
            }
                break;
            case 4://收藏櫃
            {
                Helper.GetFavorClinic(context.getApplicationContext())
                        .subscribe(mdata -> App.enterClinic(context, mlist.get(position).imagefilename.replace("menu","navi"),
                                mlist.get(position).titleTw, true, new EventModel(EventModel.CONTINUE, mdata)));
            }
                break;
            case 6://下拉展開
            {
                expandedcell expandedcell = (expandedcell)v;
                View content = expandedcell.mlayout;
                if (content.getVisibility() == View.VISIBLE) {
                    Animation fold = AnimationUtils.loadAnimation(v.getContext(),
                            R.anim.view_scale_y_drop_reverse);
                    fold.setAnimationListener(new Animation.AnimationListener() {

                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            content.setVisibility(View.GONE);
                        }
                    });
                    content.startAnimation(fold);
                } else {
                    content.setVisibility(View.VISIBLE);
                    Animation unfold = AnimationUtils.loadAnimation(
                            v.getContext(), R.anim.view_scale_y_drop);
                    unfold.setAnimationListener(new Animation.AnimationListener() {

                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            content.setVisibility(View.VISIBLE);
                        }
                    });
                    content.startAnimation(unfold);
                }
            }
                break;
            case 8://進展開列表頁
            {
                App.enterExpanded(context, mlist.get(position).Id, mlist.get(position).imagefilename.replace("menu","navi"),
                        mlist.get(position).titleTw, mlist.get(position).extra, mlist.get(position).cellcolor);
            }
            break;
            case 9://進健康撲滿
            {
                App.enterRedPack(context, mlist.get(position).titleTw, false);
            }
            break;
            case 7://外部連結
            {

            }
            break;
            case 5://不動作
            {

            }
            break;
            case 10://進健康APP
            {
                App.enterHealth(context,mlist.get(position).Id,
                        mlist.get(position).imagefilename.replace("menu","navi"),mlist.get(position).titleTw,
                        mlist.get(position).extra,mlist.get(position).cellcolor, null);
            }
            break;
            case 11://進小百科
            {
                App.enterEncyclopedia(context,mlist.get(position).Id,
                        mlist.get(position).imagefilename.replace("menu","navi"),mlist.get(position).titleTw,
                        mlist.get(position).extra,mlist.get(position).cellcolor, null);
            }
            break;
            case 12://進下載抽抽樂
            {

                SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT, Locale.TAIWAN);
                try {
                    Date eventEnd = formatter.parse("2016/11/30 23:59:59");
                    Date result = formatter.parse("2016/12/30 00:00:00");
                    Date now = new Date();
                    if(now.before(eventEnd)) {
                        if (App.pref.getBoolean(KEYMODEL.HAS_DownLoadBallot, false)) {
                            DialogLayout dl = new DialogLayout(context);
                            dl.text.setText(R.string.downloadballot_edit_confirm);
                            dl.addButton(android.R.string.cancel, mv -> dl.dialog.dismiss());
                            dl.addButton(android.R.string.ok, mv -> {
                                dl.dialog.dismiss();
                                App.enterDownloadBallot(context, mlist.get(position).Id,
                                        mlist.get(position).imagefilename
                                                .replace("ic", "navi"), mlist.get(position).titleTw,
                                        mlist.get(position).extra, mlist.get(position).cellcolor, null);
                            });
                            dl.buildDialog();

                        } else {
                            App.enterDownloadBallot(context, mlist.get(position).Id,
                                    mlist.get(position).imagefilename
                                            .replace("ic", "navi"), mlist.get(position).titleTw,
                                    mlist.get(position).extra, mlist.get(position).cellcolor, null);
                        }
                    }
                        else {
                            new EndLessLayout(context).buildDialog();
                        }
//                    }else if(now.after(eventEnd) && now.before(result)){//抽獎結束公佈前
//                        EndLessLayout ell = new EndLessLayout(context).buildDialog();
//                    }else if(now.after(result)) {//開始公佈
//                        App.enterBallotResult(context, list.get(position).Id,
//                                list.get(position).imagefilename
//                                        .replace("ic", "navi"), list.get(position).titleTw,
//                                list.get(position).extra, list.get(position).cellcolor, null);
//                    }
                }catch (ParseException ignore){}
            }
            break;
            default:
                break;
        }
    }

    public static void call(Context context, String tel) {
        tel = tel.replaceAll("[()-]", "");
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + tel));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    class Menu_ViewHolder extends RecyclerView.ViewHolder{
        /**
         * Create a new SquidViewHolder instance.
         *
         * @param itemView the item view
        //* @param item     a model instance. This item does not need to be populated with data; it will be populated
         *                 automatically in {@link }.
         */
        public Menu_ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
