package org.ourcitylove.friendlycancer.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.james.views.FreeTextView;

import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.activity.ClinicInfoActivity;
import org.ourcitylove.friendlycancer.helper.clinichelper;
import org.ourcitylove.friendlycancer.layout.ClinicListLayout;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.Value;
import org.ourcitylove.oclapp.LocationManager;
import org.parceler.Parcels;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vegetable on 2016/3/31.
 */
public class ClinicAdapter extends RecyclerView.Adapter<ClinicAdapter.Clinic_ViewHolder>{
    private List<Value> mlist;
    private Context context;


    public LinkedHashMap<String,String> mtype = new LinkedHashMap();

    public ClinicAdapter(Context context,List mlist){
        this.mlist = mlist;
        this.context = context;
    }


    @Override
    public Clinic_ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Clinic_ViewHolder(new ClinicListLayout(context));
    }

    @Override
    public void onBindViewHolder(Clinic_ViewHolder holder, int position) {
        Value mdata = mlist.get(position);
        ClinicListLayout mlayout = (ClinicListLayout)holder.itemView;
        mlayout.title.setText(mdata.title);

        mtype.put("大腸癌", mdata.colorectalCancer);
        mtype.put("乳癌", mdata.brestCancer);
        mtype.put("口腔癌", mdata.oralCancer);
        mtype.put("子宮頸癌", mdata.cervixCancer);
        int num = 0;

        mlayout.resetcontenttype();
        for(Map.Entry<String, String> entry:mtype.entrySet()){
            if(!entry.getValue().equals("●"))continue;
            FreeTextView child = (FreeTextView)mlayout.contenttype.getChildAt(num);
            child.setText(entry.getKey());
            child.setBackgroundColor(Color.parseColor(clinichelper.bgcolor.get(entry.getKey())));
            num++;
        }

//        mlayout.contenttype.findViewWithTag("colorectal_cancer").setVisibility(mdata.colorectalCancer.equals("●")?View.VISIBLE:View.GONE);
//        mlayout.contenttype.findViewWithTag("brest_cancer").setVisibility(mdata.brestCancer.equals("●")?View.VISIBLE:View.GONE);
//        mlayout.contenttype.findViewWithTag("oral_cancer").setVisibility(mdata.oralCancer.equals("●")?View.VISIBLE:View.GONE);
//        mlayout.contenttype.findViewWithTag("cervix_cancer").setVisibility(mdata.cervixCancer.equals("●")?View.VISIBLE:View.GONE);

        if(mdata.distance!=null)
            mlayout.distance.setVisibility( mdata.distance == 0?View.INVISIBLE:View.VISIBLE);
        mlayout.distance.setText(LocationManager.getDisplayDistance(context,mdata.distance));

        mlayout.setOnClickListener(v->clicklistener(mdata));
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public void notifyDataSetChanged(List mlist){
        this.mlist = mlist;
        notifyDataSetChanged();
    }

    private void clicklistener(Value mdata) {
        Intent mintent = new Intent(context, ClinicInfoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(KEYMODEL.NAVI, true);
        bundle.putString(KEYMODEL.NAVI_TITLE, context.getString(R.string.clinicinfo));
        bundle.putBoolean(KEYMODEL.BACK, true);
        bundle.putParcelable(KEYMODEL.DATA, Parcels.wrap(mdata));
        mintent.putExtras(bundle);
        context.startActivity(mintent);
    }

    class Clinic_ViewHolder extends RecyclerView.ViewHolder{
        /**
         * Create a new SquidViewHolder instance.
         *
         * @param itemView the item view
        //* @param item     a model instance. This item does not need to be populated with data; it will be populated
         *                 automatically in {@link }.
         */
        public Clinic_ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
