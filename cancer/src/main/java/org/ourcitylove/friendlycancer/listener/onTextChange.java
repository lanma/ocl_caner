package org.ourcitylove.friendlycancer.listener;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Vegetable on 2016/6/21.
 */

public abstract class onTextChange implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable s) {getText(s.toString());}

    public abstract void getText(CharSequence text);
}
