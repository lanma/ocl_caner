package org.ourcitylove.friendlycancer.listener;

import android.view.animation.Animation;

/**
 * Created by Vegetable on 2016/6/22.
 */

public abstract class AnimationListner implements Animation.AnimationListener
{
    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        onAnimation(animation);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    protected abstract void onAnimation(Animation animation);
}
