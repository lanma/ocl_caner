package org.ourcitylove.friendlycancer.converter;

import com.alibaba.fastjson.JSON;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.model.ClinicModel;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.MenuModel;
import org.ourcitylove.friendlycancer.model.MenuValue;
import org.ourcitylove.friendlycancer.model.Value;

import java.util.List;


/**
 * Created by Vegetable on 2016/3/22.
 */
public class Converter {
    public static List<MenuValue> MenuConverter() {
        String menustring = App.pref.getString(KEYMODEL.MENU, "");
        if (menustring.equals(""))
            return null;
        MenuModel model = JSON.parseObject(menustring, MenuModel.class);
        return model.values;
    }

    public static List<Value> ClinicConverter() {
        String clinicstring = App.pref.getString(KEYMODEL.ClINIC, "");
        if (clinicstring.equals(""))
            return null;
        ClinicModel model = JSON.parseObject(clinicstring, ClinicModel.class);
        return model.values;
    }
}
