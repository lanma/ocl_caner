package org.ourcitylove.friendlycancer.helper;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.MenuValue;
import org.ourcitylove.friendlycancer.model.RedPackageModel;

import java.util.Calendar;
import java.util.regex.Pattern;

import io.reactivex.Flowable;

/**
 * Created by Vegetable on 2016/6/13.
 */

public class Red_Pack_Gift {

    public static Flowable<MenuValue> getcolorectal(RedPackageModel gift){//大腸癌
        final int years = convertToyears(gift.years);
       return Flowable.fromIterable(App.menuvalue)
               .filter(mv->mv.titleTw.equals(KEYMODEL.colorectal))
               .take(1)
               .filter(mv-> 50<=years & years<=75)
               .filter(mv->Pattern.compile("男|女").matcher(gift.Gender).find());//years
    }
    public static Flowable<MenuValue> getbrest(RedPackageModel gift){//乳癌
        final int years = convertToyears(gift.years);
        return Flowable.fromIterable(App.menuvalue)
                .filter(mv->mv.titleTw.equals(KEYMODEL.brest))
                .take(1)
                .filter(mv->Pattern.compile("女").matcher(gift.Gender).find())
                .filter(mv-> {
                    if (45 <= years & years <= 70) return true;
                    else if (40 <= years & years < 45)
                        return gift.check.contains("二等親以內血親曾患有乳癌");
                    return false;});
    }
    public static Flowable<MenuValue> getoral(RedPackageModel gift){//口腔癌
        final int years = convertToyears(gift.years);
        return Flowable.fromIterable(App.menuvalue)
                .filter(mv->mv.titleTw.equals(KEYMODEL.oral))
                .take(1)
                .filter(mv->Pattern.compile("男|女").matcher(gift.Gender).find())//years
                .filter(mv->{
                    if(years>=30)
                        return gift.check.contains("現有吸菸習慣") || gift.check.contains("現在/曾有嚼檳榔習慣");
                    if(18<=years&years<=30)
                        return gift.check.contains("現在/曾有嚼檳榔習慣") && gift.check.contains("我是原住民");
                    return false;
                });
    }
    public static Flowable<MenuValue> getcervical(RedPackageModel gift){//子宮頸癌
        final int years = convertToyears(gift.years);
        return Flowable.fromIterable(App.menuvalue)
                .filter(mv->mv.titleTw.equals(KEYMODEL.cervical))
                .take(1)
                .filter(mv->Pattern.compile("女").matcher(gift.Gender).find())
                .filter(mv->30<=years)
//                .doOnNext(mv-> mv.titleTw = mv.titleTw.replaceAll("癌", "抹片"))
                ;
    }

    private static int convertToyears(int yearly){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int years = calendar.get(Calendar.YEAR)-1911;
        return Math.abs(years - yearly);
    }


}
