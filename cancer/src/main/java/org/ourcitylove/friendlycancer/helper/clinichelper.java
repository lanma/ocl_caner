package org.ourcitylove.friendlycancer.helper;

import android.location.Location;

import org.ourcitylove.friendlycancer.model.ClinicModel;
import org.ourcitylove.friendlycancer.model.Value;
import org.ourcitylove.oclapp.LocationManager;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * Created by Vegetable on 2016/6/7.
 */
public class clinichelper {
    public static Map<String,String> bgcolor = new Hashtable(){{
        put("大腸癌" ,"#00c97e");
        put("乳癌"      ,"#9f45b5");
        put("口腔癌"       ,"#00c2e8");
        put("子宮頸癌"     ,"#ff5987");
    }};

    private Location location;

    public Observable<List<Value>> getClinic(List<Value> mvalue, Location location){
        if(location!=null)this.location = location;

        Observable ob = Observable.just(mvalue);
        if (this.location != null)
            ob = ob.compose(updateDistance());
        return ob;

    }

    private final Observable.Transformer<List<Value>, List<Value>> distanceTransformer =
            observable -> observable
                    .flatMap(Observable::from)
                    .map(clinic ->{
                        clinic.distance = (LocationManager.getDistance(clinic.lat, clinic.lng,
                                location.getLatitude(), location.getLongitude()));
                        return clinic;})
                    .toSortedList(ClinicModel::compareByDistance);

    @SuppressWarnings("unchecked")
    private <T> Observable.Transformer<T, T> updateDistance() {
        return (Observable.Transformer<T, T>) distanceTransformer;
    }
}
