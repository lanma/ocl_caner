package org.ourcitylove.friendlycancer.model;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Vegetable on 2016/3/22.
 */

@Parcel
public class EventModel {
    public EventModel(){}
    public EventModel(int eventType){this.EventType = eventType;}
    public EventModel(int eventType,List mdata){this.EventType = eventType;this.Mdata = mdata;}
    public int EventType;
    public final static int CONTINUE = 0;
    public final static int Pause = 1;

    public List<Value> Mdata;
}
