package org.ourcitylove.friendlycancer.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vegetable on 2016/3/21.
 */
public class MenuModel {
    @JSONField(name = "result")
    public String result = "";
    @JSONField(name = "values")
    public List<MenuValue> values = new ArrayList<>();
}
