package org.ourcitylove.friendlycancer.model;

/**
 * Created by Vegetable on 2016/3/22.
 */
public class KEYMODEL {
    public final static String MENU = "MENUID";
    public final static String ClINIC = "CLINICID";
    public final static String BANNER = "AD_BANNER";
    public final static String BACKGROUNDIMG = "BACKGROUNDIMG";
    public final static String NAVI = "NAVI";
    public final static String BACK = "BACK_BUTTON";
    public final static String NAVI_TITLE_ICON = "NAVI_TITLE_ICON";
    public final static String NAVI_ICON = "NAVI_ICON";
    public final static String NAVI_TITLE = "NAVI_TITLE";
    public final static String NAVI_COLOR = "NAVI_COLOR";
    public final static String NAVI_MAP = "NAVI_MAP";
    public final static String FIRST = "FIRST";
    public final static String FAVOR = "FAVOR";

    public final static String DOLLAR = "dollar";

    public final static String MENUID = "MENU_ID";

    public final static String DATA ="DATA";

    public final static String colorectal = "大腸癌";
    public final static String brest = "乳癌";
    public final static String oral = "口腔癌";
    public final static String cervical = "子宮頸癌";

    public final static String FAVORITE = "FAVORITE";

    public final static String FieldName = "FieldName";
    public final static String FieldPhone = "FieldPhone";
    public final static String FieldAddress = "FieldAddress";
    public final static String HAS_DownLoadBallot = "HAS_DownLoadBallot";

    public final static String BallotWin = "BallotWin";
    public final static String BallotTitle = "BallotTitle";
    public final static String BallotSubTitle = "BallotSubTitle";
    public final static String BallotType = "BallotType";

}
