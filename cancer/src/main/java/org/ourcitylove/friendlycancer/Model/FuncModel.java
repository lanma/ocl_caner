package org.ourcitylove.friendlycancer.model;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Vegetable on 2016/4/16.
 */
public class FuncModel {
    public static Map<String,Integer> celltype = new HashMap<String, Integer>(){{
        put("normalcell",         0);
        put("normalcolorcell",    1);
        put("expandedcell",       2);
        put("backgroundimagecell",3);
        put("normalgridcell",     4);
        put("normalredpackcell"  ,5);
        put("noarmalballot"      ,6);
    }};
    public static Map<String,Integer> downtype = new Hashtable<String,Integer>(){{
        put("進搜索"         ,0);
        put("進列表頁"       ,1);
        put("進小天使"       ,2);
        put("打電話"        ,3);
        put("進收藏櫃"      ,4);
        put("不動作"        ,5);
        put("下拉展開"      ,6);
        put("外部連結"      ,7);
        put("進展開列表頁"   ,8);
        put("進健康撲滿"   ,9);
        put("進健康APP"   ,10);
        put("進小百科"    ,11);
        put("進下載抽抽樂" ,12);
    }};
}
