package org.ourcitylove.friendlycancer.model;

/**
 * Created by Vegetable on 2016/3/30.
 */
public class SearchModel {
    public final static TYPE type_searchbar = TYPE.searchbar;
    public final static TYPE type_title = TYPE.title;
    public final static TYPE type_button = TYPE.button;
    public final static TYPE type_checklabel = TYPE.checklabel;
    public final static TYPE type_multibutton = TYPE.multibutton;
    public final static TYPE type_titlenoiconwithimage = TYPE.titlenoiconwithimage;
    public final static TYPE type_text = TYPE.text;
    public final static TYPE type_searchbarnomirror = TYPE.searchbarnomirror;
    public final static TYPE type_leftimage = TYPE.leftimage;
    public final static TYPE type_titlenoicon = TYPE.titlenoicon;
    public final static TYPE type_labelandedit = TYPE.labelandedit;
    public final static TYPE type_submit = TYPE.submit;
    public final static TYPE type_leftimagewithcall = TYPE.leftimagewithcall;

    public final static String group_search = "搜尋";
    public final static String group_screen = "篩檢";
    public final static String group_area = "區域";
    public final static String group_time = "時段";
    public final static String group_workday = "平日時段";
    public final static String group_holiday_saturday = "假日時段-週六";
    public final static String group_holiday_sunday = "假日時段-週日";
    public final static String group_Name = "名稱";
    public final static String group_addr = "地址";
    public final static String group_apply = "申請項目";
    public final static String group_communication = "聯絡資訊";

    public final static String Brest = "乳癌";
    public final static String Cervix = "子宮頸癌";
    public final static String Cervix_Woman = "子宮頸癌篩檢指定女醫師";
    public final static String Oral = "口腔癌";
    public final static String Colorectal = "大腸癌";


    public SearchModel(String group, TYPE type, String title, String description) {
        this.group = group;
        this.type = type;
        this.title = title;
        this.description = description;
    }

    public String group = "";
    public TYPE type;
    public String title = "";
    public String description = "";

    public enum TYPE {
        searchbar,
        title,
        button,
        checklabel,
        multibutton,
        titlenoiconwithimage,
        text,
        searchbarnomirror,
        leftimage,
        titlenoicon,
        labelandedit,
        submit,
        leftimagewithcall,
    }
}
