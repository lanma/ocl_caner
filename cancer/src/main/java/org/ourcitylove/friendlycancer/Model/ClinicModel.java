package org.ourcitylove.friendlycancer.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;
import java.util.List;

public class ClinicModel {
    @JSONField( name = "result")
    public String result;
    @JSONField( name = "values")
    public List<Value> values = new ArrayList<Value>();

    public static int compareByDistance(Value p1, Value p2) {
        return p1.distance.compareTo(p2.distance);
    }
}
