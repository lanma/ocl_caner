package org.ourcitylove.friendlycancer.helper;

import android.content.Context;

import org.ourcitylove.friendlycancer.library.Memory;

import java.io.File;


/**
 * Created by Vegetable on 2016/3/22.
 */
public class FileHelper {
    public static File getFileDir(Context context){
        String custom_path = new Memory(context).getString("custom_storage_path");
        File root;
        if (custom_path.equals("")) {
            root = context.getExternalFilesDir(null);
            if (root == null)
                root = context.getFilesDir();
        } else {
            root = new File(custom_path);
        }
        if(!root.exists())
            root.mkdir();
        return root;
    }

    public static File getCacheDir(Context context){
        String custom_path = new Memory(context).getString("custom_storage_path");
        File root;
        if (custom_path.equals("")) {
            root = context.getExternalCacheDir();
            if (root == null)
                root = context.getCacheDir();
        } else {
            root = new File(custom_path);
        }
        return root;
    }
}
