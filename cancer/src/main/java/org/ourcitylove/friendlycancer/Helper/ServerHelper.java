package org.ourcitylove.friendlycancer.helper;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.greenrobot.eventbus.EventBus;
import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.converter.StringConverterFactory;
import org.ourcitylove.friendlycancer.model.EventModel;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.Status;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.fastjson.FastJsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Vegetable on 2016/3/21.
 */
public class ServerHelper {
    private final static String baseurl = "https://script.google.com//macros/s/AKfycbzl4QE2RpAUJKFhNSOas0AguI8sF6HKfhsMJcQHHqbPpI_tBgQ/";
    private final static String apiurl = "exec";
    private static API api;
    private interface API{
        @GET(apiurl)
        Flowable<String> GetData(@Query("sheetName")String sheetname);
        @POST(apiurl)
        Flowable<Void> SaveGameLog(@QueryMap(encoded = true)Map<String,String> map);

        @POST(apiurl+"?fun=AddDownLoadList")
        Flowable<Status> AddDownLoadList(@QueryMap Map<String, String> query);
        @POST(apiurl+"?fun=UpdateDownLoadList")
        Flowable<Status> UpdateDownLoadList(@QueryMap Map<String, String> query);
        @POST(apiurl+"?fun=AddScreenList")
        Flowable<Status> AddScreenList(@QueryMap Map<String, String> query);
        @POST(apiurl+"?fun=UpdateScreenList")
        Flowable<Status> UpdateScreenList(@QueryMap Map<String, String> query);

    }

    public static void init(){
        // Log信息
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // OkHttp3.0的使用方式
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseurl)
                .client(client)
                .addConverterFactory(FastJsonConverterFactory.create())
                .addConverterFactory(StringConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
        api = retrofit.create(API.class);
    }

    public static void getMenuData(){
            api.GetData("Menu")
                    .subscribe(s -> App.pref.edit().putString(KEYMODEL.MENU,s).commit(),
                            throwable -> {},
                            ServerHelper::getClinicData);
    }
    public static void getClinicData(){
        api.GetData("New_Clinic")
                .subscribe(s -> {
                    App.pref.edit().putString(KEYMODEL.ClINIC, s).apply();
                    EventBus.getDefault().postSticky(new EventModel(EventModel.CONTINUE));
                }, throwable -> {});
    }
    public static void SaveGameLog(String gender, String years,
                                   String colorectal, String brest, String oral, String cervical,
                                   String dollar, String question){
        Map<String,String> map = new HashMap(){{
            put("fun", "logsaved");
            put("sheetName", "Gift_Log");
            put("gender",gender);
            put("years",years);
            put("colorectal",colorectal);
            put("brest",brest);
            put("oral",oral);
            put("cervical",cervical);
            put("dollar",dollar);
            put("question",question);}};
        api.SaveGameLog(map)
                .subscribe(response->{},
                            throwable -> {});
    }

    public static Flowable<Status> AddDownLoadList(Map query){
        return api.AddDownLoadList(query)
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Flowable<Status> UpdateDownList(Map query){
        return api.UpdateDownLoadList(query)
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Flowable<Status> AddScreenList(Map query){
        return api.AddScreenList(query)
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Flowable<Status> UpdateScreenList(Map query){
        return api.UpdateScreenList(query)
                .observeOn(AndroidSchedulers.mainThread());
    }

}

