package org.ourcitylove.friendlycancer.helper;

import android.content.Context;

import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.MenuValue;
import org.ourcitylove.friendlycancer.model.Value;

import java.util.List;
import java.util.regex.Pattern;

import io.reactivex.Flowable;

/**
 * Created by Vegetable on 2016/3/23.
 */
public class Helper {
    public static Flowable<List<MenuValue>> Separate(List<MenuValue> menuValues, String seperate){return Separate(menuValues, Integer.valueOf(seperate));}
    public static Flowable<List<MenuValue>> Separate(List<MenuValue> menuValues, int seperate) {
        return Flowable.fromIterable(menuValues)
                .filter(menuValue -> menuValue.parentid.equals(String.valueOf(seperate)))
                .toSortedList((Value, Value2) -> Value.orderid - Value2.orderid)
                .toFlowable();
    }

    public static Flowable<MenuValue> SeparateFromTitle(String title){
        return Flowable.fromIterable(App.menuvalue)
                .filter(value-> Pattern.compile(title).matcher(value.titleTw).find())
                .take(1);
    }

    public static Flowable<MenuValue> SeparateFromDownType(String title){
        return Flowable.fromIterable(App.menuvalue)
                .filter(Value -> Pattern.compile(title).matcher(Value.downtype).find())
                .take(1);
    }

    public static Flowable<List<Value>> GetFavorClinic(Context context){
        return Flowable.fromIterable(App.clinicvalue)
                .filter(value -> ((App)context).pref.contains(KEYMODEL.FAVORITE + value.id))
                .toList().toFlowable();
    }
}
