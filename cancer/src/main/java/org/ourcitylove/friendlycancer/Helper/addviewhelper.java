package org.ourcitylove.friendlycancer.helper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.youtube.player.YouTubePlayer;
import com.james.views.FreeEditText;
import com.james.views.FreeLayout;
import com.james.views.FreeTextButton;
import com.james.views.FreeTextView;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.enums.Quality;
import com.thefinestartist.ytpa.utils.YouTubeThumbnail;

import org.greenrobot.eventbus.EventBus;
import org.ourcitylove.friendlycancer.R;
import org.ourcitylove.friendlycancer.layout.button;
import org.ourcitylove.friendlycancer.layout.LabelAndEdit;
import org.ourcitylove.friendlycancer.layout.leftimage;
import org.ourcitylove.friendlycancer.layout.leftimagewithcall;
import org.ourcitylove.friendlycancer.layout.SearchBarLayout;
import org.ourcitylove.friendlycancer.layout.searchbarnomirror;
import org.ourcitylove.friendlycancer.layout.SearchButtonLayout;
import org.ourcitylove.friendlycancer.layout.searchchecklabellayout;
import org.ourcitylove.friendlycancer.layout.searchmultibuttonlayout;
import org.ourcitylove.friendlycancer.layout.searchtitlelayout;
import org.ourcitylove.friendlycancer.layout.titlenoicon;
import org.ourcitylove.friendlycancer.layout.titlenoiconwithimage;
import org.ourcitylove.friendlycancer.layout.titletextlayout;
import org.ourcitylove.friendlycancer.listener.onTextChange;
import org.ourcitylove.friendlycancer.model.SearchModel;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AddViewHelper {
    private static final String assetPath = "file:///android_asset/";

    private static Map<SearchModel.TYPE, Integer> type;
    static {
        type = new LinkedHashMap<>();
        type.put(SearchModel.type_searchbar             ,0);
        type.put(SearchModel.type_title                 ,1);
        type.put(SearchModel.type_button                ,2);
        type.put(SearchModel.type_checklabel            ,3);
        type.put(SearchModel.type_multibutton           ,4);
        type.put(SearchModel.type_titlenoiconwithimage  ,5);
        type.put(SearchModel.type_text                  ,6);
        type.put(SearchModel.type_searchbarnomirror     ,7);
        type.put(SearchModel.type_leftimage             ,8);
        type.put(SearchModel.type_titlenoicon           ,9);
        type.put(SearchModel.type_labelandedit          ,10);
        type.put(SearchModel.type_submit                ,11);
        type.put(SearchModel.type_leftimagewithcall     ,12);
    }

    public static void addView(ViewGroup rootlayout, String[] modeltype, String[] modelcontent, boolean center) {
        if(Boolean.TRUE == rootlayout.getTag())return;
        rootlayout.removeAllViews();
        for (int index = 0; index < modeltype.length; index++) {
            switch (modeltype[index].toLowerCase()) {
                case "text": {
                    FreeTextView content = new FreeTextView(rootlayout.getContext());
                    content.setTextSizeFitSp(30);
                    content.setTextColor(Color.BLACK);
                    content.setPadding(25, 15, 25, 15);
                    rootlayout.addView(content, -1, -2);
                    content.setText(modelcontent[index]);
                    content.setGravity(center ? Gravity.CENTER : Gravity.START);
                }
                    break;
//                case "youtube": {
//                    FreeLayout content = new FreeLayout(rootlayout.getContext());
//                    content.setPicSize(640,-2,FreeLayout.TO_WIDTH);
//                    ImageView youtubebg = (ImageView)content.addFreeView(new ImageView(rootlayout.getContext()),640,-2);
//                    youtubebg.setScaleType(ImageView.ScaleType.CENTER);
//                    youtubebg.setTag(null);
//                    content.setMargin(youtubebg,15, 15, 15, 15);
//                    rootlayout.addView(content, -1, -2);
//                    final String youtubeid = modelcontent[index];
//                    String thumbnailpath = YouTubeThumbnail.getUrlFromVideoId(youtubeid, Quality.STANDARD_DEFINITION);
//                    Glide.with(rootlayout.getContext())
//                            .load(thumbnailpath)
//                            .fitCenter()
//                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                            .into(new SimpleTarget<GlideDrawable>() {
//                                @Override
//                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
//                                        youtubebg.setImageDrawable(resource);
//                                    ImageView mv = (ImageView)content.addFreeView(new ImageView(rootlayout.getContext()),100,70,new int[]{RelativeLayout.CENTER_IN_PARENT});
//                                    mv.setTag(null);
//                                    Glide.with(rootlayout.getContext()).load(R.drawable.youtube).fitCenter().into(mv);
//
//                                }
//                            });
//                    youtubebg.setOnClickListener(v -> {
//                        Intent intent = new Intent(v.getContext(), YouTubePlayerActivity.class);
//
//                        // Youtube video ID (Required, You can use YouTubeUrlParser to parse Video Id from url)
//                        intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, youtubeid);
//
//                        // Youtube player style (DEFAULT as default)
//                        intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);
//
//                        // Screen Orientation Setting (AUTO for default)
//                        // AUTO, AUTO_START_WITH_LANDSCAPE, ONLY_LANDSCAPE, ONLY_PORTRAIT
//                        intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO);
//
//                        // Show audio interface when user adjust volume (true for default)
//                        intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);
//
//                        // If the video is not playable, use Youtube app or Internet Browser to play it
//                        // (true for default)
//                        intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);
//
//                        // Animation when closing youtubeplayeractivity (none for default)
////                        intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_ENTER, R.anim.fade_in);
////                        intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_EXIT, R.anim.fade_out);
//
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        v.getContext().startActivity(intent);
//                    });
//                }
//                break;
                default: {
                    if(modeltype[index].matches("((?i)IMAGE)+\\d{3}")){
                        FreeLayout mlayout = new FreeLayout(rootlayout.getContext());
                        rootlayout.addView(mlayout,-1,-2);
                        String filepath = "image/" + modelcontent[index];
                        String path = assetPath + filepath;
                        int size = Integer.parseInt(modeltype[index].replaceAll("((?i)IMAGE)",""));
                        mlayout.setPicSize(640,-2,FreeLayout.TO_WIDTH);
                        ImageView bg = (ImageView)mlayout.addFreeView(new ImageView(rootlayout.getContext()),size,-2);
                        bg.setScaleType(ImageView.ScaleType.FIT_CENTER);
                            Glide.with(rootlayout.getContext()).load(Uri.parse(path)).fitCenter().into(bg);

                        bg.setTag("IMAGE");
                        break;
                    }
                }
                break;
            }
        }
    }

    public static void addSearchView(ViewGroup mlayout, List<SearchModel> modelList){
        Context context = mlayout.getContext();
        for(SearchModel model : modelList){
            switch (type.get(model.type)){
                case 0://searchbar
                {
                    SearchBarLayout SearchBarLayout = new SearchBarLayout(context);
                    mlayout.addView(SearchBarLayout,-1,-2);
                    SearchBarLayout.searchText.setBackgroundColor(ContextCompat.getColor(mlayout.getContext(),R.color.gray04));
                    SearchBarLayout.searchText.setHint(model.description);
                    SearchBarLayout.setTag(model.group);
                    SearchBarLayout.searchText.setTag(model.title);
                }
                break;
                case 1://title
                {
                    searchtitlelayout searchtitle = new searchtitlelayout(context);
                    searchtitle.title.setText(model.title);
                    mlayout.addView(searchtitle);
                    searchtitle.icon.setOnClickListener(v->{
                        titletextlayout titlelayout = new titletextlayout(mlayout.getContext());
                        titlelayout.buildDialog();

                        titlelayout.title.setText(model.title + mlayout.getContext().getString(R.string.description));
                        titlelayout.content.setText(model.description);
                    });
                }
                break;
                case 2://button
                {
                    SearchButtonLayout buttonlayout = new SearchButtonLayout(context);
                    mlayout.addView(buttonlayout);
                    buttonlayout.addbutton(model.title.split("\\$\\$"));
                    buttonlayout.setTag(model.group);
                }
                break;
                case 3://checklabel
                {
                    searchchecklabellayout contentlayout = new searchchecklabellayout(context);
                    contentlayout.checktext.setText(model.title);
                    contentlayout.setTag(model.group+"check");
//                    contentlayout.setTag(model.title);
                    mlayout.addView(contentlayout);
                }
                break;
                case 4://multibutton
                {
                    searchmultibuttonlayout multibutton = new searchmultibuttonlayout(context);
                    mlayout.addView(multibutton);
                    multibutton.setTag(model.group);
                    multibutton.addbutton(model.title.split("\\$\\$"));
                }
                break;
                case 5://titlenoiconwithimage
                {
                    titlenoiconwithimage titlewithimage = new titlenoiconwithimage(context);
                    mlayout.addView(titlewithimage);
                    titlewithimage.title.setText(model.title);
                    Glide.with(mlayout.getContext()).load(Uri.parse(assetPath + "image/" + model.description)).fitCenter().into(titlewithimage.img);
                }
                break;
                case 6://text
                {
                    FreeTextView textView = new FreeTextView(context);
                    mlayout.addView(textView);
                    textView.setTextSizeFitSp(28.5f);
                    textView.setTextColor(Color.BLACK);
                    textView.setText(model.title);
                    textView.setLineSpacing(1f,1.2f);
                    textView.setPadding(20,20,20,10);
                }
                break;
                case 7://searchbarnomirror
                {
                    searchbarnomirror labelsearchbar = new searchbarnomirror(context);
                    labelsearchbar.searchText.setHint(model.description);
                    labelsearchbar.setTag(model.group);
                    mlayout.addView(labelsearchbar);
                }
                break;
                case 8://leftimage
                {
                    leftimage leftimage = new leftimage(context);
                    Glide.with(mlayout.getContext())
                            .load(Uri.parse(assetPath + "image/" + model.title))
                            .fitCenter()
                            .into(leftimage.img);
                    mlayout.addView(leftimage);
                }
                break;
                case 9://titlenoicon
                {
                    titlenoicon titlenoicon = new titlenoicon(context);
                    titlenoicon.title.setText(model.title);

                    mlayout.addView(titlenoicon);
                }
                break;
                case 10://labelandedit
                {
                    LabelAndEdit LabelAndEdit = new LabelAndEdit(context);
                    LabelAndEdit.titlelabel.setText(model.title);
                    LabelAndEdit.searchText.setHint(model.description);
                    LabelAndEdit.setTag(model.group + model.title);
//                    labelandedit.searchText.setTag(model.title);

                    mlayout.addView(LabelAndEdit);
                }
                break;
                case 11://submit
                {
                    button btn = new button(context);
                    btn.setTag(model.group);
                    btn.setText(model.title);
                    mlayout.addView(btn);
                }
                break;
                case 12://leftimagewithcall
                {
                    leftimage layout = new leftimagewithcall(context);
                    Glide.with(mlayout.getContext())
                            .load(Uri.parse(assetPath + "image/" + model.title))
                            .fitCenter()
                            .into(layout.img);
                    mlayout.addView(layout);
                }
            }
        }
    }

    public static void setSearchListener(HashMap<String, Object> mfilter, List<SearchModel> modelList, ViewGroup mlayout) {
        for (SearchModel model : modelList) {
            String group = model.group;
            switch (type.get(model.type)) {
                case 0://searchbar
                {
                    FreeEditText search = ((FreeEditText) mlayout.findViewWithTag(model.title));
                    search.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            mfilter.put(group, search.getText().toString());
                            postSomething(mfilter);
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                        }

                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }
                    });
                }
                break;
                case 1://title
                {
//                        title no event
                }
                break;
                case 2://button
                {
                    Hashtable mtable = (Hashtable) mfilter.get(group);
                    for (String mtag : model.title.split("\\$\\$")) {
                        mlayout.findViewWithTag(mtag).setOnClickListener(btnlistener(mfilter,group));
                    }
                }
                break;
                case 3://checklabel
                {
                    searchchecklabellayout contentlayout = (searchchecklabellayout) mlayout.findViewWithTag(group + "check");
                    Hashtable mtable = (Hashtable) mfilter.get(group);
                    contentlayout.setOnClickListener(v -> {
                        if (mtable.contains(model.title)) {
                            mtable.remove(model.title);
                            Glide.with(v.getContext()).load(assetPath + "image/search_check_no.png").fitCenter().into(contentlayout.checkicon);
                        } else {
                            mtable.put(model.title, model.title);
                            Glide.with(v.getContext()).load(assetPath + "image/search_check.png").fitCenter().into(contentlayout.checkicon);
                        }
                        postSomething(mfilter);
                    });
                }
                break;
                case 4://multibutton
                {
                    searchmultibuttonlayout multibutton = (searchmultibuttonlayout)mlayout.findViewWithTag(group);
//                    Hashtable mtable = (Hashtable) mfilter.get(group);
                    for (String mtag : model.title.split("\\$\\$")) {
                        View m = multibutton.findViewWithTag(mtag);
                        m.setOnClickListener(btnlistener(mfilter,group));
                    }
                }
                break;
                case 5://titlenoiconwithimage
                {
//                        titlenoiconwithimage titlewithimage = new titlenoiconwithimage(mlayout.getContext());
//                        mlayout.addView(titlewithimage);
//                        titlewithimage.title.setText(model.title);
//                        Glide.with(mlayout.getContext()).load(Uri.parse(assetPath + "image/" + model.description)).fitCenter().into(titlewithimage.img);
                }
                break;
                case 6://text
                {
//                        FreeTextView textView = new FreeTextView(mlayout.getContext());
//                        mlayout.addView(textView);
//                        textView.setTextSizeFitSp(28.5f);
//                        textView.setTextColor(Color.BLACK);
//                        textView.setText(model.title);
////                    textView.setTypeface(Typeface.createFromAsset(mlayout.getContext().getAssets(),"fonts/BGTR00EU.TTF"));
//                        textView.setLineSpacing(1f, 1.2f);
//                        textView.setPadding(20, 20, 20, 10);
                }
                break;
                case 7://searchbarnomirror
                {
                    searchbarnomirror labelsearchbar = (searchbarnomirror)mlayout.findViewWithTag(model.group);
                        labelsearchbar.searchText.addTextChangedListener(new onTextChange() {
                            @Override
                            public void getText(CharSequence text) {
                                mfilter.put(group, text);
                                postSomething(mfilter);
                            }
                        });
                }
                break;
                case 8://leftimage
                {
//                        leftimage leftimage = new leftimage(mlayout.getContext());
//                        Glide.with(mlayout.getContext())
//                                .load(Uri.parse(assetPath + "image/" + model.title))
//                                .fitCenter()
//                                .into(leftimage.img);
//                        mlayout.addView(leftimage);
                }
                break;
                case 9://titlenoicon
                {
//                        titlenoicon titlenoicon = new titlenoicon(mlayout.getContext());
//                        titlenoicon.title.setText(model.title);
//
//                        mlayout.addView(titlenoicon);
                }
                break;
                case 10://labelandedit
                {
                        LabelAndEdit LabelAndEdit = (LabelAndEdit)mlayout.findViewWithTag(group + model.title);
                        LabelAndEdit.searchText.addTextChangedListener(new onTextChange() {
                            @Override
                            public void getText(CharSequence text) {
                                mfilter.put(group + model.title, text);
                                postSomething(mfilter);}
                        });

                }
                break;
                case 11://submit
                {
//                        button btn = new button(mlayout.getContext());
//                        btn.setTag(model.group);
//                        btn.setText(model.title);
//                        mlayout.addView(btn);
                }
                break;
            }
        }
    }

    public static void Expanded(ViewGroup rootlayout, String[] modeltype, String[] modelcontent) {
        if(Boolean.TRUE == rootlayout.getTag())return;
        rootlayout.removeAllViews();
        for (int index = 0; index < modeltype.length; index++) {
            switch (modeltype[index].toLowerCase()) {
                case "text": {
                    FreeTextView content = new FreeTextView(rootlayout.getContext());
                    content.setTextSizeFitSp(30);
                    content.setTextColor(Color.BLACK);
                    content.setPadding(25, 15, 25, 15);
                    rootlayout.addView(content, -1, -2);
                    content.setText(modelcontent[index]);
                    content.setGravity(Gravity.START);
                }
                break;
                case "youtube": {
                    FreeLayout content = new FreeLayout(rootlayout.getContext());
                    content.setPicSize(640,-2);
                    ImageView youtubebg = (ImageView)content.addFreeView(new ImageView(rootlayout.getContext()),640,480);
                    youtubebg.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    youtubebg.setTag(null);
                    content.setMargin(youtubebg,15, 15, 15, 15);
                    rootlayout.addView(content, -1, -2);
                    final String youtubeid = modelcontent[index];
                    String thumbnailpath = YouTubeThumbnail.getUrlFromVideoId(youtubeid, Quality.STANDARD_DEFINITION);
                    Glide.with(rootlayout.getContext())
                            .load(thumbnailpath)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(new SimpleTarget<GlideDrawable>() {
                                @Override
                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                    youtubebg.setImageDrawable(resource);
                                    ImageView mv = (ImageView)content.addFreeView(new ImageView(rootlayout.getContext()),100,70,new int[]{RelativeLayout.CENTER_IN_PARENT});
                                    mv.setTag(null);
                                    Glide.with(rootlayout.getContext()).load(R.drawable.youtube).fitCenter().into(mv);

                                }
                            });
                    youtubebg.setOnClickListener(v -> {
                        Intent intent = new Intent(v.getContext(), YouTubePlayerActivity.class);

                        // Youtube video ID (Required, You can use YouTubeUrlParser to parse Video Id from url)
                        intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, youtubeid);

                        // Youtube player style (DEFAULT as default)
                        intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);

                        // Screen Orientation Setting (AUTO for default)
                        // AUTO, AUTO_START_WITH_LANDSCAPE, ONLY_LANDSCAPE, ONLY_PORTRAIT
                        intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO);

                        // Show audio interface when user adjust volume (true for default)
                        intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);

                        // If the video is not playable, use Youtube app or Internet Browser to play it
                        // (true for default)
                        intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);

                        // Animation when closing youtubeplayeractivity (none for default)
//                        intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_ENTER, R.anim.fade_in);
//                        intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_EXIT, R.anim.fade_out);

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(intent);
                    });
                }
                break;
                default:{
                    if(modeltype[index].matches("((?i)IMAGE)+\\d{3}")){
                        FreeLayout mlayout = new FreeLayout(rootlayout.getContext());
                        rootlayout.addView(mlayout,-1, -2);
                        String filepath = "image/" + modelcontent[index];
                        String path = assetPath + filepath;
                        int size = Integer.parseInt(modeltype[index].replaceAll("((?i)IMAGE)",""));
                        mlayout.setPicSize(640, -2);
                        ImageView bg = (ImageView)mlayout.addFreeView(new ImageView(rootlayout.getContext()), size, -2);
                        mlayout.setMargin(bg, 15, 0, 15, 0);
                        bg.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        Glide.with(rootlayout.getContext()).load(Uri.parse(path)).fitCenter().into(bg);

                        bg.setTag("IMAGE");
                        break;
                    }
                }
                break;
            }
        }
    }

    protected static View.OnClickListener btnlistener(Map filter,String group) {
        return v -> {
            Hashtable mtable = (Hashtable)filter.get(group);
            if (mtable.contains(v.getTag())) {
                mtable.remove(v.getTag());
                ((FreeTextButton) v).setTextColor(ContextCompat.getColor(v.getContext(), R.color.gray02));
                v.setBackgroundResource(R.drawable.button_normal);
            } else {
                mtable.put(v.getTag(), v.getTag());
                ((FreeTextButton) v).setTextColor(ContextCompat.getColor(v.getContext(), R.color.white));
                v.setBackgroundResource(R.drawable.button_click);
            }
          postSomething(filter);
        };
    }

    static void postSomething(Object object){
        EventBus.getDefault().post(object);}
}

