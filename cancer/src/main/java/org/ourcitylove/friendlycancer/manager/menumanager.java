package org.ourcitylove.friendlycancer.manager;

import android.content.Context;

import com.alibaba.fastjson.JSON;

import org.apache.commons.io.IOUtils;
import org.ourcitylove.friendlycancer.App;
import org.ourcitylove.friendlycancer.library.Memory;
import org.ourcitylove.friendlycancer.model.KEYMODEL;
import org.ourcitylove.friendlycancer.model.MenuValue;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by Vegetable on 2016/6/10.
 */
public class menumanager {
    private Memory memory;
    private Context mcontext;
    private List<MenuValue> cachelist;

    public void init(Context context) {
        memory = new Memory(context);
        mcontext = context;
        initMenu();
    }

    public Observable<ArrayList<MenuValue>> getMenu() {

        Observable ob = Observable.concat(_getfromDisk(), _getfromDisk())
                .first(value -> value != null & !value.isEmpty())
                .flatMap(Observable::from)
                .toSortedList((val1, val2)-> Double.compare(Double.valueOf(val1.parentid), Double.valueOf(val2.parentid)));

        return ob;
    }

    public Observable<ArrayList<MenuValue>> _getfromDisk() {
        String m = memory.getString(KEYMODEL.MENU);
        return Observable.just(m)
                .map(menu -> JSON.parseArray(menu, MenuValue.class))
                .map(menu->new ArrayList<>(menu))
                .doOnNext(this::_storeMemory);
    }

    public void _storeDisk(ArrayList<MenuValue> mdisklist) {
        memory.setString(KEYMODEL.MENU, JSON.toJSONString(mdisklist));
    }

    public void _storeMemory(ArrayList<MenuValue> list) {
        this.cachelist = list;
    }

    private void initMenu() {
        if (App.pref.getString(KEYMODEL.MENU, "").equals("")) {
            try {
                String defaultmenu = IOUtils.toString(mcontext.getAssets().open("DefaultMenu.txt"), Charset.defaultCharset());
                App.pref.edit().putString(KEYMODEL.MENU, defaultmenu);
            } catch (IOException e) {
            }
        }
    }
}
