# Retrofit 2.X
## https://square.github.io/retrofit/ ##

-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

-keepclasseswithmembers class * { @retrofit2.* <methods>; }
-keepclasseswithmembers class * { @retrofit2.http.* <methods>; }
-keepclasseswithmembers interface * { @retrofit2.* <methods>; }
